
BUILD_PATH=build/intermediates/classes/release
INCLUDE_PATH=src/main/jni/include

echo "Using classes at ${BUILD_PATH} to generate into ${INCLUDE_PATH}"

javah -jni -classpath ${BUILD_PATH} -o ${INCLUDE_PATH}/com_cambriantech_CB_ImagePainter.h com.cambriantech.CB_ImagePainter
javah -jni -classpath ${BUILD_PATH} -o ${INCLUDE_PATH}/com_cambriantech_CB_Harmony.h com.cambriantech.CB_Harmony
javah -jni -classpath ${BUILD_PATH} -o ${INCLUDE_PATH}/com_cambriantech_CB_Coloring.h com.cambriantech.CBColoring