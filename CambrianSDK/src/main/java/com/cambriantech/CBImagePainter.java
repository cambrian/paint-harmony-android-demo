package com.cambriantech;


import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.media.ExifInterface;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.Map;
import java.util.Vector;

public class CBImagePainter extends CBImageView {

	private static final int TOUCH_STEP_BEGAN = 0;
	private static final int TOUCH_STEP_MOVED = 1;
	private static final int TOUCH_STEP_ENDED = 2;
	private static final int TOUCH_STEP_CANCELLED = 3;
	
	public static final int PAINT_TOOL_FILL = 0;
	public static final int PAINT_TOOL_PAINT = 1;
	public static final int PAINT_TOOL_ERASER = 2;
    public static final int PAINT_TOOL_RECTANGLE = 3;

    private boolean _rectSnapToEnabled = false;
    private boolean _hasSetColor = false;

	CB_ImagePainter _painter;
    private boolean _hasMadeChangesToImage;

    boolean _hasCheckedLicense;
    boolean _isLicensed;

    int _canceledTool = -1;

	private Paint _paintPreview;

    class DrawPoint {
        Point point;
        double radius;
    }
	private Vector<DrawPoint> _drawPoints;

    CBImagePainterListener _listener;

    public interface CBImagePainterListener {
        String getCBLicenseKey();
        void onHistoryChanged();
        void onStartedTool(int toolMode);
        void onFinishedTool(int toolMode);
        boolean shouldStartTool(int toolMode);
    }
	
	private void init() {

        if (isInEditMode()) {
            setBackgroundColor(Color.GRAY);
        }
        else {
            System.loadLibrary("CambrianSDK");
        }

        _canceledTool = -1;
        _painter = new CB_ImagePainter();
		_paintPreview = new Paint();
        _drawPoints = new Vector<DrawPoint>();
        _rectangleToolPoints = new Vector<Point>();
	}

	public CBImagePainter(Context context) {
		super(context);
        init();
	}
	
	public CBImagePainter(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    public CBImagePainter(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void setCBImagePainterListener(CBImagePainterListener listener) {
        _listener = listener;

        int result = _painter.enableWithLicenseKey(getContext(), listener.getCBLicenseKey());

        _isLicensed = (result == 0);

        if (!_isLicensed) {
            //show alert or something

            String message = "Unlicensed copy of API.";
            switch (result) {
//                license_status_trial_expired = 10,
//                        license_status_invalid_key = 11,
//                        license_status_undefined_key = 12,
//                        license_status_invalid_bundle = 13
                case 10:
                    message += " Trial period expired";
                    break;
                case 11:
                    message += " Invalid License Key";
                    break;
                case 12:
                    message += " Invalid Key";
                    break;
                case 13:
                    message += " Invalid bundle for this license";
                    break;
                default:

            }

            Log.d("Cambrian", message);
        }

        _hasCheckedLicense = true;
    }

    public void loadPNGAsset(String imagePath, boolean hasAlphaMasking) {

        //String assetPath = getAssetFilePath(imagePath);
        _painter.loadPNG(imagePath, hasAlphaMasking);

        _hasMadeChangesToImage = false;
        _imageWidth = _painter.getRenderedWidth();
        _imageHeight = _painter.getRenderedHeight();

        updateImageView();
    }




    public boolean hasLoadedImage() {
        return getDrawable() != null;
    }

    public boolean rectSnapToEnabled() {
        return _rectSnapToEnabled;
    }

    public void setRectSnapToEnabled(boolean enabled)
    {
        _rectSnapToEnabled = enabled;
    }

    protected void loadCompressedImage(byte[] imageData, int orientation, int[] size)
    {
        _painter.loadCompressedImage(imageData, orientation);

        //go ahead and load it
        Bitmap image = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
        setImageBitmap(image);

        _hasMadeChangesToImage = false;

        size[0] = _painter.getRenderedWidth();
        size[1] = _painter.getRenderedHeight();
    }

    public void saveToDirectory(String directoryPath) {
        Log.d(this.getClass().getSimpleName(), "Saving project to directory " + directoryPath);
        _painter.saveToDirectory(directoryPath);
    }

    public boolean loadFromDirectory(String directoryPath) {
        Log.d(this.getClass().getSimpleName(), "Loading project from directory " + directoryPath);
        boolean success = _painter.loadFromDirectory(directoryPath);

        if (success) {
            _hasMadeChangesToImage = false;
            _imageWidth = _painter.getRenderedWidth();
            _imageHeight = _painter.getRenderedHeight();

            updateImageView();
        }

        return success;
    }

    public void loadAsset(String imagePath, boolean hasAlphaMasking) {

        String assetPath = getAssetFilePath(imagePath);
        if (imagePath.contains(".png")) {
            _painter.loadPNG(assetPath, hasAlphaMasking);
        } else {
            InputStream stream = null;
            try {
                stream = getContext().getAssets().open(imagePath);
                loadCompressedImage(stream, ExifInterface.ORIENTATION_NORMAL);
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        _hasMadeChangesToImage = false;
        _imageWidth = _painter.getRenderedWidth();
        _imageHeight = _painter.getRenderedHeight();

        updateImageView();
    }

    private String getAssetFilePath(String filename) {
        String newFileName = "/data/data/" + getContext().getPackageName() + "/" + filename;

        File file = new File(newFileName);

        if (file.exists()) {
            return newFileName;
        }

        AssetManager assetManager = getContext().getAssets();

        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(filename);

            File directory = file.getParentFile();
            if (!directory.exists()) {
                directory.mkdirs();
            }
            out = new FileOutputStream(newFileName);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }

        return newFileName;
    }

    protected byte[] getRenderedImage()
    {
        return _painter.renderedImage();
    }

    public Bitmap getImageBitmap()
    {
        if (!_hasCheckedLicense) {
            Log.d("Cambrian", "Must set a listener for this object");
            return null;
        } else if (!_isLicensed) {
            //Log.d("Cambrian", "Invalid license");
            return null;
        } else if (getRenderedWidth() == 0) {
            Log.d("Cambrian", "Bitmap width is zero");
        }

        byte[] bitmapData = getRenderedImage();
        Buffer dst = ByteBuffer.wrap(bitmapData);
        Bitmap image = Bitmap.createBitmap(getRenderedWidth(), getRenderedHeight(), Bitmap.Config.ARGB_8888);
        image.copyPixelsFromBuffer(dst);
        return image;
    }

	private void touchedImage(Point point, int step) {
        if (getDrawable() == null) return;

        if (_listener != null) {
            if (step == TOUCH_STEP_BEGAN) {
                boolean shouldContinue = _listener.shouldStartTool(getToolMode());
                if (!shouldContinue) {
                    _canceledTool = getToolMode();
                    return;
                } else {
                    _canceledTool = -1;
                }
            }
            else if (step != TOUCH_STEP_BEGAN && _canceledTool == getToolMode()) {
                //this tool has been canceled
                return;
            }
        }


		boolean madeChanges = _painter.touchedImage(point.x, point.y, step);
		
		if (madeChanges) {
            _hasMadeChangesToImage = true;
            if (null != _listener) _listener.onHistoryChanged();
		}
	}

    public int maxLayerCount()
    {
        return 6;
    }

    public boolean canAppendNewLayer()
    {
        return numLayers() < maxLayerCount();
    }

    public boolean appendNewLayer()
    {
        decommitChanges();
        if (canAppendNewLayer()) {
            return _painter.appendNewLayer();
        }
        return false;
    }

    public boolean removeLayerAtIndex(int index)
    {
        decommitChanges();
        boolean success = _painter.removeLayerAtIndex(index);
        if (success) {
            updateImageView();
        }
        return success;
    }

    public boolean clearMaskAtIndex(int index)
    {
        decommitChanges();
        boolean success = _painter.clearMaskAtIndex(index);
        if (success) {
            updateImageView();
        }
        return success;
    }

    public boolean hasMadeChangesInMaskAtIndex(int index)
    {
        return _painter.hasMadeChangesInMaskAtIndex(index);
    }
	
	public void stepBackward()
	{
		_painter.stepBackward();
        if (null != _listener) _listener.onHistoryChanged();
		updateImageView();
	}
	
	public int getToolMode()
	{
		return _painter.toolMode();
	}
	
	public void setToolMode(int mode)
	{
		_painter.setToolMode(mode);
	}
	
	public int getPaintColor()
	{
		return _painter.paintColor();
	}

    public void setPaintColor(int color) {
        setPaintColor(color, false);
    }
	
	public void setPaintColor(int color, boolean modifyImage)
	{
        if (modifyImage && _painter.hasMadeChangesInTopLayer()) {
            _hasMadeChangesToImage = true;
            _painter.setPaintColor(color, true);
            updateImageView();
        } else {
            _painter.setPaintColor(color, false);
        }
        _hasSetColor = true;
	}

    public int getMinBrushSize()
    {
        return 5;
    }

    public int getMaxBrushSize()
    {
        return 40;
    }

	public boolean autoBrushSizeEnabled()
	{
		return _painter.autoBrushSizeEnabled();
	}
	
	public void setAutoBrushSizeEnabled(boolean enabled)
	{
        _painter.setAutoBrushSizeEnabled(enabled);
	}

    public boolean brushTapFillEnabled() {
        return _painter.brushTapFillEnabled();
    }

    public void setBrushTapFillEnabled(boolean enabled) {
        _painter.setBrushTapFillEnabled(enabled);
    }

    public boolean smartBrushEnabled() {
        return _painter.smartBrushEnabled();
    }

    public void setSmartBrushEnabled(boolean enabled) {
        _painter.setSmartBrushEnabled(enabled);
    }

    public int getBrushSize()
    {
        return _painter.brushSize();
    }

    public void setBrushSize(int size)
    {
        if (size >= getMinBrushSize() && size <= getMaxBrushSize()) {
            _painter.setBrushSize(size);
        }
    }
	
	public int maxHistorySize()
	{
		return _painter.maxHistorySize();
	}
	
	public void setMaxHistorySize(int max)
	{
		_painter.setMaxHistorySize(max);
	}
    
	public boolean canStepBackward()
	{
		return _painter.canStepBackward();
	}
	
	public boolean hasMadeChanges()
	{
		return _hasMadeChangesToImage;
	}
	
	public boolean hasChangesToCommit()
	{
		return _painter.hasChangesToCommit();
	}
	
	public void clearHistory()
	{
		_painter.clearHistory();
	}


    public void commitChanges() {
        _commitChanges();
    }

	public void commitChanges(final Runnable afterCommit)
	{
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                _commitChanges();

                if (afterCommit != null) {
                    afterCommit.run();
                }
            }
        }, 100);
	}

    private void _commitChanges() {
        if (_rectangleToolPoints.size() > 0) {

            int xOffset = -getPaddingLeft();
            int yOffset = -getPaddingTop();

            int pointArray[] = new int[2 + 2 * _rectangleToolPoints.size()];
            for (int i = 0; i < _rectangleToolPoints.size(); i++) {
                pointArray[2 * i] = _rectangleToolPoints.elementAt(i).x + xOffset;
                pointArray[2 * i + 1] = _rectangleToolPoints.elementAt(i).y + yOffset;
            }
            pointArray[pointArray.length - 2] = _rectangleToolPoints.elementAt(0).x + xOffset;
            pointArray[pointArray.length - 1] = _rectangleToolPoints.elementAt(0).y + yOffset;
            _painter.drawFilledPolygon(pointArray);

            _rectangleToolPoints.clear();

            _listener.onFinishedTool(PAINT_TOOL_RECTANGLE);
        }
        _painter.commitChanges();
        if (null != _listener) _listener.onHistoryChanged();
        updateImageView();
    }
	
	public void decommitChanges()
	{
        _rectangleToolPoints.clear();
		_painter.decommitChanges();
        updateImageView();
	}

    public int numLayers()
    {
        return _painter.numLayers();
    }

    public int editLayerIndex()
    {
        return _painter.editLayerIndex();
    }

    public void setEditLayerIndex(int index)
    {
        _painter.setEditLayerIndex(index);
    }

    public int getPaintColorAtIndex(int index)
    {
        return _painter.paintColorAtIndex(index);
    }

    public Map<String, String> getImageUserData()
    {
        return _painter.getImageUserData();
    }

    public void setImageUserData(Map<String, String> userData)
    {
        _painter.setImageUserData(userData);
    }

    public Map<String, String> getLayerUserData(int index)
    {
        return _painter.getLayerUserData(index);
    }

    public void setLayerUserData(Map<String, String> userData, int index)
    {
        _painter.setLayerUserData(userData, index);
    }

    void outOfRangeTouched(Point imageViewLocation, Point imageLocation, int action)
    {
        if (_drawPoints.size() > 0) {
            Log.d("Touch", "Out of range, committing");
            _drawPoints.clear();
            commitChanges();
            updateImageView();
        }
    }

    boolean imageTouched(Point imageViewLocation, Point imageLocation, int action)
    {
        if (!_hasSetColor || !isClickable()) return false;

        if (getToolMode() == PAINT_TOOL_RECTANGLE) {
            _handleRectangleTool(action, imageLocation);
            return true;
        }
        else if (getToolMode() != PAINT_TOOL_FILL) {
            DrawPoint point = new DrawPoint();
            point.point = imageViewLocation;
            point.radius = getBrushSize();
            _drawPoints.add(point);
            invalidate();
        }

        if (action == android.view.MotionEvent.ACTION_DOWN) {

            if (getToolMode() == PAINT_TOOL_PAINT) {
                //TODO:load color
                _paintPreview.setColor(_painter.paintColor());
            } else if (getToolMode() == PAINT_TOOL_ERASER) {
                _paintPreview.setColor(Color.GRAY);
            }

            _listener.onStartedTool(getToolMode());

            if (hasChangesToCommit()) commitChanges();
            //Log.d("Touch", "Touch down at " + location.x + ", " + location.y);
            touchedImage(imageLocation, CBImagePainter.TOUCH_STEP_BEGAN);

            return true;
        }
        else if (action == android.view.MotionEvent.ACTION_MOVE) {
            //Log.d("Touch", "Touch moved at " + location.x + ", " + location.y);
            touchedImage(imageLocation, CBImagePainter.TOUCH_STEP_MOVED);
            return true;
        }
        else if (action == android.view.MotionEvent.ACTION_UP) {
            _drawPoints.removeAllElements();
            //Log.d("Touch", "Touch up at " + location.x + ", " + location.y);
            touchedImage(imageLocation, CBImagePainter.TOUCH_STEP_ENDED);
            updateImageView();
            _listener.onFinishedTool(this.getToolMode());
            return true;
        }
        else if (action == android.view.MotionEvent.ACTION_CANCEL) {
            //Log.d("Touch", "Touch cancelled at " + location.x + ", " + location.y);
            touchedImage(imageLocation, CBImagePainter.TOUCH_STEP_CANCELLED);
            updateImageView();
            return true;
        }

        return false;
    }

    private double getZoomScale() {
        return 1.0f;
    }

    Vector<Point> _rectangleToolPoints;
    int _rectangleDraggingIndex = -1;

    public void showRectangle() {

        if (getToolMode() != PAINT_TOOL_RECTANGLE) {
            setToolMode(PAINT_TOOL_RECTANGLE);
        }

        Point centerPoint = new Point(_painter.getRenderedWidth() / 2, _painter.getRenderedHeight() / 2);
        _generateRectangle(centerPoint);

        _listener.onStartedTool(getToolMode());

        invalidate();
    }

    private void _generateRectangle(Point imageLocation) {

        int width = (int) ((double) _painter.getRenderedWidth() / 2.0f * getZoomScale());
        int height = (int) ((double) _painter.getRenderedHeight() / 2.0f * getZoomScale());

        _rectangleToolPoints.add(new Point(imageLocation.x - width / 2, imageLocation.y - height / 2));
        _rectangleToolPoints.add(new Point(imageLocation.x + width / 2, imageLocation.y - height / 2));
        _rectangleToolPoints.add(new Point(imageLocation.x + width / 2, imageLocation.y + height / 2));
        _rectangleToolPoints.add(new Point(imageLocation.x - width / 2, imageLocation.y + height / 2));

        for (int i=0; i<_rectangleToolPoints.size(); i++) {
            _rectangleToolPoints.elementAt(i).x = Math.max(0, _rectangleToolPoints.elementAt(i).x);
            _rectangleToolPoints.elementAt(i).x = Math.min(_painter.getRenderedWidth(), _rectangleToolPoints.elementAt(i).x);

            _rectangleToolPoints.elementAt(i).y = Math.max(0, _rectangleToolPoints.elementAt(i).y);
            _rectangleToolPoints.elementAt(i).y = Math.min(_painter.getRenderedHeight(), _rectangleToolPoints.elementAt(i).y);
        }
    }

    private void _handleRectangleTool(int step, Point imageLocation) {

        if (step == android.view.MotionEvent.ACTION_DOWN) {
            _rectangleDraggingIndex = -1;
            if (_rectangleToolPoints.size() == 0) {
                showRectangle();
            }
            else {
                double closestDistance = Integer.MAX_VALUE;
                int closestPoint = -1;
                for (int i=0; i<_rectangleToolPoints.size(); i++) {
                    Point point = _rectangleToolPoints.elementAt(i);

                    double distance = CBImageUtilities.getEuclideanDistance(imageLocation, point);

                    if (closestDistance == Integer.MAX_VALUE || distance < closestDistance) {
                        closestPoint = i;
                        closestDistance = distance;
                    }
                }

                double range = Math.max(25, 50 / getZoomScale());
                if (closestDistance < range) {
                    //[_rectangleToolPoints objectAtIndex:i];
                    _rectangleDraggingIndex = closestPoint;
                } else {
                    _rectangleDraggingIndex = -1;
                }
            }

        } else if (_rectangleDraggingIndex >= 0) {
            //dragging


            boolean isTablet = false;
            int radius = isTablet ? 20 : 40;

            Point closestPoint = new Point(-1, -1);
            double closestPointDistance = -1;
            if (_rectSnapToEnabled) {
                int closestPointData[] = _painter.getClosestLinePoint(imageLocation.x, imageLocation.y, radius);
                closestPoint = new Point(closestPointData[0], closestPointData[1]);
                closestPointDistance = CBImageUtilities.getEuclideanDistance(imageLocation, closestPoint);
            }

            int indexBefore = (_rectangleDraggingIndex + 3) % 4;
            int indexAfter = (_rectangleDraggingIndex + 1) % 4;

            Point siblingPoint, siblingPointA, siblingPointB;
            siblingPoint = new Point();
            siblingPoint.x = _rectangleToolPoints.elementAt(indexBefore % 2 > 0 ? indexBefore : indexAfter).x;
            siblingPoint.y = _rectangleToolPoints.elementAt(indexBefore % 2 > 0 ? indexAfter : indexBefore).y;
            siblingPointA = new Point(imageLocation.x, imageLocation.y);
            siblingPointB = new Point(imageLocation.x, imageLocation.y);
            siblingPointA.x = siblingPoint.x;
            siblingPointB.y = siblingPoint.y;

            double siblingDistanceA = CBImageUtilities.getEuclideanDistance(imageLocation, siblingPointA);
            double siblingDistanceB = CBImageUtilities.getEuclideanDistance(imageLocation, siblingPointB);

            double minSiblingDistance = radius / 2;

            if (siblingDistanceA < minSiblingDistance) {
                imageLocation = siblingPointA;
            }
            else if (siblingDistanceB < minSiblingDistance) {
                imageLocation = siblingPointB;
            }
            else if (closestPoint.x >= 0 && closestPointDistance < radius) {
                imageLocation = closestPoint;
            }

            _rectangleToolPoints.elementAt(_rectangleDraggingIndex).x = imageLocation.x;
            _rectangleToolPoints.elementAt(_rectangleDraggingIndex).y = imageLocation.y;
        } else {
            _rectangleDraggingIndex = -1;
        }

        invalidate();
    }

    protected void onDraw(Canvas canvas) {
    	super.onDraw(canvas);

        if (_rectangleToolPoints.size() > 0) {

            //create polygon path
            float[] point = getUntranslatedCoords(this, _rectangleToolPoints.elementAt(3).x, _rectangleToolPoints.elementAt(3).y, true);
            Path polygonPath = new Path();
            polygonPath.moveTo(point[0], point[1]);

            for (int i=0; i<_rectangleToolPoints.size(); i++) {
                point = getUntranslatedCoords(this, _rectangleToolPoints.elementAt(i).x, _rectangleToolPoints.elementAt(i).y, true);
                polygonPath.lineTo(point[0], point[1]);
            }

            //draw interior color filled
            _paintPreview.setStrokeWidth(3);
            _paintPreview.setStyle(Paint.Style.FILL);
            _paintPreview.setColor(getPaintColor());
            _paintPreview.setAlpha(120);
            canvas.drawPath(polygonPath, _paintPreview);

            //draw lines
            _paintPreview.setAlpha(255);
            _paintPreview.setStyle(Paint.Style.STROKE);
            _paintPreview.setColor(Color.RED);
            _paintPreview.setPathEffect(new DashPathEffect(new float[] {10,20}, 0));
            canvas.drawPath(polygonPath, _paintPreview);

            //draw circles
            _paintPreview.setPathEffect(null);
            _paintPreview.setColor(Color.BLUE);
            for (int i=0; i<_rectangleToolPoints.size(); i ++) {
                point = getUntranslatedCoords(this, _rectangleToolPoints.elementAt(i).x, _rectangleToolPoints.elementAt(i).y, true);
                canvas.drawCircle(point[0], point[1], (float) 20, _paintPreview);
            }
            return;
        }

    	if (_drawPoints.size() < 2) {
    		return;
    	}
    	
    	//int radius = (int) (imageScale() *  getBrushSize());

    	//draw start
    	DrawPoint previewPoint = _drawPoints.elementAt(0);
        int radius = (int) (imageScale() *  previewPoint.radius);
        _paintPreview.setStrokeWidth((float) radius * 2);
    	canvas.drawCircle(previewPoint.point.x, previewPoint.point.y, (float)  radius, _paintPreview);
    	
    	for (int i=1; i<_drawPoints.size(); i ++) {
            DrawPoint nextPoint = _drawPoints.elementAt(i);
            radius = (int) (imageScale() *  nextPoint.radius);
            _paintPreview.setStrokeWidth((float) radius * 2);
    		canvas.drawLine(previewPoint.point.x, previewPoint.point.y, nextPoint.point.x, nextPoint.point.y, _paintPreview);
    		previewPoint = nextPoint;
    		canvas.drawCircle(previewPoint.point.x, previewPoint.point.y, (float) radius, _paintPreview);
    	}
    	
    	//draw end
    	previewPoint = _drawPoints.elementAt(_drawPoints.size() - 1);
        radius = (int) (imageScale() *  previewPoint.radius);
    	canvas.drawCircle(previewPoint.point.x, previewPoint.point.y, (float) radius, _paintPreview);
    }

    public static String getThumbnailPath(String directoryPath) {
        //return CB_ImagePainter.getThumbnailPath(directoryPath);
        return directoryPath + "/thumbnail.png";
    }

    public static String getPreviewPath(String directoryPath) {
        //return CB_ImagePainter.getPreviewPath(directoryPath);
        return directoryPath + "/preview.png";
    }
}
