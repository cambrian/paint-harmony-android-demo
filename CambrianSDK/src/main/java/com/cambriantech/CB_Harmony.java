package com.cambriantech;

/**
 * Created by joelteply on 7/7/13.
 */
public class CB_Harmony {

    native byte[] renderedImage();
    native int getRenderedWidth();
    native int getRenderedHeight();

    native void loadCompressedImage(byte[] image, int orientation);

    native int getColorAtPoint(int touchPointX, int touchPointY);
    native int[] getMostCommonColors(int count);
}
