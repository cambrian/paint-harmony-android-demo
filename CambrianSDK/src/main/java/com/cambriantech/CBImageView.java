package com.cambriantech;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by joelteply on 7/7/13.
 */
abstract class CBImageView extends ImageView {

    protected int _imageWidth = 0;
    protected int _imageHeight = 0;

    private int [] _orientationHistory = new int[5];
    private int _orientationIndex;
    private int _highestIndex = -1;
    private int _currentRotation;
    private boolean _isTrackingRotation;
    private int _trackedRotationChange;
    private int _trackedRotationChangeAtStart;

    private void init() {

        if (isInEditMode()) {
            setBackgroundColor(Color.GRAY);
        }

        setClickable(true);

        setOnTouchListener(_imageViewTouchListener);

        if (isInEditMode()) {
            return;
        }

        _imageViewOrientationListener = new OrientationEventListener(getContext(), SensorManager.SENSOR_DELAY_NORMAL) {
            public void onOrientationChanged(int angle) {
                angle = angle + 45;
                if (angle > 360) angle = angle - 360;
                int orientation = angle / 90;

                if (_orientationIndex > _highestIndex) {
                    _highestIndex = _orientationIndex;
                }
                _orientationHistory[_orientationIndex] = orientation;
                _orientationIndex ++;

                if (_orientationIndex == _orientationHistory.length) {
                    _orientationIndex = 0;
                }

                int lastOrientation = _currentRotation;
                _currentRotation = getCurrentRotation();

                if (_highestIndex == _orientationHistory.length - 1 && lastOrientation != _currentRotation) {
                    //enough data
                    rotationChanged(lastOrientation, _currentRotation);
                }
            }
        };

        _imageViewOrientationListener.enable();

    }

    public CBImageView(Context context) {
        super(context);
        init();
    }

    public CBImageView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    public CBImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    abstract void loadCompressedImage(byte[] imageData, int orientation, int[] size);

    abstract byte[] getRenderedImage();

    abstract void outOfRangeTouched(Point imageViewLocation, Point imageLocation, int action);

    abstract boolean imageTouched(Point imageViewLocation, Point imageLocation, int action);

    public byte[] loadInputStream(InputStream is)
    {
        BufferedInputStream bis = new BufferedInputStream(is);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int nRead;
        byte[] data = new byte[16384];

        try {
            while ((nRead = bis.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }
            buffer.flush();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        return buffer.toByteArray();
    }

    public int getCameraOrientation()
    {
        int rotation = _currentRotation;
        if (_trackedRotationChange >= 0) {
            rotation = _trackedRotationChange;
        }

        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_BACK, info);
        int cameraRotation = info.orientation / 90;

        Log.d(getClass().getSimpleName(), "Camera rotation: " + cameraRotation);

        //rotation = cameraRotation; //(rotation + cameraRotation) % 4;

        switch (rotation) {
            case Surface.ROTATION_0:
                //portrait
                return ExifInterface.ORIENTATION_ROTATE_90;
            case Surface.ROTATION_90:
                //landscape
                return ExifInterface.ORIENTATION_ROTATE_180;
            case Surface.ROTATION_180:
                //reverse portrait
                return ExifInterface.ORIENTATION_ROTATE_270;
            default:
                //reverse landscape
                return ExifInterface.ORIENTATION_NORMAL;
        }
    }

    private int angleToOrienation(int angle) {
        return rotationToOrientation(angle / 90);
    }

    private int rotationToOrientation(int rotation) {
        switch (rotation) {
            case Surface.ROTATION_0:
                //portrait
                return ExifInterface.ORIENTATION_NORMAL;
            case Surface.ROTATION_90:
                //landscape
                return ExifInterface.ORIENTATION_ROTATE_90;
            case Surface.ROTATION_180:
                //reverse portrait
                return ExifInterface.ORIENTATION_ROTATE_180;
            default:
                //reverse landscape
                return ExifInterface.ORIENTATION_ROTATE_270;
        }
    }

    public int getOrientation(Uri imageUri, boolean fromCamera) throws IOException {
        String[] orientationColumn = {MediaStore.Images.ImageColumns.ORIENTATION};
        Cursor cursor = getContext().getContentResolver().query(imageUri, orientationColumn, null, null, null);
        int orientation = ExifInterface.ORIENTATION_UNDEFINED;

        if (cursor != null && cursor.getCount() == 1) {
            cursor.moveToFirst();
            int angle = cursor.getInt(0);
            orientation = angleToOrienation(angle);
        }
        else if (fromCamera) {
            orientation = getCameraOrientation();
            Log.d("CBImageView", "Camera orientation is " + orientation);
        }
        return orientation;
    }

    public void loadCompressedImage(Uri imageUri, boolean fromCamera, int orientation)
    {
        try {
            if (fromCamera) {
                closedCamera();
            }
            if (orientation == ExifInterface.ORIENTATION_UNDEFINED) {
                orientation = getOrientation(imageUri, fromCamera);
            }

            InputStream stream;
            stream = getContext().getContentResolver().openInputStream(imageUri);
            loadCompressedImage(stream, orientation);
            stream.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void loadCompressedImage(InputStream is)
    {
        loadCompressedImage(is, ExifInterface.ORIENTATION_NORMAL);
    }

    public void loadCompressedImage(InputStream is, int orientation)
    {
        byte[] imageData = loadInputStream(is);
        loadCompressedImage(imageData, orientation);
    }

    public void loadCompressedImage(byte[] imageData)
    {
        loadCompressedImage(imageData, ExifInterface.ORIENTATION_NORMAL);
    }



    public void loadCompressedImage(byte[] imageData, int orientation)
    {
//        int[] size = new int[2];
//        loadCompressedImage(imageData,  orientation, size);
//        _imageWidth = size[0];
//        _imageHeight = size[1];
        byte[] byteArray = CBImageUtilities.convertToJpeg(imageData, 640);

        int[] size = new int[2];
        loadCompressedImage(byteArray,  orientation, size);
        _imageWidth = size[0];
        _imageHeight = size[1];

        updateImageView();

        //loadCompressedImage(image.getB,  orientation, size);

        //causing rotational issues
//        Bitmap image = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
//        setImageBitmap(image);
        //updateImageView();
    }

    public Bitmap getImageBitmap()
    {
        byte[] bitmapData = getRenderedImage();
        Bitmap image = BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length);
        return image;
    }

    public int getRenderedWidth()
    {
        return _imageWidth;
    }

    public int getRenderedHeight()
    {
        return _imageHeight;
    }

    protected final float[] getPointerCoords(ImageView view, MotionEvent e)
    {
        final int index = e.getActionIndex();
        return getTranslatedCoords(view, e.getX(index), e.getY(index), true);
    }


    View.OnTouchListener _imageViewTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {

            ImageView imageView = (ImageView) view;

            if (getDrawable() == null) return false;

            float[] coordinates = getPointerCoords(imageView, motionEvent);
//            Log.d("Touch", "Touch at " + coordinates[0] + ", " + coordinates[1]
//                    + " in " + imageView.getWidth() + "x" + imageView.getHeight());

            final Point imageLocation = new Point((int) coordinates[0], (int) coordinates[1]);
            final Point imageViewlocation = new Point((int) motionEvent.getX(), (int) motionEvent.getY());

            int range = 50;

            if (imageLocation.x - range > _imageWidth || imageLocation.y - range > _imageHeight
                    || imageLocation.x + range < 0 || imageLocation.y + range < 0) {
                outOfRangeTouched(imageViewlocation, imageLocation, motionEvent.getAction());
                return false;
            }

            return imageTouched(imageViewlocation, imageLocation, motionEvent.getAction());
        }
    };

    OrientationEventListener _imageViewOrientationListener;


    protected final float[] getTranslatedCoords(ImageView view, float x, float y, boolean doTranslation)
    {
        //Log.d("Touch", "getTranslatedCoords " + x + ", " + y);
        final float[] coords = new float[] { x - view.getPaddingLeft(), y - view.getPaddingTop() };

        Matrix matrix = new Matrix();
        view.getImageMatrix().invert(matrix);
        if (doTranslation) {
            matrix.postTranslate(view.getScrollX(), view.getScrollY());
        }
        matrix.mapPoints(coords);

        int drawableW = getDrawable().getIntrinsicWidth();
        int drawableH = getDrawable().getIntrinsicHeight();

        coords[0] = coords[0] * _imageWidth / drawableW;
        coords[1] = coords[1] * _imageHeight / drawableH;

        return coords;
    }

    protected final float[] getUntranslatedCoords(ImageView view, float x, float y, boolean doTranslation)
    {
        final float[] coords = new float[] { x, y};

        Matrix matrix = view.getImageMatrix();
        if (doTranslation) {
            matrix.postTranslate(view.getScrollX(), view.getScrollY());
        }
        matrix.mapPoints(coords);

        int drawableW = getDrawable().getIntrinsicWidth();
        int drawableH = getDrawable().getIntrinsicHeight();

        coords[0] = coords[0] * drawableW / _imageWidth;
        coords[1] = coords[1] * drawableH / _imageHeight;

        return coords;
    }

    public final float imageScale()
    {
        float[] coords = getTranslatedCoords(this, getPaddingLeft(), getPaddingTop(), false);

        if (coords[0] == 0) {
            return (getMeasuredWidth() - getPaddingLeft() - getPaddingRight()) / (float) _imageWidth;
        } else {
            return (getMeasuredHeight() - getPaddingTop() - getPaddingBottom()) / (float) _imageHeight;
        }

    }

    protected int getCurrentRotation()
    {
        if (_highestIndex < 0) return 0;

        Arrays.sort(_orientationHistory);
        return _orientationHistory[_highestIndex / 2];
    }

    protected void rotationChanged(int lastRotation, int currentRotation) {
        Log.d(getClass().getSimpleName(), "Orientation changed to " + currentRotation + " from " + lastRotation);
        if (_isTrackingRotation && currentRotation != _trackedRotationChangeAtStart) {
            _trackedRotationChange = currentRotation;
        }
    }

    public void openedCamera()
    {
        _isTrackingRotation = true;
        _trackedRotationChange = -1;
        _trackedRotationChangeAtStart = _currentRotation;
    }

    public void closedCamera()
    {
        _isTrackingRotation = false;
        _trackedRotationChangeAtStart = -1;
    }

    protected void updateImageView()
    {
        if (getRenderedWidth() > 0) {
            Bitmap image = getImageBitmap();
            setImageBitmap(image);
            refreshDrawableState();
        }
    }
}
