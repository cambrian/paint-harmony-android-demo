package com.cambriantech;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.SensorManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by joelteply on 7/18/13.
 */
public class CBCameraActivity extends Activity implements CBCameraView.CBCameraViewDelegate {

    private Camera _camera;
    private int _cameraId=-1;

    ViewGroup _innerLayout;
    private CBCameraView _preview;
    private ImageView _previewPhoto;
    private Button _rotateButton;
    private Button _captureButton;
    private Button _continueButton;

    boolean _takingPicture;
    boolean _isTablet;

    private OnOrientationChange _onOrientationChange=null;
    private int _displayOrientation=-1;
    private int _outputOrientation=-1;

    private int _lastPictureOrientation=-1;
    private int _userOrientationAdjustment = 0;
    private boolean _inPreview = false;

    private Uri _imageUri;
    private boolean _resizeImage;

    private Point windowSize;
    private double screenAspectRatio;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cb_camera_activity);

        _isTablet = (getBaseContext().getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;

        _onOrientationChange = new OnOrientationChange(getBaseContext());
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        _preview = (CBCameraView) findViewById(R.id.camera_preview);
        _preview.setDelegate(this);

        _previewPhoto = (ImageView) findViewById(R.id.camera_photo_preview);

        _innerLayout =  (ViewGroup) findViewById(R.id.camera_inner_layout);

        _continueButton = (Button) findViewById(R.id.camera_button_continue);
        _continueButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        exitWithImage();
                    }
                }
        );


        // Add a listener to the Capture button
        Button cancelButton = (Button) findViewById(R.id.camera_button_cancel);
        cancelButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CBCameraActivity.this.finish();
                    }
                }
        );

        // Add a listener to the Capture button
        _captureButton = (Button) findViewById(R.id.camera_button_capture);
        _captureButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        _continueButton.setVisibility(View.GONE);

                        if (_previewPhoto.getVisibility() == View.VISIBLE) {
                            showPhotoPreview(false);
                        } else {
                            takePicture();
                        }

                        orientationChanged(getResources().getConfiguration().orientation);
                        setCameraDisplayOrientation();

                    }
                }
        );

        _rotateButton = (Button) findViewById(R.id.camera_button_rotate);
        _rotateButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (_preview.getVisibility() == View.VISIBLE) return;

                        _userOrientationAdjustment += 90;
                        _userOrientationAdjustment = (_userOrientationAdjustment % 360);
                        displayImage();
                    }
                }
        );
    }

    Camera getCamera() {
        if (_camera == null) {
            // Create an instance of Camera
            _cameraId = CBCameraControl.getCamera(false);
            if (_cameraId == -1) {
                _cameraId = CBCameraControl.getCamera(true);
            }

            try {
                _camera = Camera.open(_cameraId);
                _preview.setCamera(_camera);
            } catch (RuntimeException e) {
                Log.e(CBCameraControl.class.getSimpleName(),
                        "Camera failed to open: " + e.getLocalizedMessage());
                return null;
            }

            _onOrientationChange.enable();
        }
        return _camera;
    }

    public void onResume() {
        super.onResume();

        Camera camera = getCamera();

        if (camera == null) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            // set title
            alertDialogBuilder.setTitle("Cannot connect to camera service");

            // set dialog message
            alertDialogBuilder
                    .setMessage("Could not open the camera interface. This usually means an application failed to release access to it.\n\nTo resolve this issue, restart your device.")
                    .setCancelable(false)
                    .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // if this button is clicked, close
                            // current activity
                            CBCameraActivity.this.finish();
                        }
                    });
            alertDialogBuilder.show();
            return;
        }

        _onOrientationChange.enable();

        windowSize = CBImageUtilities.getScreenSize(this);
        screenAspectRatio = (double) windowSize.x / (double) windowSize.y;

        _userOrientationAdjustment = 0;

        Camera.Parameters params = camera.getParameters();
        setCameraPictureOrientation(params);

        Camera.Size size = CBCameraControl.getClosestPreviewSize(params, 640, 480, true);
        if (size != null) {
            Log.d(CBCameraControl.class.getSimpleName(), "Got " + size.width + "x" + size.height);
            params.setPreviewSize(size.width, size.height);
        }

        orientationChanged(getResources().getConfiguration().orientation);
        setCameraDisplayOrientation();
        camera.setParameters(params);
    }

    @Override
    protected void onPause() {
        super.onPause();

        releaseCamera();

        finish();
        // Another activity is taking focus (this activity is about to be "paused").
    }

    private void takePicture() {

        _userOrientationAdjustment = 0;
        setCameraDisplayOrientation();

        Camera.Parameters params = getCamera().getParameters();


        //params.setPictureFormat(ImageFormat.JPEG);

        setCameraPictureOrientation(params);

        final boolean hasFlash = params.getSupportedFlashModes() != null;

        if (hasFlash) {
            params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        }

        getCamera().setParameters(params);

        if (!_takingPicture) {
            _takingPicture = true;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getCamera().setOneShotPreviewCallback(_previewCallback);
                }
            }, 200);
        }
    }

    private Camera.PreviewCallback _previewCallback = new Camera.PreviewCallback() {
        @Override
        public void onPreviewFrame(byte[] data, Camera camera) {
            stopPreview();
            Camera.Parameters params = getCamera().getParameters();
            final boolean hasFlash = params.getSupportedFlashModes() != null;

            if (hasFlash) {
                //turn off flash
                params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                getCamera().setParameters(params);
            }

            _pictureCallback.onPictureTaken(data, getCamera());
        }
    };

    private Camera.PictureCallback _pictureCallback = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            _takingPicture = false;
            AsyncPhotoSaver saver = new AsyncPhotoSaver();
            saver.execute(data);
        }
    };

    private void _setImageUri(Uri imageUri, boolean resize) {
        _imageUri = imageUri;
        _resizeImage = resize;
        _continueButton.setVisibility(View.VISIBLE);
        displayImage();
    }

    private void displayImage() {

        if (null == _imageUri) return;

        Bitmap displayImage;
        if (_resizeImage) {
            displayImage = CBImageUtilities.getReducedBitmap(this, _imageUri, 640);
        }
        else {
            displayImage = CBImageUtilities.getBitmap(this, _imageUri);
        }

        if (null != displayImage) {
            displayImage = CBImageUtilities.rotateImage(displayImage, getNecessaryRotation());
            _previewPhoto.setImageBitmap(displayImage);
        } else {
            //try at least
            _previewPhoto.setImageURI(_imageUri);
        }

        showPhotoPreview(true);
    }

    private void showPhotoPreview(boolean show) {
        if (show) {
            _captureButton.setText("Retake");
            _previewPhoto.setVisibility(View.VISIBLE);
            _rotateButton.setVisibility(View.VISIBLE);
            _preview.setVisibility(View.GONE);
            releaseCamera();
        } else {
            _captureButton.setText("Capture");
            _previewPhoto.setVisibility(View.GONE);
            _rotateButton.setVisibility(View.GONE);
            _preview.setVisibility(View.VISIBLE);
            getCamera();
            startPreview();
        }
    }

    private void exitWithImage() {
        Intent result = getIntent();

        int currentOrientation = getExifOrientation(getNecessaryRotation());

        result.putExtra(MediaStore.Images.ImageColumns.ORIENTATION, currentOrientation);
        //result.putExtra("dest", destination);

        result.putExtra(MediaStore.EXTRA_OUTPUT, _imageUri);
        //Log.d(getClass().getSimpleName(), "Saved picture to " + imageUri.toString());

        Bitmap thumbnail = CBImageUtilities.getReducedBitmap(this, _imageUri, 200);
        thumbnail = CBImageUtilities.rotateImage(thumbnail, getNecessaryRotation());

        result.putExtra("data", thumbnail);

        _takingPicture = false;
        setResult(Activity.RESULT_OK, result);

        finish();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void finish()
    {
        //camera destroyed by surface
        super.finish();
        _onOrientationChange.disable();
    }

    @Override
    public void onStop() {
        super.onStop();
        releaseCamera();
    }

    @Override
    public void releaseCamera() {
        if (_inPreview) stopPreview();

        if (_camera != null) {
            try {
                _camera.release();
                _camera = null;
            } catch (Exception ex) { }
        }
        _onOrientationChange.disable();
    }

    @Override
    public void stopPreview() {
        if (!_inPreview) return;

        _inPreview=false;
        if (_camera != null) {
            _camera.stopPreview();
        }
        _onOrientationChange.disable();
    }

    @Override
    public void startPreview() {
        if (_inPreview) return;

        _inPreview=true;
        getCamera().startPreview();
        _onOrientationChange.enable();
    }

    int getNecessaryRotation() {
        return (_lastPictureOrientation + _userOrientationAdjustment) % 360;
    }

    private class AsyncPhotoSaver extends AsyncTask<byte[], Void, Uri> {

        @Override
        protected Uri doInBackground(byte[]... params) {
            byte[] data = params[0];
            Uri fileUri = getIntent().getParcelableExtra(MediaStore.EXTRA_OUTPUT);

            File pictureFile = null;


            if (fileUri != null) {
                pictureFile = new File(fileUri.getPath());
            } else {
                pictureFile = CBCameraControl.getOutputMediaFile(CBCameraControl.MEDIA_TYPE_IMAGE, "Cambrian");
                if (pictureFile == null){
                    Log.d(getClass().getSimpleName(), "Error creating media file, check storage permissions");
                    return null;
                }
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                saveYUVToJPEG(getCamera(), fos, data);
                //fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
                Log.d(getClass().getSimpleName(), "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d(getClass().getSimpleName(), "Error accessing file: " + e.getMessage());
            }

            return fileUri;
        }

        protected void onProgressUpdate(Void... params) {

        }

        @Override
        protected void onPostExecute(Uri fileUri) {
            _setImageUri(fileUri, false);
        }
    }

    private void saveYUVToJPEG(Camera camera, FileOutputStream out, byte[] data ) {
        YuvImage yuvimg = null;
        try {
            Camera.Parameters params= getCamera().getParameters();
            int width = params.getPreviewSize().width;
            int height = params.getPreviewSize().height;


            Rect rect = new Rect();
            rect.left   = 0;
            rect.top    = 0;
            rect.right  = width  - 1;
            rect.bottom = height - 1;       // The -1 is required, otherwise a buffer overrun occurs
            yuvimg = new YuvImage(data, params.getPreviewFormat(), width, height, null);
            yuvimg.compressToJpeg(rect, 100, out);
        } finally {
            yuvimg = null;
        }
    }

    private int getExifOrientation(int rotation) {

        switch (rotation) {
            case 270:
                return ExifInterface.ORIENTATION_ROTATE_270;
            case 180:
                return ExifInterface.ORIENTATION_ROTATE_180;
            case 90:
                return ExifInterface.ORIENTATION_ROTATE_90;
            default:
                return ExifInterface.ORIENTATION_NORMAL;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        setCameraDisplayOrientation();
    }

    // based on
    // http://developer.android.com/reference/android/hardware/Camera.html#setDisplayOrientation(int)
    // and http://stackoverflow.com/a/10383164/115145

    private void setCameraDisplayOrientation() {
        Camera.CameraInfo info=new Camera.CameraInfo();
        int rotation= getWindowManager().getDefaultDisplay()
                        .getRotation();
        int degrees=0;
        DisplayMetrics dm=new DisplayMetrics();

        Camera.getCameraInfo(_cameraId, info);
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        switch (rotation) {
            case Surface.ROTATION_0:
                degrees=0;
                break;
            case Surface.ROTATION_90:
                degrees=90;
                break;
            case Surface.ROTATION_180:
                degrees=180;
                break;
            case Surface.ROTATION_270:
                degrees=270;
                break;
        }

        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            _displayOrientation=(info.orientation + degrees) % 360;
            _displayOrientation=(360 - _displayOrientation) % 360;
        }
        else {
            _displayOrientation=(info.orientation - degrees + 360) % 360;
        }

        boolean wasInPreview=_inPreview;

        if (_camera != null) {
            //Log.d("JOEL", "setDisplayOrientation: " + _displayOrientation);
            getCamera().setDisplayOrientation(_displayOrientation);
        }
    }

    private void setCameraPictureOrientation(Camera.Parameters params) {
        Camera.CameraInfo info=new Camera.CameraInfo();

        Camera.getCameraInfo(_cameraId, info);

        if (getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED) {
            _outputOrientation=
                    getCameraPictureRotation(getWindowManager()
                            .getDefaultDisplay()
                            .getOrientation());
        }
        else if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            _outputOrientation=(360 - _displayOrientation) % 360;
        }
        else {
            _outputOrientation=_displayOrientation;
        }

        if (_lastPictureOrientation != _outputOrientation) {
            params.setRotation(_outputOrientation);
            _lastPictureOrientation=_outputOrientation;
        }
    }

    // based on:
    // http://developer.android.com/reference/android/hardware/Camera.Parameters.html#setRotation(int)

    private int getCameraPictureRotation(int orientation) {
        Camera.CameraInfo info=new Camera.CameraInfo();
        Camera.getCameraInfo(_cameraId, info);
        int rotation=0;

        orientation=(orientation + 45) / 90 * 90;

        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            rotation=(info.orientation - orientation + 360) % 360;
        }
        else { // back-facing camera
            rotation=(info.orientation + orientation) % 360;
        }

        return(rotation);
    }

    private void orientationChanged(int orientation) {
        if (_camera != null && orientation != -1) {
            int newOutputOrientation=getCameraPictureRotation(orientation);

            if (newOutputOrientation != _outputOrientation) {
                _outputOrientation=newOutputOrientation;

                Camera.Parameters params=getCamera().getParameters();

                params.setRotation(_outputOrientation);

                //_camera.setDisplayOrientation(_outputOrientation);
                setCameraDisplayOrientation();

                getCamera().setParameters(params);
                _lastPictureOrientation=_outputOrientation;

                //scalePreviewWindow(1);
            }
        }
    }


    private class OnOrientationChange extends OrientationEventListener {
        private boolean isEnabled=false;

        public OnOrientationChange(Context context) {
            super(context);
            disable();
        }

        @Override
        public void onOrientationChanged(int orientation) {
            orientationChanged(orientation);
        }

        @Override
        public void enable() {
            isEnabled=true;
            super.enable();
        }

        @Override
        public void disable() {
            isEnabled=false;
            super.disable();
        }

        boolean isEnabled() {
            return(isEnabled);
        }
    }

    public void scalePreviewWindow(double scale) {

        if (null == _preview) return;

        try {
            double previewWidth =  _preview.previewSize().width;
            double previewHeight = _preview.previewSize().height;
            if (_preview.isOrientationRotated()) {
                previewWidth =  _preview.previewSize().height;
                previewHeight = _preview.previewSize().width;
            }
            double previewAspectRatio = previewWidth / previewHeight;

            int resizedWidth = 0;
            int resizedHeight = 0;

            //reverse the less than to greater to not get bigger than screen
            if (previewAspectRatio < screenAspectRatio) {
                resizedWidth = (int) (windowSize.x * scale);
                resizedHeight = (int) (((double) resizedWidth) / previewAspectRatio);
            } else {
                resizedHeight = (int) (windowSize.y * scale);
                resizedWidth = (int) (((double) resizedHeight) * previewAspectRatio);
            }

            if (resizedWidth > windowSize.x || resizedHeight > windowSize.y) {
                int newWidth = resizedWidth;
                int newHeight = resizedHeight;

                if (previewAspectRatio > screenAspectRatio) {
                    newHeight = (int) (((double) resizedWidth) / screenAspectRatio);
                } else {
                    newWidth = (int) (((double) resizedHeight) * screenAspectRatio);
                }

                getWindow().setLayout(newWidth, newHeight);
            } else {
                getWindow().setLayout(windowSize.x, windowSize.y);
            }

            CBImageUtilities.setViewSize(_innerLayout, windowSize.x, windowSize.y);
            CBImageUtilities.setViewSize(_preview, resizedWidth, resizedHeight);
        }
        catch (NullPointerException ne) {
            Log.w("scalePreviewWindow failed: scale="+scale, ne);
        }
    }
}