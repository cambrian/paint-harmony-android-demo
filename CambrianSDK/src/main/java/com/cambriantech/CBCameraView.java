package com.cambriantech;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;

import java.io.IOException;
import java.util.Vector;

/** A basic Camera preview class */
public class CBCameraView extends TextureView implements
        TextureView.SurfaceTextureListener, Camera.PreviewCallback {

    private Camera mCamera;
    private byte[] _previewData;

    public abstract interface CBCameraViewDelegate
    {
        void releaseCamera();
        void stopPreview();
        void startPreview();
    }
    private CBCameraViewDelegate _delegate;
    public void setDelegate(CBCameraViewDelegate delegate)
    {
        _delegate = delegate;
    }

    public void setCamera(Camera camera) {
        mCamera = camera;
        setSurfaceTextureListener(this);
        try {
            mCamera.setPreviewTexture(getSurfaceTexture());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void init() {
        if (isInEditMode()) {
            return;
        }
        setSurfaceTextureListener(this);
    }

    public CBCameraView(Context context) {
        super(context);
        init();
    }

    public CBCameraView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    public CBCameraView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public Camera.Size previewSize() {
        return mCamera.getParameters().getPreviewSize();
    }

    public boolean isOrientationRotated() {
        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            return false;
        } else {
            return true;
        } //see setDisplayOrientation() above
    }

    private void initPreview(SurfaceTexture surface, int width, int height) {

        if (mCamera == null) return;

        try {
            mCamera.setPreviewTexture(surface);
        } catch (Throwable ex) {
            Log.e("JOEL", "Exception in setPreviewTexture()", ex);
        }

        Camera.Parameters params = mCamera.getParameters();
        Camera.Size previewSize = params.getPreviewSize();

        Log.e("JOEL", "SurfaceTexture width="+width + " height=" + height + " camera width=" + previewSize.width + " height=" + previewSize.height);

        int scaledHeight = 0;
        int scaledWidth = 0;
        float scaleX = 1f;
        float scaleY = 1f;

        float ratioSurface = width > height ? (float) width / height : (float) height / width;
        float ratioPreview = (float) previewSize.width / previewSize.height;
        if (ratioPreview > ratioSurface) {
            scaledWidth = width;
            scaledHeight = (int) (((float) previewSize.width / previewSize.height) * width);
            scaleX = 1f;
            scaleY = (float) scaledHeight / height;
        } else if (ratioPreview < ratioSurface) {
            scaledWidth = (int) (height / ((float) previewSize.width / previewSize.height));
            scaledHeight = height;
            scaleX = (float) scaledWidth / width;
            scaleY = 1f;
        }

        Matrix matrix = new Matrix();

        if (width > height) {
            matrix.setScale(scaleY, 3.5F * scaleX);
            matrix.postTranslate(-scaledHeight / 4F, 3.5F * -scaledWidth / 2.5F);
            matrix.postTranslate(scaledHeight / 4F, 3.5F * scaledWidth / 2.5F);
        } else {
            matrix.setScale(scaleX, scaleY);
            matrix.postTranslate(-scaledWidth / 4F, -scaledHeight / 2.5F);
            matrix.postTranslate(scaledWidth / 4F, scaledHeight / 2.5F);
        }
        setTransform(matrix);

        initPreviewBuffers();
    }

    public byte[] getPreviewData() {
        return _previewData;
    }

    public void startPreviewCapture() {
        mCamera.setPreviewCallbackWithBuffer(this);
    }

    private void initPreviewBuffers() {

        if (_previewData == null) {
            Camera.Parameters params = mCamera.getParameters();
            Camera.Size previewSize = params.getPreviewSize();
            _previewData = new byte[previewSize.height * previewSize.width * 3 / 2];
            Log.d("JOEL", "Camera Preview Size Width: " + previewSize.width + " Height: " + previewSize.height + " Buffer: " + _previewData.length + " bytes");
        }

        if (_previewData != null) {
            //mCamera.addCallbackBuffer(null);//flush
            mCamera.addCallbackBuffer(_previewData);
        }
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {


        camera.addCallbackBuffer(_previewData);
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {

        initPreview(surface, width, height);

        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new settings
        try {
            _delegate.startPreview();

        } catch (Exception e){
            Log.d(getClass().getSimpleName(), "Error starting camera preview: " + e.getMessage());
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        initPreview(surface, width, height);
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        _delegate.stopPreview();
        _delegate.releaseCamera();
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }
}