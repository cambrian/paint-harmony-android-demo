package com.cambriantech;

import java.util.HashMap;
import java.util.Map;

public class CB_ImagePainter {

    native int enableWithLicenseKey(Object caller, String key);
	
	 //properties
    native byte[] renderedImage();
    native int getRenderedWidth();
    native int getRenderedHeight();

    native int paintColor();
    native void setPaintColor(int color, boolean updateImage);

    native void drawFilledPolygon(int[] points);

    native boolean canAppendNewLayer();
    native boolean appendNewLayer();
    native boolean removeLayerAtIndex(int index);
    native boolean clearMaskAtIndex(int index);
    native boolean hasMadeChangesInMaskAtIndex(int index);
    
    native int toolMode();
    native void setToolMode(int mode);

    native int brushSize();
    native void setBrushSize(int brushSize);

    native boolean autoBrushSizeEnabled();
    native void setAutoBrushSizeEnabled(boolean enabled);

    native boolean brushTapFillEnabled();
    native void setBrushTapFillEnabled(boolean enabled);

    native boolean smartBrushEnabled();
    native void setSmartBrushEnabled(boolean enabled);

    native int maxHistorySize();
    native void setMaxHistorySize(int max);

    native int numLayers();
    native int editLayerIndex();
    native void setEditLayerIndex(int index);
    native int paintColorAtIndex(int index);

    private native String[][] _getImageUserData();
    private native void _setImageUserData(String[][] userData);

    private native String[][] _getLayerUserData(int index);
    private native void _setLayerUserData(String[][] userData, int index);
    
    native boolean canStepBackward();
    native boolean hasMadeChanges();
    native boolean hasMadeChangesInTopLayer();
    native boolean hasChangesToCommit();
	
    //methods
	native boolean touchedImage(int touchPointX, int touchPointY, int step);

    native int[] getClosestLinePoint(int xPos, int yPos, int radius);
	
	native void loadImage(byte[] largeImage, int width, int height, int orientation);
	native void loadCompressedImage(byte[] image, int orientation);
    native void loadPNG(String path, boolean hasAlphaMasking);

    native boolean saveToDirectory(String directoryPath);
    native boolean loadFromDirectory(String directoryPath);
    native static String getThumbnailPath(String directoryPath);
    native static String getPreviewPath(String directoryPath);
	
	native void clearHistory();
	native void stepBackward();
	native void commitChanges();
	native void decommitChanges();

    Map<String,String> getImageUserData()
    {
        return arrayToMap(_getImageUserData());
    }

    void setImageUserData(Map<String,String> userData)
    {
        _setImageUserData(mapToArray(userData));
    }

    Map<String,String> getLayerUserData(int index)
    {
        String[][] userData = _getLayerUserData(index);
        if (userData != null) {
            return arrayToMap(_getLayerUserData(index));
        }
        return null;
    }

    void setLayerUserData(Map<String,String> userData, int index)
    {
        _setLayerUserData(mapToArray(userData), index);
    }

    private String[][] mapToArray(Map<String,String> map) {
        String[][] results = new String[map.size()][2];

        int i = 0;
        for (Map.Entry<String, String> entry : map.entrySet()) {
            results[i][0] = entry.getKey();
            results[i][1] = entry.getValue();
            i++;
        }

        return results;
    }

    private Map<String, String> arrayToMap(String[][] array) {
        Map<String, String> map = new HashMap<String, String>();

        for (int i=0; i<array.length; i++) {
            map.put(array[i][0], array[i][1]);
        }

        return map;
    }
}
