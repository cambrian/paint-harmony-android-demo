package com.cambriantech;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by joelteply on 5/20/14.
 */
public class CBImageUtilities {

    public static String convertMediaUriToPath(Context context, Uri uri) {
        String [] proj={MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String path = cursor.getString(column_index);
        cursor.close();
        return path;
    }

    public static Point getImageSize(byte[] imageData) {
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(imageData, 0, imageData.length, bounds);

        return new Point(bounds.outWidth, bounds.outHeight);
    }

    public static Point getImageSize(String fileName) {
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(fileName, bounds);
        return new Point(bounds.outWidth, bounds.outHeight);
    }

    public static Point getImageSize(Context context, Uri imageUri) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(imageUri);
            BitmapFactory.Options bounds = new BitmapFactory.Options();
            bounds.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(inputStream, null, bounds);
            inputStream.close();
            return new Point(bounds.outWidth, bounds.outHeight);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static int getSampleSize(Point imageSize, int maxSize) {
        int largestDimension =  Math.max(imageSize.x, imageSize.y);

        int sampleSize = largestDimension / maxSize;
        if (sampleSize == 0) {
            sampleSize = 1;
        }
        return sampleSize;
    }

    public static Bitmap getBitmap(Context context, Uri imageUri) {

        try {
            InputStream inputStream = context.getContentResolver().openInputStream(imageUri);
            Bitmap imageDownSampled = BitmapFactory.decodeStream(inputStream);
            inputStream.close();

            return imageDownSampled;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Bitmap getReducedBitmap(Context context, Uri imageUri, int maxSize) {

        try {
            Point imageSize = getImageSize(context, imageUri);

            //sample at smaller size
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inSampleSize = getSampleSize(imageSize, maxSize);

            InputStream inputStream = context.getContentResolver().openInputStream(imageUri);
            Bitmap imageDownSampled = BitmapFactory.decodeStream(inputStream, null, opts);
            inputStream.close();

            return imageDownSampled;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Bitmap getReducedBitmap(byte[] imageData, int maxSize) {
        Point imageSize = getImageSize(imageData);
        //sample at smaller size
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = getSampleSize(imageSize, maxSize);
        Bitmap imageDownSampled = BitmapFactory.decodeByteArray(imageData, 0, imageData.length, opts);

        return imageDownSampled;
    }

    public static byte[] convertToJpeg(byte[] imageData, int maxSize) {

        Bitmap imageDownSampled = getReducedBitmap(imageData, maxSize);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        imageDownSampled.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

    public static Bitmap rotateImage(Bitmap bitmap, int rotate) {
        if (rotate != 0) {
            // rotate
            Matrix m = new Matrix();
            m.postRotate(rotate);
            Bitmap rotImage = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                    bitmap.getHeight(), m, true);
            bitmap.recycle();
            System.gc();

            return rotImage;
        }
        return bitmap;
    }

    public static Point getScreenSize(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        Display display = wm.getDefaultDisplay();

        Point size = new Point();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(size);
        } else {
            size.set(display.getWidth(), display.getHeight());
        }

        return size;
    }

    public static float getScreenSmallestWidth() {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();

        int widthPixels = metrics.widthPixels;
        int heightPixels = metrics.heightPixels;

        float scaleFactor = metrics.density;

        float widthDp = widthPixels / scaleFactor;
        float heightDp = heightPixels / scaleFactor;

        return Math.min(widthDp, heightDp);
    }

    public static float getScreenLargestWidth() {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();

        int widthPixels = metrics.widthPixels;
        int heightPixels = metrics.heightPixels;

        float scaleFactor = metrics.density;

        float widthDp = widthPixels / scaleFactor;
        float heightDp = heightPixels / scaleFactor;

        return Math.max(widthDp, heightDp);
    }

    public static void setViewSize(View view, int width, int height) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = width;
        params.height = height;
        view.setLayoutParams(params);
    }

    public static double getEuclideanDistance(Point pointA, Point pointB) {
        return Math.sqrt(Math.pow(pointA.x - pointB.x, 2) + Math.pow(pointA.y - pointB.y, 2));
    }
}
