package com.cambriantech;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import java.util.Vector;

/**
 * Created by joelteply on 7/7/13.
 */
public class CBHarmony  extends CBImageView {
    CB_Harmony _harmony;
    int _detectedColors[];
    private CBHarmonyListener _delegate;

    private void init() {
        if (isInEditMode()) {
            setBackgroundColor(Color.GRAY);
        }
        else {
            System.loadLibrary("CambrianSDK");
        }
        _harmony = new CB_Harmony();
    }

    /**
     * Interface definition for a callback to be invoked when a view is clicked.
     */
    public interface CBHarmonyListener {
        void gotCommonColors(int detectedColors[]);
        void touchStarted(Point point, int color);
        void touchMoved(Point point, int color);
        void touchEnded(Point point, int color);
    }

    public CBHarmony(Context context) {
        super(context);
        init();
    }

    public CBHarmony(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CBHarmony(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void setCBHarmonyListener(CBHarmonyListener delegate) {
        _delegate = delegate;
    }

    void loadCompressedImage(byte[] imageData, int orientation, int[] size) {
        _harmony.loadCompressedImage(imageData, orientation);
        size[0] = _harmony.getRenderedWidth();
        size[1] = _harmony.getRenderedHeight();

        _detectedColors = _harmony.getMostCommonColors(15);

        if (null != _delegate) {
            _delegate.gotCommonColors(_detectedColors);
        }
    }

    byte[] getRenderedImage() {
        return _harmony.renderedImage();
    }

    void outOfRangeTouched(Point imageViewLocation, Point imageLocation, int action)
    {
        if (null != _delegate) {
            _delegate.touchEnded(imageViewLocation, lastColor);
        }
    }

    static Point lastPoint;
    static int lastColor;
    boolean imageTouched(Point imageViewLocation, Point imageLocation, int action)
    {

        boolean pointReoccurred = lastPoint != null && lastPoint.x == imageLocation.x && lastPoint.y == imageLocation.y;

        int color = lastColor;
        if (!pointReoccurred) {
            color = _harmony.getColorAtPoint(imageLocation.x, imageLocation.y);
        }

        //Log.d("CBHarmony", "color at point " + imageLocation.x + ", " + imageLocation.y + " is " + color);

        if (action==MotionEvent.ACTION_UP) {
            _delegate.touchEnded(imageViewLocation, color);
            lastColor = 0;
        }
        else if (action==MotionEvent.ACTION_CANCEL) {
            _delegate.touchEnded(imageViewLocation, color);
        }
        else if (action== MotionEvent.ACTION_DOWN) {
            _delegate.touchStarted(imageViewLocation, color);
        }
        else if (action== MotionEvent.ACTION_MOVE) {
            _delegate.touchMoved(imageViewLocation, color);
        }

        lastPoint = imageLocation;
        lastColor = color;

        return true;
    }



}
