package com.cambriantech;

import android.graphics.Color;

public class CBColoring {
    static {
        System.loadLibrary("CambrianSDK");
    }

    public native static int[] getComplementaryColors(int color, int count, double angleSpan);
    public native static int[] getAdjacentColors(int color, int count, double angleSpan);
    public native static int[] getShadesOfColor(int color, int count);

    public native static double getColorDistance(int colorA, int colorB, boolean asHSV, float[] coefficient);
}
