package com.cambriantech;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CBCameraControl {

    /** Check if this device has a camera */
    public static boolean hasCamera(Context context) {

        // if device support rear camera?
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            //yes
           return true;
        }else{
            //no
            // if device support any other camera?
            if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)){
                // this device has a camera
               return true;
                 } else {
                // no camera on this device
               return false;
           }
        }
    }

    public static int getCamera(boolean isFront) {
        int cameraFacing = isFront ? Camera.CameraInfo.CAMERA_FACING_FRONT : Camera.CameraInfo.CAMERA_FACING_BACK;
        int cameraCount = 0;
        Camera cam = null;
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        cameraCount = Camera.getNumberOfCameras();
        for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            if (cameraInfo.facing == cameraFacing) {
                return camIdx;
            }
        }

        return -1;
    }

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    /** Create a file Uri for saving an image or video */
    public static Uri getOutputMediaFileUri(int type, String title){
        return Uri.fromFile(getOutputMediaFile(type, title));
    }

    /** Create a File for saving an image or video */
    public static File getOutputMediaFile(int type, String title){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), title);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    public static Camera.Size getClosestPreviewSize(Camera.Parameters params, int idealWidth, int idealHeight, boolean requireAspectRatio) {

        List<Camera.Size> sizes = params.getSupportedPreviewSizes();
        return _getClosestSize(params, sizes, idealWidth, idealHeight, requireAspectRatio);
    }

    public static Camera.Size getClosestPictureSize(Camera.Parameters params, int idealWidth, int idealHeight, boolean requireAspectRatio) {

        List<Camera.Size> sizes = params.getSupportedPictureSizes();
        return _getClosestSize(params, sizes, idealWidth, idealHeight, requireAspectRatio);
    }

    private static Camera.Size _getClosestSize(Camera.Parameters params, List<Camera.Size> sizes, int idealWidth, int idealHeight, boolean requireAspectRatio) {

        //Log.i(TAG, "CAMERA SIZE: (" + this.mCameraView.getWidth() + ", " + this.mCameraView.getHeight() + ")");

        Camera.Size bestSize = null;
        double bestAspectRatio = 0;
        double idealAspectRatio = ((double) idealWidth) / ((double) idealHeight);

        for (Camera.Size size : sizes) {
            Log.d("JOEL", "Camera size available: " + size.width + "x" + size.height);
            if (size.width == idealWidth && size.height == idealHeight) {
                return size;
            }
            double aspectRatio = ((double) size.width) / ((double) size.height);

            double aspectFactor = requireAspectRatio ? 0.99f : 0.8f;

            if (bestSize == null ||
                    (size.width >= bestSize.width && size.height >= bestSize.height
                    && (aspectFactor * (double) size.width) < bestSize.width
                    && Math.abs(aspectRatio - idealAspectRatio) <= Math.abs(bestAspectRatio - idealAspectRatio))) {
                bestSize = size;
                bestAspectRatio = aspectRatio;
            }
        }

        Log.d("JOEL", "Chose camera size: " + bestSize.width + "x" + bestSize.height);
        return bestSize;
    }
}
