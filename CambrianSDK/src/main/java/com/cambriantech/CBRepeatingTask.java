package com.cambriantech;

/**
 * Created by joelteply on 7/30/13.
 */

import android.os.AsyncTask;

/**
 * Created by joelteply on 7/30/13.
 */
public abstract class CBRepeatingTask<Result> extends AsyncTask <Object, Object, Object> {

    private long _startTime;
    public static long lastQueryTime;
    public static long lastQueryDuration;

    public CBRepeatingTask() {

    }

    protected abstract Result ExecuteTask();
    protected abstract void HandleTaskResult(Result result);

    @Override
    protected void onPreExecute()
    {
        this._startTime = System.currentTimeMillis();
        lastQueryTime = System.currentTimeMillis();
    }

    @Override
    protected Object doInBackground(Object... voids) {
        return ExecuteTask();
    }

    @Override
    protected void onPostExecute(Object result)
    {
        lastQueryDuration = System.currentTimeMillis() - this._startTime;
        HandleTaskResult((Result) result);
    }

    @Override
    protected void onProgressUpdate(Object... params) {

    }

    static CBRepeatingTask _existingTask;
    public static void performTask(final CBRepeatingTask task)
    {
        performTask(task, false);
    }

    public static void performTask(final CBRepeatingTask task, boolean required) {

        if (_existingTask != null && _existingTask.getStatus() != AsyncTask.Status.FINISHED) {
            if (required) {
                //schedule for later
                final Thread t = new Thread() {
                    @Override
                    public void run() {
                        try {
                            while (_existingTask.getStatus() != AsyncTask.Status.FINISHED) {
                                Thread.sleep(100L);
                            }
                            _existingTask = task;
                            _existingTask.execute();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {

                        }
                    }
                };
                t.start();
            }
            return;
        }

        if (!required && _existingTask != null) {
            long timeSinceLastQuery = System.currentTimeMillis() - CBRepeatingTask.lastQueryTime;
            if (timeSinceLastQuery < 3 * CBRepeatingTask.lastQueryDuration) {
                return;//too consuming
            }
        }

        _existingTask = task;
        _existingTask.execute();
    }
}