package com.cambriantech.paintharmony;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.SimpleAdapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IconListItemAdapter extends SimpleAdapter {
    private Context context;
    private HashMap<String, Drawable> icons;
    final String assetPrefix = "file:///android_asset/";

    public IconListItemAdapter(Context context, List<? extends Map<String, ?>> mData,
                               int resource, String[] from, int[] to) {
        super(context, mData, resource, from, to);
        this.context = context;
        this.icons = new HashMap<String, Drawable>();

        final int count = to.length;
        //preload assets
        for (int i =0; i <mData.size(); i++) {
            final Map dataSet = mData.get(i);
            for (int j = 0; j < count; j++) {

                final Object data = dataSet.get(from[j]);
                String text = data == null ? "" : data.toString();

                if (text.startsWith(assetPrefix)) {
                    String assetFileName = text.substring(assetPrefix.length());
                    AsyncIconData iconData = new AsyncIconData(null, assetFileName);
                    new AsyncIconLoader().execute(iconData);
                }
            }
        }
    }

    public void setViewImage(ImageView v, String value) {
        try {
            v.setImageResource(Integer.parseInt(value));
        } catch (NumberFormatException nfe) {

            if (value.startsWith(assetPrefix)) {
                String assetFileName = value.substring(assetPrefix.length());
                AsyncIconData data = new AsyncIconData(v, assetFileName);
                new AsyncIconLoader().execute(data);
            } else {
                v.setImageURI(Uri.parse(value));
            }
        }
    }

    private class AsyncIconData {
        public ImageView imageView;
        public String assetFileName;

        public AsyncIconData(ImageView imageView, String assetFileName) {
            this.imageView = imageView;
            this.assetFileName = assetFileName;
        }
    }

    private class AsyncIconLoader extends AsyncTask<AsyncIconData, Void, Drawable> {

        AsyncIconData data;
        @Override
        protected Drawable doInBackground(AsyncIconData... params) {

            Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
            this.data = params[0];
            if (icons.containsKey(data.assetFileName)) {
                return icons.get(data.assetFileName);
            }

            AssetManager mAssetManager = context.getResources().getAssets();

            //check for @2x file
            float density = context.getResources().getDisplayMetrics().density;
            String asset2x = data.assetFileName.replace(".png", "@2x.png");

            //Log.d(getClass().getName(), asset2x);

            try {
                boolean asset2xExists = Arrays.asList(mAssetManager.list("")).contains(asset2x);

                InputStream ims = asset2xExists && density >= 2.0 ? mAssetManager.open(asset2x) :  mAssetManager.open(data.assetFileName);
                // load image as Drawable
                Drawable drawable = Drawable.createFromStream(ims, null);
                // set image to ImageView
                return drawable;

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Drawable drawable) {
            if (this.data.imageView != null) {
                this.data.imageView.setImageDrawable(drawable);
            }

            if (!icons.containsKey(data.assetFileName)) {
                icons.put(data.assetFileName, drawable);
            }
        }
    }
}
