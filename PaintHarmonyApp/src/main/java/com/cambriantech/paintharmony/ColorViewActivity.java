package com.cambriantech.paintharmony;


import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cambriantech.CBColoring;
import com.cambriantech.paintharmony.data.ColorCategory;
import com.cambriantech.paintharmony.data.PaintColor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by joelteply on 7/7/13.
 */
public class ColorViewActivity extends SubViewActivity {

    private PaintColor _paintColor;
    private ViewGroup _color_view_layout;

    private ImageView _color_view_add_favorite_image;
    private TextView _color_view_brand_text;
    private TextView _color_view_name_text;
    private TextView _color_view_code_text;

    private ViewGroup _color_view_scrollers;
    private ColorScroller _color_view_shades_scroller;
    private ColorScroller _color_view_complements_scroller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.color_view_activity);

        _color_view_layout = (ViewGroup) findViewById(R.id.color_view_layout);

        _color_view_add_favorite_image = (ImageView) findViewById(R.id.color_view_add_favorite_image);
        _color_view_add_favorite_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isFavorite = getPaintColor().getIsFavorite();
                getPaintColor().setIsFavorite(!isFavorite);
                displayIsFavorite();

                //notify
                Intent intent = new Intent(PaintUtility.FAVORITES_CHANGED_NOTIFICATION);
                intent.putExtra(PaintUtility.PAINT_COLOR_EXTRA_KEY, getPaintColor());
                PaintUtility.sendBroadcast(intent, getBaseContext(), this);
            }
        });


        _color_view_brand_text = (TextView) findViewById(R.id.color_view_brand_text);
        _color_view_name_text = (TextView) findViewById(R.id.color_view_name_text);
        _color_view_code_text = (TextView) findViewById(R.id.color_view_code_text);

        _color_view_scrollers = (ViewGroup) findViewById(R.id.color_view_scrollers);
        _color_view_shades_scroller = (ColorScroller) findViewById(R.id.color_view_shades_scroller);
        _color_view_complements_scroller = (ColorScroller) findViewById(R.id.color_view_complements_scroller);

        setPaintColor(PaintUtility.getCurrentPaintColor());
    }

    public PaintColor getPaintColor() {
        return _paintColor;
    }

    public void setPaintColor(PaintColor paintColor) {
        _paintColor = paintColor;
        _color_view_layout.setBackgroundColor(paintColor.getColor());

        ColorCategory category = paintColor.getColorCategory();
        _color_view_brand_text.setText(category.getBrand().getName() + " - " + category.getName());
        _color_view_name_text.setText(paintColor.getName());
        _color_view_code_text.setText(paintColor.getCode());
        displayIsFavorite();

        if (PaintUtility.isLite(getBaseContext())) {
            _color_view_scrollers.setVisibility(View.GONE);
        } else {
            int[] colorShades = CBColoring.getShadesOfColor(_paintColor.getColor(), 15);
            loadScroller(_color_view_shades_scroller, colorShades);

            int[] colorComplements = CBColoring.getComplementaryColors(_paintColor.getColor(), 5, 45.0f);
            int[] colorAnalogues = CBColoring.getAdjacentColors(_paintColor.getColor(), 5, 45.0f);
            int[] combined = new int[colorComplements.length + colorAnalogues.length];
            System.arraycopy(colorComplements, 0, combined, 0, colorComplements.length);
            System.arraycopy(colorAnalogues, 0, combined, colorComplements.length, colorAnalogues.length);
            loadScroller(_color_view_complements_scroller, combined);
        }

    }

    private void loadScroller(ColorScroller scroller, int[] colors) {
        ArrayList<PaintColor> paintColors = new ArrayList<PaintColor>();
        for (int i=0; i<colors.length; i++) {
            PaintColor paintColor = new PaintColor();
            paintColor.setColor(colors[i]);
            paintColors.add(paintColor);
        }
        scroller.setColors(paintColors);
    }

    void displayIsFavorite() {

        if (getPaintColor().getIsFavorite()) {
            _color_view_add_favorite_image.setImageResource(R.drawable.favorites_cell_icon);
        } else {
            _color_view_add_favorite_image.setImageResource(R.drawable.favorites_cell_icon_off);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
