package com.cambriantech.paintharmony;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

/**
 * Created by joelteply on 9/11/14.
 */
public class LayerMenuItem extends RelativeLayout {

    public interface LayerMenuItemListener {
        void LayerMenuItem_Selected(int index);
    }

    //context menu, tool menu
    private View _item_color;
    private Button _item_button;

    public LayerMenuItemListener _listener;
    public int _index;

    private void inflate(Context context)
    {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(inflater != null){
            inflater.inflate(R.layout.layer_menu_item, this);
        }

        _item_color = findViewById(R.id.paint_layers_menu_item_color);

        _item_button = (Button) findViewById(R.id.paint_layers_menu_item_button);
        _item_button.setTextColor(Color.BLACK);
        _item_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _listener.LayerMenuItem_Selected(_index);
            }
        });

        setClickable(true);
    }

    public LayerMenuItem(Context context) {
        super(context);

        inflate(context);
    }

    public LayerMenuItem(Context context, AttributeSet attrs) {
        super(context, attrs);

        inflate(context);
    }

    @SuppressLint("NewApi")
    public LayerMenuItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        inflate(context);
    }

    public void setInfo(LayerMenuItemListener visualiser, int index, int color, String text) {
        _listener = visualiser;
        _index = index;
        _item_color.setBackgroundColor(color);
        _item_button.setText(text);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        _item_button.setEnabled(enabled);
    }

    @Override
    public void setSelected(boolean selected)
    {
        View v = findViewById(R.id.paint_layers_menu_item);
        v.setBackgroundColor(selected ? Color.rgb(200,200,200) : Color.rgb(240,240,240));
    }

}