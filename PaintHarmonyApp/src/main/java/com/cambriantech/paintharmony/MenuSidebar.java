package com.cambriantech.paintharmony;


import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import android.graphics.Paint;
import android.os.AsyncTask;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import com.cambriantech.CBRepeatingTask;
import com.cambriantech.paintharmony.data.*;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;


public class MenuSidebar extends LinearLayout {

    private final static String ASSET_PREFIX = "file:///android_asset/";
    private final static String RESOURCE_PREFIX = "";

    private final static String FEATURES_SECTION = "Features";

    private final static int FEATURES_LOAD_PHOTO_ID = 0;
    private final static String FEATURES_LOAD_PHOTO_ICON = RESOURCE_PREFIX + R.drawable.library_cell_icon;
    private final static String FEATURES_LOAD_PHOTO_TITLE = "Photo from library";
    private final static String FEATURES_LOAD_PHOTO_DESC = "Load a photo from your library";

    private final static int FEATURES_COLOR_PHOTO_ID = 1;
    private final static String FEATURES_COLOR_PHOTO_ICON = RESOURCE_PREFIX + R.drawable.camera_cell_icon;
    private final static String FEATURES_COLOR_PHOTO_TITLE = "Color from Camera";
    private final static String FEATURES_COLOR_PHOTO_DESC = "Get a color from the camera";

    private final static int FEATURES_COLOR_LIBRARY_ID = 2;
    private final static String FEATURES_COLOR_LIBRARY_ICON = RESOURCE_PREFIX + R.drawable.color_library_cell_icon;
    private final static String FEATURES_COLOR_LIBRARY_TITLE = "Color from Library";
    private final static String FEATURES_COLOR_LIBRARY_DESC = "Get a color from a photo in your library";

    private final static int FEATURES_LOAD_EXAMPLE_ID = 3;
    private final static String FEATURES_LOAD_EXAMPLE_ICON = ASSET_PREFIX + "living-room-1-masked.png";
    private final static String FEATURES_LOAD_EXAMPLE_TITLE = "Load Default Image";
    private final static String FEATURES_LOAD_EXAMPLE_DESC = "Reload the default image";

    private final static int FEATURES_MORE_ID = 4;
    private final static String FEATURES_MORE_ICON = RESOURCE_PREFIX + R.drawable.pro_icon;
    private final static String FEATURES_MORE_TITLE_LITE = "Many More Brands";
    private final static String FEATURES_MORE_DESC_LITE = "Over 23,000 paints available";
    private final static String FEATURES_MORE_TITLE_PRO = "Suggestions and Feedback";
    private final static String FEATURES_MORE_DESC_PRO = "Suggest a brand or feature";

    private Context context;
    private MenuSidebarListener _delegate;

    private EditText menu_search_text;
    private ListView listview;
    private ListView color_results;

    public interface MenuSidebarListener {
        void onLibraryChosen();
        void onCameraColorChosen();
        void onLibraryColorChosen();
        void onExampleChosen();
        void onPaintColorChosen(PaintColor color);
        void onColorCategoryChosen(ColorCategory colorCategory);
        void onMoreChosen();
    }

    private void inflate(Context context)
    {
        this.context = context;

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(inflater != null){
            inflater.inflate(R.layout.menu_sidebar, this);
        }
        menu_search_text = (EditText) findViewById(R.id.menu_search_text);
        menu_search_text.addTextChangedListener(_searchTextChanged);

        listview = (ListView) findViewById(R.id.listview);
        listview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
                if (listview.getVisibility() == View.VISIBLE) stopSearching();
            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i2, int i3) {

            }
        });

        color_results = (ListView) findViewById(R.id.color_results);
        color_results.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
                if (color_results.getVisibility() == View.VISIBLE) clearSearchFocus();
            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i2, int i3) {

            }
        });

        color_results.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
             PaintColor paintColor = (PaintColor) color_results.getItemAtPosition(position);
             _delegate.onPaintColorChosen(paintColor);
              if (PaintUtility.isLandscape(getContext())) {
                  stopSearching();
                  listview.setSelectionFromTop(0, 0);
              }
            }

        });
    }

    public MenuSidebar(Context context) {
        super(context);

        inflate(context);
    }

    public MenuSidebar(Context context, AttributeSet attrs) {
        super(context, attrs);

        inflate(context);
    }

    @SuppressLint("NewApi")
    public MenuSidebar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        inflate(context);
    }

    public void setMenuSidebarListener(MenuSidebarListener delegate) {
        _delegate = delegate;
    }

    private final static String ITEM_DATA = "data";
    private final static String ITEM_ICON = "icon";
    private final static String ITEM_TITLE = "title";
    private final static String ITEM_CAPTION = "caption";

    private Map<String,?> createItem(Object data, String icon, String title, String caption) {
        Map<String,Object> item = new HashMap<String,Object>();
        item.put(ITEM_DATA, data);
        item.put(ITEM_ICON, icon);
        item.put(ITEM_TITLE, title);
        item.put(ITEM_CAPTION, caption);
        return item;
    }

    public void willOpen() {

//        menu_search_text.setFocusableInTouchMode(true);
//        menu_search_text.requestFocusFromTouch();
    }

    public void opened() {

    }

    public void willClose() {
        listview.setSelectionFromTop(0, 0);
        stopSearching();
    }

    private void stopSearching() {
        clearSearchFocus();
        color_results.setVisibility(View.GONE);
    }

    private void clearSearchFocus() {
        menu_search_text.clearFocus();
        menu_search_text.setText("");
        InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(menu_search_text.getWindowToken(), 0);
    }

    public void closed() {

        //menu_search_text.setFocusable(false);
    }

    public void loadContent() {

        if (isInEditMode()) {
            return;
        }

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Map<String,Object> item = (Map<String,Object>) listview.getItemAtPosition(position);
                Object data = item.get(ITEM_DATA);
                itemClicked(data);
            }

        });

        new AsyncListViewLoader().execute(listview);
    }

    private class AsyncListViewLoader extends AsyncTask<ListView, Void, SeparatedListAdapter> {

        ListView _listView;

        @Override
        protected SeparatedListAdapter doInBackground(ListView... params) {

            Thread.currentThread().setPriority(Thread.MIN_PRIORITY);

            this._listView = params[0];
            SeparatedListAdapter adapter = new SeparatedListAdapter(_listView.getContext());


            BrandDataSource brandDataSource = new BrandDataSource();
            brandDataSource.open();
            List<Brand> brands = brandDataSource.getAllBrands();
            brandDataSource.close();

            Log.d(getClass().getName(), "Got " + brands.size() + " brands");

            //setup user section
            //iconUri = "android.resource://com.cambriantech.paintharmony/" + R.drawable.camera_cell_icon;

            List<Map<String,?>> featuresData = new LinkedList<Map<String,?>>();

            featuresData.add(createItem(FEATURES_LOAD_PHOTO_ID, FEATURES_LOAD_PHOTO_ICON, FEATURES_LOAD_PHOTO_TITLE, FEATURES_LOAD_PHOTO_DESC));
            featuresData.add(createItem(FEATURES_COLOR_PHOTO_ID, FEATURES_COLOR_PHOTO_ICON, FEATURES_COLOR_PHOTO_TITLE, FEATURES_COLOR_PHOTO_DESC));
            featuresData.add(createItem(FEATURES_COLOR_LIBRARY_ID, FEATURES_COLOR_LIBRARY_ICON, FEATURES_COLOR_LIBRARY_TITLE, FEATURES_COLOR_LIBRARY_DESC));
            featuresData.add(createItem(FEATURES_LOAD_EXAMPLE_ID, FEATURES_LOAD_EXAMPLE_ICON, FEATURES_LOAD_EXAMPLE_TITLE, FEATURES_LOAD_EXAMPLE_DESC));

            if (PaintUtility.isLite(context)) {
                featuresData.add(createItem(FEATURES_MORE_ID, FEATURES_MORE_ICON, FEATURES_MORE_TITLE_LITE, FEATURES_MORE_DESC_LITE));
            } else {
                featuresData.add(createItem(FEATURES_MORE_ID, FEATURES_MORE_ICON, FEATURES_MORE_TITLE_PRO, FEATURES_MORE_DESC_PRO));
            }

            adapter.addSection("Features", new IconListItemAdapter(_listView.getContext(), featuresData, R.layout.list_complex,
                    new String[] { ITEM_ICON, ITEM_TITLE, ITEM_CAPTION },
                    new int[] { R.id.list_complex_icon, R.id.list_complex_title, R.id.list_complex_caption }));

            for (Brand brand : brands) {
                List<ColorCategory> categories = brand.getCategories();
                Log.d(getClass().getName(), "Got " + categories.size() + " categories");

                List<Map<String,?>> brandData = new LinkedList<Map<String,?>>();

                if (categories.size() > 0) {
                    for (ColorCategory category : categories) {
                        String iconUri = ASSET_PREFIX + "Brands/" + brand.getIconFileName();
                        brandData.add(createItem(category, iconUri, category.getName(), brand.getName()));
                    }


                    adapter.addSection(brand.getName(), new IconListItemAdapter(_listView.getContext(), brandData, R.layout.list_complex,
                            new String[] { ITEM_ICON, ITEM_TITLE, ITEM_CAPTION },
                            new int[] { R.id.list_complex_icon, R.id.list_complex_title, R.id.list_complex_caption }));
                }
            }

            return adapter;
        }

        @Override
        protected void onPostExecute(SeparatedListAdapter adapter) {
            this._listView.setAdapter(adapter);
        }
    }

    private void itemClicked(Object data)
    {
        if (ColorCategory.class.isInstance(data)) {
            if (_delegate != null) {
                _delegate.onColorCategoryChosen((ColorCategory) data);
            }
        } else {
            int itemId = (Integer) data;
            switch (itemId) {
                case FEATURES_LOAD_PHOTO_ID:
                    if (_delegate != null) {
                        _delegate.onLibraryChosen();
                    }
                    break;
                case FEATURES_COLOR_PHOTO_ID:
                    if (_delegate != null) {
                        _delegate.onCameraColorChosen();
                    }
                    break;
                case FEATURES_COLOR_LIBRARY_ID:
                    if (_delegate != null) {
                        _delegate.onLibraryColorChosen();
                    }
                    break;
                case FEATURES_LOAD_EXAMPLE_ID:
                    if (_delegate != null) {
                        _delegate.onExampleChosen();
                    }
                    break;
                case FEATURES_MORE_ID:
                    if (_delegate != null) {
                        _delegate.onMoreChosen();
                    }
                    break;
                default:
                    break;
            }
        }
    }


    TextWatcher _searchTextChanged = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            String queryString = menu_search_text.getText().toString();

            if (queryString.length() > 1) {
                AsyncPaintQuery query = new AsyncPaintQuery(queryString);
                AsyncPaintQuery.performTask(query, true);
            }
        }
    };

    public class AsyncPaintQuery extends CBRepeatingTask<ColorResultsListAdapter> {

        private String _query;

        public AsyncPaintQuery(String query) {
            this._query = query;
        }

        protected ColorResultsListAdapter ExecuteTask() {
            PaintColorDataSource dataSource = new PaintColorDataSource();
            dataSource.open();
            List<PaintColor> colors = dataSource.searchForColors(this._query);
            dataSource.close();

            ColorResultsListAdapter resultsAdapter = new ColorResultsListAdapter(getContext(), colors);

            return resultsAdapter;
        }

        protected void HandleTaskResult(ColorResultsListAdapter adapter) {
            color_results.setVisibility(View.VISIBLE);
            color_results.setAdapter(adapter);
        }
    }
}
