package com.cambriantech.paintharmony;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.cambriantech.CBCameraActivity;
import com.cambriantech.CBCameraControl;
import com.cambriantech.paintharmony.data.Brand;
import com.cambriantech.paintharmony.data.ColorCategory;
import com.cambriantech.paintharmony.data.PaintColor;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class PaintUtility {

    private final static ScheduledExecutorService _worker = Executors.newSingleThreadScheduledExecutor();
    private static AsyncPaintColorFinder _colorFinder;

    public static boolean isAmazon = false;

    public static final String PRO_NAME = "Paint Harmony";
    public static final String PRO_BUNDLE_ID = "com.cambriantech.paintharmonypro";

    public static final String LITE_NAME = "Paint Harmony";
    public static final String LITE_BUNDLE_ID = "com.cambriantech.paintharmonylite";

    public static final int HIGHLIGHT_COLOR = 0xffffc400;
    public static final int ALT_HIGHLIGHT_COLOR = 0xff00c400;
    public static final int CAMERA_PIC_REQUEST = 1337;
    public static final int LIBRARY_PIC_REQUEST = 1338;

    public final static String COLOR_CHANGED_NOTIFICATION = "ColorChangedNotification";
    public final static String COLOR_CATEGORY_CHANGED_NOTIFICATION = "CategoryChangedNotification";
    public final static String FAVORITES_CHANGED_NOTIFICATION = "FavoritesChangedNotification";

    public final static String SENDER_EXTRA_KEY = "Sender";
    public final static String PAINT_COLOR_EXTRA_KEY = "PaintColor";
    public final static String COLOR_EXTRA_KEY = "Color";
    public final static String COLOR_CATEGORY_EXTRA_KEY = "ColorCategory";

    public static PaintColor _currentColor;

    static boolean isLite(Context context) {
        return context.getPackageName().equals(LITE_BUNDLE_ID);
    }

    static String getProductName(Context context) {
        return isLite(context) ? LITE_NAME : PRO_NAME;
    }

    static PaintColor getCurrentPaintColor()
    {
        return _currentColor;
    }

    static void setCurrentPaintColor(PaintColor paintColor)
    {
        _currentColor = paintColor != null ? paintColor.clone() : null;
    }

    static void broadcastPaintColor(PaintColor paintColor, int color, Context context, Object sender) {

        setCurrentPaintColor(paintColor);

        if (PaintSwatch.currentSwatch != null && PaintSwatch.currentSwatch.getColor() != color) {
            PaintSwatch.currentSwatch.setIsHighlighted(false);
        }

        Intent intent = new Intent(COLOR_CHANGED_NOTIFICATION);

        intent.putExtra(PAINT_COLOR_EXTRA_KEY, paintColor);
        intent.putExtra(COLOR_EXTRA_KEY, color);

        sendBroadcast(intent, context, sender);
//        if (null != paintColor) {
//            broadcastColorCategory(paintColor.getColorCategory(), context, sender);
//        }
    }

    static void broadcastColorCategory(ColorCategory category, Context context, Object sender) {
        Intent intent = new Intent(COLOR_CATEGORY_CHANGED_NOTIFICATION);

        intent.putExtra(COLOR_CATEGORY_EXTRA_KEY, category);

        sendBroadcast(intent, context, sender);
    }

    static void sendBroadcast(Intent intent, Context context, Object sender) {
        intent.putExtra(SENDER_EXTRA_KEY, getClassIdentifier(sender));
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    static boolean isSender(Intent intent, Object object)
    {
        String broadcasterClassName = intent.getStringExtra(SENDER_EXTRA_KEY);
        String objectClassName = getClassIdentifier(object);
        //Log.d("isSender", broadcasterClassName + "=" + objectClassName);
        return broadcasterClassName.equals(objectClassName);
    }

    private static String getClassIdentifier(Object object) {
        Class oClass = object.getClass();

        String className = oClass.getName();

        if (className.contains("$")) {
            return className.substring(0, className.indexOf("$"));
        }
        return oClass.getName();
    }

    private static Uri makePhotoUri(Activity activity) {
        String rootDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();
        String fileName = rootDirectory + "/" + activity.getClass().getSimpleName().concat(".png");
        Uri fileUri = Uri.fromFile(new File(fileName));
        return fileUri;
    }

    public static Uri launchCameraIntent(Activity activity) {
        //take photo
        if (!CBCameraControl.hasCamera(activity.getBaseContext())) {
            launchLibraryIntent(activity);
            return null;
        }

        Intent cameraIntent = new Intent(activity, CBCameraActivity.class);
        //Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        Uri fileUri = makePhotoUri(activity);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        activity.getIntent().putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        activity.startActivityForResult(cameraIntent, PaintUtility.CAMERA_PIC_REQUEST);

        return fileUri;
    }

    public static void launchLibraryIntent(Activity activity) {
        Intent libraryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        activity.startActivityForResult(libraryIntent, LIBRARY_PIC_REQUEST);
    }

    public static Uri getPhotoUri(Activity activity, int requestCode, int resultCode, Intent data) {

        if (resultCode != Activity.RESULT_OK
                || (requestCode != PaintUtility.CAMERA_PIC_REQUEST && requestCode != PaintUtility.LIBRARY_PIC_REQUEST)) {
            return null;
        }

        if (requestCode == PaintUtility.LIBRARY_PIC_REQUEST) {
            return data.getData();
        } else {
            Uri imageUri = data.getParcelableExtra(MediaStore.EXTRA_OUTPUT);
            //Log.d("JOEL", "HERE " + imageUri);
            return imageUri;
        }
    }

    public static int[] getColorComponents(int hexColor)
    {
        int[] color = new int[4];
        color[0] = (hexColor & 0x00FF0000) >> 16; //R
        color[1] = (hexColor & 0x0000FF00) >> 8; //G
        color[2] = (hexColor & 0x000000FF); //B
        color[3] = (hexColor & 0xFF000000) >> 24; //A

        return color;
    }

    public static float getScreenDensity(Context context)
    {
        return context.getResources().getDisplayMetrics().density;
    }

    public static Point getScreenSize(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        Display display = wm.getDefaultDisplay();

        Point size = new Point();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(size);
        } else {
            size.set(display.getWidth(), display.getHeight());
        }

        return size;
    }

    public static double getColorDistance(int colorA, int colorB) {
        int[] colorComponentsA = PaintUtility.getColorComponents(colorA);
        int[] colorComponentsB = PaintUtility.getColorComponents(colorB);

        return Math.sqrt(Math.pow(colorComponentsB[0]-colorComponentsA[0],2)
                        + Math.pow(colorComponentsB[1]-colorComponentsA[1],2)
                        + Math.pow(colorComponentsB[2]-colorComponentsA[2],2));
    }

    static final int BUFFER_SIZE = 1024 * 8;
    static boolean saveAsJpeg(Bitmap bitmap, File file) {
        try {
            boolean success = file.createNewFile();
            if (success) {
                FileOutputStream fos = new FileOutputStream(file);
                final BufferedOutputStream bos = new BufferedOutputStream(fos, BUFFER_SIZE);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                bos.flush();
                bos.close();
                fos.close();
                return true;
            } else {
                Log.d(PaintUtility.class.getSimpleName(), "Could not create file " + file.getAbsolutePath());
                return false;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private static void galleryAddPic(Context context, File file) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        context.sendBroadcast(mediaScanIntent);
    }

    public static boolean savePhoto(Context context, Bitmap bitmap) {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        //MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, title, description);
        String directoryName = getProductName(context);
        File directory = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                directoryName
        );

        boolean success = true;
        if (!directory.exists()) {
            success = directory.mkdir();
        }

        if (!success) {
            return false;
        }

        File file = new File(directory,  "PH_"+ timeStamp + ".jpg");

        success = saveAsJpeg(bitmap, file);
        if (success) {
            galleryAddPic(context, file);
        }
        return success;
    }

    public static ProgressDialog showProgressDialog(Context context, String message) {
        ProgressDialog mDialog = new ProgressDialog(context);
        mDialog.setMessage(message);
        mDialog.setCancelable(false);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        return mDialog;
    }


    public static void hideDialogAfterDelay(final Dialog dialog, long milliseconds) {
        Runnable task = new Runnable() {
            public void run() {
                dialog.hide();
            }
        };
        _worker.schedule(task, milliseconds, TimeUnit.MILLISECONDS);
    }

    public static void findColorAsync(final AsyncPaintColorFinder.Handler handler, final int color, boolean required)
    {
        findColorAsync(handler, color, null, null, required);
    }

    public static void findColorAsync(final AsyncPaintColorFinder.Handler handler,
                                      final int color,
                                      final Brand brand,
                                      final ColorCategory category,
                                      boolean required)
    {
        AsyncPaintColorFinder.performTask(new AsyncPaintColorFinder(handler, color, brand, category), required);
    }

    public static String join(Collection<?> s, String delimiter) {
        StringBuilder builder = new StringBuilder();
        Iterator<?> iter = s.iterator();
        while (iter.hasNext()) {
            builder.append(iter.next());
            if (!iter.hasNext()) {
                break;
            }
            builder.append(delimiter);
        }
        return builder.toString();
    }

    public static void setViewBackground(View view, Drawable drawable) {
        // Make sure we're running on JELLY_BEAN or higher to use APIs
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackground(drawable);
        } else {
            view.setBackgroundDrawable(drawable);
        }
    }

    public static boolean isLandscape(Context context) {
        int orientation = context.getResources().getConfiguration().orientation;
        return orientation == Configuration.ORIENTATION_LANDSCAPE;
    }
}
