package com.cambriantech.paintharmony;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import com.cambriantech.paintharmony.R;
import com.cambriantech.paintharmony.data.ColorCategory;
import com.cambriantech.paintharmony.data.PaintColor;
import com.cambriantech.paintharmony.data.PaintColorDataSource;

import java.util.ArrayList;
import java.util.List;

public class SwatchesScroller extends LinearLayout {

    private ColorCategory _colorCategory;
    private static final int TOTAL_ROWS = 4;
    private static final int PROCESS_COLUMNS = 5;
    private int _swatch_size = 80;

    private HorizontalScrollView _scrollView;
    private LinearLayout _swatches_scroller_content;
    private static boolean _busyLoading;

    public SwatchesScroller(Context context) {
        super(context);

        inflate(context);
    }

    public SwatchesScroller(Context context, AttributeSet attrs) {
        super(context, attrs);

        inflate(context);
    }

    @SuppressLint("NewApi")
    public SwatchesScroller(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        inflate(context);
    }

    private void inflate(Context context)
    {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(inflater != null){
            inflater.inflate(R.layout.swatches_scroller, this);
        }

        Point screenSize = PaintUtility.getScreenSize(getContext());
        _swatch_size = screenSize.y / 15;

        _scrollView = (HorizontalScrollView) findViewById(R.id.swatches_scroller);
        _swatches_scroller_content = (LinearLayout) findViewById(R.id.swatches_scroller_content);
        _swatches_scroller_content.setMinimumHeight(_swatch_size * TOTAL_ROWS);
    }

    void setColorCategory(ColorCategory colorCategory) {
        if (_colorCategory == colorCategory) return;
        _colorCategory = colorCategory;
        refresh();
    }

    private class AsyncColorLoader extends AsyncTask<ColorCategory, ArrayList<View>, ArrayList<View>> {

        @Override
        protected ArrayList<View> doInBackground(ColorCategory... params) {

            ColorCategory category = params[0];

            int index = 0;
            LinearLayout verticalLayout = null;

            List<PaintColor> colors = category.getPaintColors();

            ArrayList<View> views = new ArrayList<View>();
            int colorViewCount = 0;

            for (PaintColor color : colors) {

                if (verticalLayout == null || index % TOTAL_ROWS == 0) {
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(_swatch_size, TOTAL_ROWS * _swatch_size);
                    verticalLayout = new LinearLayout(getContext());
                    verticalLayout.setOrientation(LinearLayout.VERTICAL);
                    verticalLayout.setLayoutParams(layoutParams);
                    views.add(verticalLayout);
                }

                PaintSwatch colorView = new PaintSwatch(getContext());
                colorView.setPaintColor(color);
                views.add(colorView);
                colorViewCount ++;

                if (PROCESS_COLUMNS > 0 && colorViewCount == PROCESS_COLUMNS * TOTAL_ROWS) {
                    publishProgress(views);
                    colorViewCount = 0;
                    views = new ArrayList<View>();
                }
                index ++;
            }
            return views;
        }

        protected void onProgressUpdate(ArrayList<View>... params) {
            ArrayList<View> views = params[0];

            addColorViews(views);
            //_swatches_scroller_content.invalidate();
        }

        @Override
        protected void onPostExecute(ArrayList<View> views) {

            addColorViews(views);

            _swatches_scroller_content.invalidate();
            _busyLoading = false;
        }

        private void addColorViews(List<View> views) {

            LinearLayout verticalLayout = null;
            for (View view : views) {
                if (PaintSwatch.class.isInstance(view)) {
                    verticalLayout.addView(view, _swatch_size, _swatch_size);
                } else {
                    if (verticalLayout != null) {
                        _swatches_scroller_content.addView(verticalLayout, _swatch_size, TOTAL_ROWS * _swatch_size);
                    }
                    verticalLayout = (LinearLayout) view;
                }
            }

            if (verticalLayout != null) {
                _swatches_scroller_content.addView(verticalLayout, _swatch_size, TOTAL_ROWS * _swatch_size);
            }
            _swatches_scroller_content.invalidate();
        }
    }

    void refresh() {
        if (_busyLoading) {
            return;
        }
        _swatches_scroller_content.removeAllViews();
        _scrollView.scrollTo(0,0);

        _busyLoading = true;
        AsyncColorLoader loader = new AsyncColorLoader();
        loader.execute(_colorCategory);
    }
}
