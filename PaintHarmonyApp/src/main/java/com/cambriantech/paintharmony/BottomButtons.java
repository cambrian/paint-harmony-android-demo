package com.cambriantech.paintharmony;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.cambriantech.paintharmony.data.PaintColor;

/**
 * Created by joelteply on 7/29/13.
 */
public class BottomButtons extends LinearLayout {

    //context menu, tool menu
    PaintButton _paint_camera_button;
    PaintButton _paint_undo_button;
    PaintButton _paint_tool_button;
    PaintButton _paint_brush_size_button;
    PaintButton _paint_layers_button;
    PaintButton _paint_save_button;

    public interface BottomButtonsListener {
        void onCameraButtonClicked();
        void onUndoButtonClicked();
        void onToolButtonClicked();
        void onBrushSizeButtonClicked();
        void onLayersButtonClicked();
        void onSaveButtonClicked();
    }

    BottomButtonsListener _listener;
    public void setListener(BottomButtonsListener listener) {
        _listener = listener;
    }

    private void inflate(Context context)
    {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(inflater != null){
            inflater.inflate(R.layout.bottom_buttons, this);
        }

        //context menu, tool menu
        _paint_camera_button = (PaintButton) findViewById(R.id.paint_camera_button);
        _paint_camera_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _listener.onCameraButtonClicked();
            }
        });

        //context menu, tool menu
        _paint_undo_button = (PaintButton) findViewById(R.id.paint_undo_button);
        _paint_undo_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _listener.onUndoButtonClicked();
            }
        });
        _paint_undo_button.setEnabled(false);

        //context menu, tool menu
        _paint_tool_button = (PaintButton) findViewById(R.id.paint_tool_button);
        _paint_tool_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _listener.onToolButtonClicked();
            }
        });

        _paint_brush_size_button = (PaintButton) findViewById(R.id.paint_brush_size_button);
        _paint_brush_size_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //show brush size slider
                _listener.onBrushSizeButtonClicked();
            }
        });
        _paint_brush_size_button.setEnabled(false);

        _paint_layers_button = (PaintButton) findViewById(R.id.paint_layers_button);
        _paint_layers_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _listener.onLayersButtonClicked();
            }
        });

        //context menu, tool menu
        _paint_save_button = (PaintButton) findViewById(R.id.paint_brush_save_button);
        _paint_save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _listener.onSaveButtonClicked();
            }
        });

        setClickable(true);
    }

    public BottomButtons(Context context) {
        super(context);

        inflate(context);
    }

    public BottomButtons(Context context, AttributeSet attrs) {
        super(context, attrs);

        inflate(context);
    }

    @SuppressLint("NewApi")
    public BottomButtons(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        inflate(context);
    }

    @Override
    public void setClickable(boolean clickable) {
        super.setClickable(clickable);
        _paint_camera_button.setClickable(clickable);
        _paint_undo_button.setClickable(clickable);
        _paint_tool_button.setClickable(clickable);;
        _paint_brush_size_button.setClickable(clickable);;
        _paint_save_button.setClickable(clickable);;
        _paint_tool_button.setClickable(clickable);
    }
}
