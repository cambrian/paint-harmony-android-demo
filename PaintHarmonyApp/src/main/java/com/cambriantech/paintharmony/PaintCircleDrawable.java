package com.cambriantech.paintharmony;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LayerRasterizer;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

/**
 * Created by joelteply on 7/12/13.
 */
public class PaintCircleDrawable extends Drawable {
    private Paint paint;
    private int radiusOffset;
    private Point centerOffset;

    public PaintCircleDrawable(Point centerOffset, int radiusOffset) {
        this.paint = new Paint();
        this.paint.setColor(Color.RED);

        this.centerOffset = centerOffset;
        this.radiusOffset = radiusOffset;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawCircle(canvas.getWidth() / 2 + centerOffset.x,
                          canvas.getHeight() / 2 + centerOffset.y,
                          canvas.getWidth() / 2 + this.radiusOffset, this.paint);
    }

    public int getColor() {
        return this.paint.getColor();
    }

    public void setColor(int color) {
        this.paint.setColor(color);
    }

    @Override
    public void setAlpha(int i) {
        this.paint.setAlpha(i);
    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {
       this.paint.setColorFilter(colorFilter);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSPARENT;
    }
}