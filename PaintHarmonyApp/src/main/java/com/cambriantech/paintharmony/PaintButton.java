package com.cambriantech.paintharmony;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;

/**
 * Created by joelteply on 7/20/13.
 */
class PaintButton extends Button {

    android.view.View.OnClickListener _listener;
    private AlphaAnimation _alphaUp;

    private void init() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            _alphaUp = new AlphaAnimation(0.3f, 1.0f);
            _alphaUp.setDuration(200);
            _alphaUp.setFillAfter(true);
            setAnimation(_alphaUp);

            super.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    _alphaUp.start();
                    startAnimation(_alphaUp);
                    _listener.onClick(view);
                }
            });
        } else {
            super.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    _listener.onClick(view);
                }
            });
        }
    }

    public PaintButton(Context context) {
        super(context);
        init();
    }

    public PaintButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PaintButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (isEnabled() == enabled) return;

        super.setEnabled(enabled);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            setAlpha(enabled ? 1.0f : 0.5f);
            if (enabled) {
                setAnimation(_alphaUp);
            } else {
                clearAnimation();
            }
        }
    }

    public void setOnClickListener(android.view.View.OnClickListener listener) {
        _listener = listener;
    }
}

