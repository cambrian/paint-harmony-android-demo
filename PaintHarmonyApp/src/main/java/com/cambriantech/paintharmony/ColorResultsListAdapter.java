package com.cambriantech.paintharmony;

import java.util.List;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.cambriantech.paintharmony.data.PaintColor;

public class ColorResultsListAdapter extends ArrayAdapter<PaintColor> {
    private final Context context;
    private final List<PaintColor> colors;

    public ColorResultsListAdapter(Context context, List<PaintColor> colors) {
        super(context, R.layout.color_list_item);
        this.context = context;
        this.colors = colors;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.color_list_item, parent, false);

        PaintColor color = colors.get(position);
        View color_list_swatch = rowView.findViewById(R.id.color_list_swatch);
        color_list_swatch.setBackgroundColor(color.getColor());

        TextView color_list_brand = (TextView) rowView.findViewById(R.id.color_list_brand);
        color_list_brand.setText(color.getColorCategory().getBrand().getName() + " - " + color.getColorCategory().getName());

        TextView color_list_name = (TextView) rowView.findViewById(R.id.color_list_name);
        color_list_name.setText(color.getName() + " #" + color.getCode());

        return rowView;
    }

    public int getCount()
    {
        return this.colors.size();
    }

    public PaintColor getItem(int position)
    {
        return this.colors.get(position);
    }
}