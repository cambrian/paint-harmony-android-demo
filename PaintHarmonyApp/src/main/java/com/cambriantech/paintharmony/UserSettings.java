package com.cambriantech.paintharmony;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by joelteply on 8/25/13.
 */
public class UserSettings {
    private static UserSettings ourInstance;

    private SharedPreferences settings;

    public static UserSettings getInstance() {
        return ourInstance;
    }

    public static void initialize(Context context) {
        ourInstance = new UserSettings(context);
    }

    private UserSettings(Context context) {
        settings = context.getSharedPreferences("PAINT_HARMONY", Context.MODE_PRIVATE);

////get the sharepref
//        int id = settings.getInt("ID", 0);
//
////set the sharedpref
//        Editor editor = settings.edit();
//        editor.putInt("ID", "1");
//        editor.commit();
    }

    public boolean hasSeenLayerDirections() {
        return this.settings.getBoolean("hasSeenLayerDirections", false);
    }

    public void setHasSeenLayerDirections(boolean value) {
        SharedPreferences.Editor editor = this.settings.edit();
        editor.putBoolean("hasSeenLayerDirections", value);
        editor.commit();
    }

    public boolean hasSeenTutorial() {
        return this.settings.getBoolean("hasSeenTutorial", false);
    }

    public void setHasSeenTutorial(boolean value) {
        SharedPreferences.Editor editor = this.settings.edit();
        editor.putBoolean("hasSeenTutorial", value);
        editor.commit();
    }
}
