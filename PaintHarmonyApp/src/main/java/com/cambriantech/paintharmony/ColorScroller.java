package com.cambriantech.paintharmony;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import com.cambriantech.paintharmony.data.Brand;
import com.cambriantech.paintharmony.data.ColorCategory;
import com.cambriantech.paintharmony.data.PaintColor;

import java.util.ArrayList;
import java.util.List;

public class ColorScroller extends LinearLayout {
    private List<PaintColor> _colors;
    private static int _swatch_size;

    private HorizontalScrollView _scrollView;
    private LinearLayout _color_scroller_content;

    private boolean _matchBrand;
    private boolean _matchCategory;

    public ColorScroller(Context context) {
        super(context);

        inflate(context);
    }

    public ColorScroller(Context context, AttributeSet attrs) {
        super(context, attrs);

        inflate(context);
    }

    public boolean getMatchBrand() {
        return _matchBrand;
    }

    public void setMatchBrand(boolean match) {
        _matchBrand = match;
    }

    public boolean getMatchCategory() {
        return _matchCategory;
    }

    public void setMatchCategory(boolean match) {
        _matchCategory = match;
    }

    @SuppressLint("NewApi")
    public ColorScroller(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        inflate(context);
    }

    private void inflate(Context context)
    {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(inflater != null){
            inflater.inflate(R.layout.color_scroller, this);
        }

        _scrollView = (HorizontalScrollView) findViewById(R.id.color_scroller);
        _color_scroller_content = (LinearLayout) findViewById(R.id.color_scroller_content);

        float density = PaintUtility.getScreenDensity(context);

        _swatch_size = (int) (density * 100.0f);
    }

    void setColors(List<PaintColor> colors) {
        _colors = colors;
        refresh();
    }

    void refresh() {
        int index = 0;

        _color_scroller_content.removeAllViews();
        _scrollView.scrollTo(0,0);

        Brand brand = null;
        ColorCategory category = null;

        PaintColor currentColor = PaintUtility.getCurrentPaintColor();

        if (currentColor != null) {
            if (getMatchBrand()) {
                brand = currentColor.getColorCategory().getBrand();
            }
            if (getMatchCategory()) {
                category = currentColor.getColorCategory();
            }
        }

        for (PaintColor color : _colors) {

            PaintSwatch swatch = new PaintSwatch(getContext(), brand, category);
            swatch.setPaintColor(color);

            _color_scroller_content.addView(swatch, _swatch_size, _swatch_size);
        }
    }
}
