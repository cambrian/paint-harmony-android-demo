package com.cambriantech.paintharmony;



import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cambriantech.CBImagePainter;
import com.cambriantech.paintharmony.MenuTabs.MenuTabsListener;
import com.cambriantech.paintharmony.data.ColorCategory;
import com.cambriantech.paintharmony.data.CoreSQL;
import com.cambriantech.paintharmony.data.PaintColor;
import com.cambriantech.paintharmony.data.PaintColorDataSource;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

import java.util.Timer;
import java.util.TimerTask;

public abstract class PaintActivity extends BaseActivity implements MenuTabsListener,
        MenuSidebar.MenuSidebarListener,
        CBImagePainter.CBImagePainterListener,
        BottomButtons.BottomButtonsListener,
        MenuButtons.MenuButtonsListener,
        LayerMenuItem.LayerMenuItemListener,
        EditLayerMenuItem.EditLayerMenuItemListener {

    private final String TAG = "PaintActivity";
    private final String EMAIL_FEEDBACK_ADDRESS = "contact@cambriantech.com";
    private final String EMAIL_FEEDBACK_SUBJECT = " Suggestion - Android";

    private boolean commitInterrupted;

    private ViewGroup paint_tutorial;
    private Button paint_tutorial_button;
    private LinearLayout paint_fullscreen_content;
    private CBImagePainter painter;
    private PanelSlideAnimation drawerAnimation;
    private AlphaAnimation alphaUp;

    private MenuTabs paint_tabs;
    private MenuSidebar paint_drawer;

    private Button paint_color_name_button;
    private View paint_color_view;
    private ImageButton paint_color_details_button;
    private AdView paint_ads;

    private ViewGroup _paint_rect_options_view;
    private ImageButton _paint_rect_options_commit_button;
    private ImageButton _paint_rect_options_decommit_button;

    private ViewGroup _paint_brush_size;
    private SeekBar _paint_brush_size_seekBar;
    private ImageView _paint_brush_image;
    private PaintCheckBox _paint_brush_auto_checkbox;
    private PaintCheckBox _paint_brush_smart_checkbox;

    private BottomButtons _bottomButtons;
    private MenuButtons _paint_menu_buttons;

    private ViewGroup layers_menu;
    private ViewGroup layers_menu_content;
    private Button layers_menu_add_button;
    private Button layers_menu_edit_button;

    private ViewGroup layers_edit_menu;
    private ViewGroup layers_edit_menu_content;
    private Button layers_edit_menu_cancel_button;

    //Your License Key:
    @Override
    public abstract String getCBLicenseKey();

    @Override
    public void onHistoryChanged() {
        _bottomButtons._paint_undo_button.setEnabled(painter.canStepBackward());
        toggleBrushSize(false);
        closeOptionsMenu();
    }

    private final int MIN_FILL_THRESHOLD = 2;

    @Override
    public void onStartedTool(int toolMode) {

        finishedCommitting();

        if (toolMode==CBImagePainter.PAINT_TOOL_RECTANGLE) {
            _paint_rect_options_view.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onFinishedTool(int toolMode) {

        finishedCommitting();

        if (toolMode==CBImagePainter.PAINT_TOOL_RECTANGLE) {
            _paint_rect_options_view.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public boolean shouldStartTool(int toolMode) {
        return true;
    }

    private Timer _scheduledCommit;
    private void scheduleCommit(long delay) {
        commitInterrupted = false;
        if (_scheduledCommit != null) {
            _scheduledCommit.cancel();
        }
        _scheduledCommit = new Timer();
        _scheduledCommit.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!commitInterrupted) {
                            commitChanges();
                        }
                    }
                });
            }
        }, delay);
    }

    private void commitChanges() {
        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    painter.commitChanges();
                    finishedCommitting();
                }
                catch (Exception e) {
                    android.util.Log.e("TAG",
                            "Exception committing", e);
                    // TODO get this out to library clients
                }
            }
        }, 250);
    }

    private void decommitChanges() {
        painter.decommitChanges();
        finishedCommitting();
    }

    private void finishedCommitting() {
        commitInterrupted = false;
        if (_scheduledCommit != null) {
            _scheduledCommit.cancel();
            _scheduledCommit = null;
        }
        if (painter.getToolMode()==CBImagePainter.PAINT_TOOL_FILL) {
            _paint_rect_options_view.setVisibility(View.GONE);
        }
    }

    /**
     * Interface definition for a callback to be invoked when a view is clicked.
     */
    public interface PaintActivityListener {
        void onColorChanged();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate");

        UserSettings.initialize(getBaseContext());
        CoreSQL.initialize(getBaseContext());

        setContentView(R.layout.activity_paint);

        this.painter = (CBImagePainter) findViewById(R.id.paint_image_view);
        this.painter.setToolMode(CBImagePainter.PAINT_TOOL_PAINT);
        this.painter.setCBImagePainterListener(this);

        paint_fullscreen_content = (LinearLayout) findViewById(R.id.paint_fullscreen_content);
        if (paint_fullscreen_content.isInEditMode()) {
            return;
        }

        paint_ads = (AdView) findViewById(R.id.paint_ads);
        if (PaintUtility.isLite(getBaseContext())) {
            AdRequest adRequest = new AdRequest();
            adRequest.addKeyword("home depot behr diy sherwin williams dunn edwards hardware lowes decorator design");
            paint_ads.loadAd(adRequest);
        } else {
            paint_ads.setVisibility(View.GONE);
        }

        //alpha effect
        alphaUp = new AlphaAnimation(0.3f, 1.0f);
        alphaUp.setDuration(200);
        alphaUp.setFillAfter(true);

        paint_tutorial = (ViewGroup) findViewById(R.id.paint_tutorial);
        paint_tutorial.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        if (UserSettings.getInstance().hasSeenTutorial()) {
            paint_tutorial.setVisibility(View.GONE);
        }
        paint_tutorial_button = (Button) findViewById(R.id.paint_tutorial_button);
        paint_tutorial_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paint_tutorial.setVisibility(View.GONE);
                UserSettings.getInstance().setHasSeenTutorial(true);
            }
        });

        paint_color_name_button = (Button) findViewById(R.id.paint_color_name_button);
        paint_color_view = (View) findViewById(R.id.paint_color_view);
        paint_color_details_button = (ImageButton) findViewById(R.id.paint_color_details_button);

        paint_tabs  =  (MenuTabs) findViewById(R.id.paint_tabs);
        paint_tabs.setMenuTabsListener(this);

        paint_drawer  =  (MenuSidebar) findViewById(R.id.paint_drawer);
        //setup width
        int widthAvailable = PaintUtility.getScreenSize(getBaseContext()).x;
        float density = PaintUtility.getScreenDensity(getBaseContext());
        float deviceWidth = ((float) widthAvailable) / density;

        if (PaintUtility.isLandscape(getBaseContext())) {
            int menuWidth =  (int) (300.0f * density);
            int rightMargin = widthAvailable - menuWidth;
            FrameLayout.LayoutParams pdLayoutParams = (FrameLayout.LayoutParams) paint_drawer.getLayoutParams();
            pdLayoutParams.setMargins(pdLayoutParams.leftMargin, pdLayoutParams.topMargin, rightMargin, pdLayoutParams.bottomMargin);
            paint_drawer.setVisibility(View.VISIBLE);
            paint_drawer.setLayoutParams(pdLayoutParams);

            FrameLayout.LayoutParams fLayoutParams = (FrameLayout.LayoutParams) paint_fullscreen_content.getLayoutParams();
            fLayoutParams.setMargins(menuWidth, fLayoutParams.topMargin, 0, fLayoutParams.bottomMargin);
            paint_fullscreen_content.setLayoutParams(fLayoutParams);
        }

        paint_drawer.setMenuSidebarListener(this);



        if (PaintSwatch.currentSwatch != null) {
            PaintSwatch.currentSwatch.setIsHighlighted(false);
        }

        //Setup drawer
        paint_fullscreen_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerIsOpen()) {
                    toggleDrawer(false);
                }
            }
        });

        drawerAnimation = new PanelSlideAnimation(paint_fullscreen_content, PanelSlideAnimation.LEFT_MARGIN);
        drawerAnimation.setAnimationListener(new Animation.AnimationListener(){
            @Override
            public void onAnimationStart(Animation arg0) {
                paint_drawer.setVisibility(View.VISIBLE);
                if (drawerAnimation.endOffset != 0) {
                    paint_drawer.willOpen();
                } else {
                    paint_drawer.willClose();
                }
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {
            }
            @Override
            public void onAnimationEnd(Animation arg0) {
                if (drawerAnimation.endOffset == 0) {
                    drawerClosed();
                } else {
                    drawerOpened();
                }
            }
        });



        _bottomButtons = (BottomButtons) findViewById(R.id.paint_bottom_buttons);
        _bottomButtons.setListener(this);

        _paint_menu_buttons = (MenuButtons) findViewById(R.id.paint_menu_buttons);
        _paint_menu_buttons.setListener(this);


        _paint_rect_options_view = (ViewGroup) findViewById(R.id.paint_rect_options_view);

        _paint_rect_options_commit_button = (ImageButton) findViewById(R.id.paint_rect_options_commit_button );
        _paint_rect_options_commit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _paint_rect_options_view.setVisibility(View.INVISIBLE);
                commitChanges();
            }
        });

        _paint_rect_options_decommit_button = (ImageButton) findViewById(R.id.paint_rect_options_decommit_button);
        _paint_rect_options_decommit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _paint_rect_options_view.setVisibility(View.INVISIBLE);
                decommitChanges();
            }
        });

        _paint_brush_size  = (ViewGroup) findViewById(R.id.paint_brush_size);
        _paint_brush_size_seekBar  = (SeekBar) findViewById(R.id.paint_brush_size_seekBar);
        _paint_brush_size_seekBar.setMax(this.painter.getMaxBrushSize() - this.painter.getMinBrushSize());
        _paint_brush_size_seekBar.setOnSeekBarChangeListener(_brushSizeChanged);
        _paint_brush_image = (ImageView) findViewById(R.id.paint_brush_image);
        _paint_brush_auto_checkbox = (PaintCheckBox) findViewById(R.id.paint_brush_auto_checkbox);
        _paint_brush_auto_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                painter.setAutoBrushSizeEnabled(isChecked);
                updateBrushSizeOptions();
            }
        });

        _paint_brush_smart_checkbox = (PaintCheckBox) findViewById(R.id.paint_brush_smart_checkbox);
        _paint_brush_smart_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                painter.setSmartBrushEnabled(isChecked);
            }
        });

        layers_menu = (LinearLayout) findViewById(R.id.paint_layers_menu);
        layers_menu_content = (LinearLayout) findViewById(R.id.paint_layers_menu_content);
        layers_menu_add_button = (Button) findViewById(R.id.paint_layers_menu_add_button);
        layers_menu_add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeAllOverlays(null);
                appendNewLayer();
            }
        });
        layers_menu_edit_button = (Button) findViewById(R.id.paint_layers_menu_edit_button);
        layers_menu_edit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeAllOverlays(layers_edit_menu);
                showLayersEditMenu(true);
            }
        });

        layers_edit_menu = (LinearLayout) findViewById(R.id.paint_layers_edit_menu);
        layers_edit_menu_content = (LinearLayout) findViewById(R.id.paint_layers_edit_menu_content);
        layers_edit_menu_cancel_button = (Button) findViewById(R.id.paint_layers_edit_menu_cancel_button);
        layers_edit_menu_cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeAllOverlays(null);
            }
        });

        updateBrushSizeOptions();
    }

    SeekBar.OnSeekBarChangeListener _brushSizeChanged = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            painter.setBrushSize(seekBar.getProgress() + painter.getMinBrushSize());
            displayBrushSize();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {}

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {}
    };

    private void showLayersMenu(boolean show) {
        if (show) {
            closeAllOverlays(null);
            buildLayersMenu();
        }

        layers_menu.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }

    private void buildLayersMenu() {

        layers_menu_content.removeAllViews();
        int currentLayerIndex = painter.editLayerIndex();

        for (int i=painter.numLayers()-1; i>0; i--) {

            boolean isCurrentLayer = i == currentLayerIndex;

            int currentColor = painter.getPaintColorAtIndex(i);
            LayerMenuItem menuItem = new LayerMenuItem(getBaseContext());

            PaintColor paintColor = PaintColorDataSource.getPaintColor(painter, i);

            menuItem.setInfo(this, i, currentColor, getColorDescription(paintColor, currentColor));
            menuItem.setSelected(isCurrentLayer);

            layers_menu_content.addView(menuItem);
        }

        layers_menu_edit_button.setVisibility(painter.numLayers() > 2 ? View.VISIBLE : View.GONE);
    }

    private String getColorDescription(PaintColor paintColor, int color) {
        if (paintColor != null) return paintColor.getName();

        String hexColor = String.format("#%06X", (0xFFFFFF & color));
        return "RGB: " + hexColor;
    }

    public void LayerMenuItem_Selected(int index) {
        showLayersMenu(false);
        setLayerIndex(index);
    }

    private void showLayersEditMenu(boolean show) {
        if (show) {
            closeAllOverlays(null);
            buildLayersEditMenu();
        }

        layers_edit_menu.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void setLayerIndex(int index) {
        painter.setEditLayerIndex(index);
        //long paintColorID = _project.getLayerPaintColorID(painter, index);
        //long surfaceDefaultID = _project.getLayerSurfaceID(painter, index);

        //setPaintColor(paintColor, true);
    }

    private void buildLayersEditMenu() {

        layers_edit_menu_content.removeAllViews();
        int currentLayerIndex = painter.editLayerIndex();

        for (int i=painter.numLayers()-1; i>0; i--) {

            boolean isCurrentLayer = i == currentLayerIndex;

            PaintColor paintColor = PaintColorDataSource.getPaintColor(painter, i);

            int currentColor = painter.getPaintColorAtIndex(i);

            EditLayerMenuItem menuItem = new EditLayerMenuItem(getBaseContext());
            menuItem.setInfo(this, i, currentColor, getColorDescription(paintColor, currentColor));

            layers_edit_menu_content.addView(menuItem);
        }
    }

    public void EditLayerMenuItem_Deleted(int index) {
        showLayersEditMenu(false);
        painter.removeLayerAtIndex(index);
    }

    private void appendNewLayer() {
        if (!painter.canAppendNewLayer()) return;

        painter.appendNewLayer();

        if (UserSettings.getInstance().hasSeenLayerDirections()) {
            Toast toast = Toast.makeText(this, "A new color layer has been created.", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER,0,0);
            toast.show();
        } else {
            //alert
            AlertDialog.Builder alert = new AlertDialog.Builder(this);

            alert.setTitle("New Layer Created");

            TextView myMsg = new TextView(this);

            myMsg.setText("A new color layer has been created.\n" +
                    "Changes in color will not affect the existing paint below it.\n" +
                    "Feel free to select a new color and paint either new walls or existing areas.");
            myMsg.setPadding(10, 10, 10, 10);
            myMsg.setMinimumHeight(300);

            myMsg.setGravity(Gravity.LEFT);

            alert.setView(myMsg);

            alert.setNeutralButton("OK", null);
            alert.show();
            UserSettings.getInstance().setHasSeenLayerDirections(true);
        }

        paint_tabs.openTab(paint_tabs.MENU_TAB_SWATCHES, true);
    }

    private void displayBrushSize() {
        int size = painter.getBrushSize();

        if (_paint_brush_image.getVisibility() == View.VISIBLE) {
            float imageWidth = _paint_brush_image.getMeasuredWidth();
            float normalizedSize = ((float) size) / (float) painter.getMaxBrushSize();
            int padding = (int) (imageWidth * (1.0f - normalizedSize)) / 2;
            _paint_brush_image.setPadding(padding, padding, padding, padding);
            _paint_brush_image.invalidate();
        }
    }

    private void updateBrushSizeOptions() {
        boolean enabled = !painter.autoBrushSizeEnabled();
        _paint_brush_size_seekBar.setEnabled(enabled);
        _paint_brush_size_seekBar.setAlpha(enabled ? 1.0f : 0.3f);
        _paint_brush_image.setAlpha(enabled ? 1.0f : 0.3f);
    }

    private void toggleBrushSize(boolean show) {
        if (show) {
            _paint_brush_size.setVisibility(View.VISIBLE);
            paint_color_name_button.setVisibility(View.GONE);
            paint_color_view.setVisibility(View.GONE);
            paint_color_details_button.setVisibility(View.GONE);
            _paint_brush_size_seekBar.setProgress(painter.getBrushSize() - painter.getMinBrushSize());
            _paint_brush_size_seekBar.setEnabled(!painter.autoBrushSizeEnabled());
            _paint_brush_auto_checkbox.setChecked(painter.autoBrushSizeEnabled());
            _paint_brush_smart_checkbox.setChecked(painter.smartBrushEnabled());

            displayBrushSize();
        } else {
            _paint_brush_size.setVisibility(View.GONE);

            if (PaintUtility.getCurrentPaintColor() != null) {
                paint_color_name_button.setVisibility(View.VISIBLE);
                paint_color_details_button.setVisibility(View.VISIBLE);
            }
            paint_color_view.setVisibility(View.VISIBLE);
        }
    }

    private void saveImage()
    {
        //save changes
        Bitmap image = painter.getImageBitmap();

        String description = null;
        PaintColor currentColor = PaintUtility.getCurrentPaintColor();

        if (null != currentColor) {
            ColorCategory category = currentColor.getColorCategory();
            description = category.getBrand().getName() + ", " + category.getName() + " - " + currentColor.getName();
        }

        final ProgressDialog dialog = PaintUtility.showProgressDialog(PaintActivity.this, "Saving to your gallery");
        dialog.show();

        //add branding
        Bitmap mutableBitmap = image.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(mutableBitmap);
        addBranding(canvas, description);

        PaintUtility.savePhoto(PaintActivity.this, mutableBitmap);
        PaintUtility.hideDialogAfterDelay(dialog, 1500);

    }

    private void addBranding(Canvas canvas, String description) {
        Paint paint = new Paint();

        int rectStart = canvas.getHeight() - 30;

        paint.setColor(0x99000000);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawRect(0, rectStart, canvas.getWidth(), canvas.getHeight(), paint);

        paint.setColor(Color.WHITE);
        paint.setTextSize(15);

        float width = 0;
        if (description != null && description.length() > 0) {
            width = paint.measureText(description);
            canvas.drawText(description, 10, canvas.getHeight() - 10, paint);
        }

        String promoText = PaintUtility.getProductName(getBaseContext()) +" for Android";
        float promoWidth = paint.measureText(promoText);
        if (promoWidth + width + 40 <= canvas.getWidth()) {
            canvas.drawText(promoText, canvas.getWidth() - promoWidth - 10, canvas.getHeight() - 10, paint);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        LocalBroadcastManager.getInstance(this.getBaseContext()).registerReceiver(_paintColorReceiver,
                new IntentFilter(PaintUtility.COLOR_CHANGED_NOTIFICATION));

        Log.d(TAG, "onStart");

        if (paint_fullscreen_content.isInEditMode()) {
            return;
        }
    }

    String workingDirectory() {
        //return "/sdcard/ph";
        return getBaseContext().getFilesDir().getAbsolutePath() + "/current_project";
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (outState != null) {
            Log.d("JOEL", "save state");
            PaintColor paintColor = PaintUtility.getCurrentPaintColor();
            //outState.putLong(PaintUtility.PAINT_COLOR_EXTRA_KEY, paintColor.getId());

//            HashMap<String, String> test = new HashMap<String, String>();
//            test.put("test", "testing 1");
//            this.painter.setLayerUserData(test, 1);
            //this.painter.saveToDirectory(workingDirectory());
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle outState) {
        super.onRestoreInstanceState(outState);

        //if already loaded, forget it
//        if (PaintUtility.getCurrentPaintColor() != null) {
//            return;
//        }

        if (outState != null) {
            //this.painter.loadFromDirectory(workingDirectory());
//            Map<String,String> data = this.painter.getLayerUserData(1);
//            Log.d("JOEL", "Got back " + data.get("test"));

            if (PaintUtility.getCurrentPaintColor() == null) {
                Log.d("JOEL", "loading paint color from saved state");
                long paintColorId = outState.getLong(PaintUtility.PAINT_COLOR_EXTRA_KEY, 0);

                if (paintColorId > 0) {
                    PaintColorDataSource datasource = new PaintColorDataSource();
                    datasource.open();
                    PaintColor paintColor = datasource.getPaintColor(paintColorId);
                    datasource.close();

                    PaintUtility.setCurrentPaintColor(paintColor);
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (paint_fullscreen_content.isInEditMode()) {
            return;
        }

        PaintColor paintColor = PaintUtility.getCurrentPaintColor();

        if (paintColor == null) {
            getRandomColor();
        }
        else {
            this.setPaintColor(paintColor, paintColor.getColor(), true);
            paint_tabs.loadColorCategory(paintColor.getColorCategory());
        }

        boolean hasImage = this.painter.hasLoadedImage();

        if (!hasImage) {
            reloadImage();
        }

        Log.d(TAG, "onResume");

        toolModeChanged();
        this.paint_drawer.loadContent();
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(_paintColorReceiver);

        Log.d(TAG, "onStop");


    }

    @Override
    protected void onPause()
    {
        super.onPause();
        //saveState();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.d(TAG, "onDestroy");
    }

    /**
     * Back button listener.
     * Will close the application if the back button pressed twice.
     */
    static int backButtonCount = 0;
    static Toast toast = null;

    @Override
    public void onBackPressed()
    {
        if(backButtonCount >= 1)
        {
            if (toast != null) toast.cancel();
            toast = null;
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            backButtonCount = 0;
        }
        else
        {
            toast = Toast.makeText(this, "Press the back button once again to close the application.", Toast.LENGTH_LONG);
            toast.show();
            backButtonCount++;
        }
    }

    private BroadcastReceiver _paintColorReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (PaintUtility.isSender(intent, this)) return;

            PaintColor paintColor = (PaintColor) intent.getSerializableExtra(PaintUtility.PAINT_COLOR_EXTRA_KEY);
            int color = intent.getIntExtra(PaintUtility.COLOR_EXTRA_KEY, 0);

            setPaintColor(paintColor, color, false);
        }
    };

    private boolean gotRandomColor;
    private void getRandomColor()
    {
        PaintColorDataSource datasource = new PaintColorDataSource();
        datasource.open();
        PaintColor paintColor = datasource.getRandomColor();
        datasource.close();

        setPaintColor(paintColor, paintColor.getColor(), true);
        paint_tabs.loadColorCategory(paintColor.getColorCategory());

        gotRandomColor = true;
    }

    private void setPaintColor(PaintColor paintColor, int color, boolean broadcast) {

        setPaintColor(paintColor);
        if (paintColor == null) {
            setColor(color);
        } else {
            if (PaintSwatch.currentSwatch != null && PaintSwatch.currentSwatch.getColor() != paintColor.getColor()) {
                PaintSwatch.currentSwatch.setIsHighlighted(false);
            }
        }

        if (broadcast) {
            PaintUtility.broadcastPaintColor(paintColor, color, getBaseContext(), this);
        }
    }

    private void setPaintColor(PaintColor paintColor) {

        if (paintColor != null) {
            this.setColor(paintColor.getColor());

            paint_color_name_button.setVisibility(View.VISIBLE);
            paint_color_details_button.setVisibility(View.VISIBLE);

            paint_color_name_button.setText(paintColor.getName());

            PaintColorDataSource.setPaintColorIDMetaData(painter, painter.editLayerIndex(), paintColor.getId());
        } else {
            //once invisible, forever gone
            paint_color_name_button.setVisibility(View.INVISIBLE);
            paint_color_details_button.setVisibility(View.INVISIBLE);
        }
        paint_color_name_button.getParent().requestLayout();
    }

    private int getColor() {
        return this.painter.getPaintColor();
    }

    private void setColor(int color) {
        paint_color_view.setBackgroundColor(color);
        if (this.painter.getToolMode() == CBImagePainter.PAINT_TOOL_ERASER) setDefaultToolMode();
        this.painter.setPaintColor(color, true);
        Log.d(TAG, "Changed color to " + color);
    }

    private boolean drawerIsOpen()
    {
        return View.VISIBLE == paint_drawer.getVisibility();
    }

    private void toggleDrawer() {
        toggleDrawer(!drawerIsOpen());
    }

    private void toggleDrawer(boolean open) {

        if (PaintUtility.isLandscape(getBaseContext())) {
            return;
        }

        if (open == drawerIsOpen()) return;

        int paint_drawer_width = paint_drawer.getMeasuredWidth();

        float openOffset = paint_drawer_width;

        if (open) {
            closeOptionsMenu();
        }

        Log.d("JOEL", "drawer will " + (open ? "OPEN" : "CLOSE"));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (open) {
                drawerAnimation.prepareForAnimation(200, 0, openOffset);
            } else {
                drawerAnimation.prepareForAnimation(200, openOffset, 0);
            }
            paint_drawer.startAnimation(drawerAnimation);
        }
        else {
            if (open) {
                paint_drawer.setVisibility(View.VISIBLE);
                paint_drawer.bringToFront();
                drawerOpened();
            } else {
                paint_drawer.setVisibility(View.GONE);
                paint_fullscreen_content.bringToFront();
                drawerClosed();
            }
        }

    }

    private void drawerOpened() {
        painter.setClickable(false);
        _bottomButtons.setClickable(false);
        paint_tabs.setClickable(false);
        paint_drawer.opened();
    }

    private void drawerClosed() {
        painter.setClickable(true);
        _bottomButtons.setClickable(true);
        paint_tabs.setClickable(true);
        paint_drawer.setVisibility(View.GONE);
        paint_drawer.closed();
    }

    private void reloadImage()
    {
        this.painter.loadAsset("living-room-1-masked.png", true);
        setDefaultToolMode();
        if (PaintUtility.getCurrentPaintColor() != null) {
            this.setPaintColor(PaintUtility.getCurrentPaintColor());
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    public void colorButtonClicked(View button) {
        if (null == PaintUtility.getCurrentPaintColor()) return;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            paint_color_name_button.startAnimation(alphaUp);
            paint_color_view.startAnimation(alphaUp);
            paint_color_details_button.startAnimation(alphaUp);
        }

        Intent intent = new Intent(this, ColorViewActivity.class);
        startActivity(intent);
    }


    //Camera
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        int orientation = ExifInterface.ORIENTATION_UNDEFINED;
        if (data != null) {
            orientation = data.getIntExtra(MediaStore.Images.ImageColumns.ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        }
        Uri imageUri = PaintUtility.getPhotoUri(this, requestCode, resultCode, data);
        this.painter.closedCamera();

        if (imageUri != null) {
            this.painter.loadCompressedImage(imageUri, requestCode == PaintUtility.CAMERA_PIC_REQUEST, orientation);
        }
        setDefaultToolMode();
    }

    @Override
    public void onDrawerClicked() {
        // TODO Auto-generated method stub
        toggleDrawer();
        closeAllOverlays(paint_tabs);
    }

    @Override
    public void onTabOpened(int tab) {
        // TODO Auto-generated method stub
        //toggleDrawer();
        closeAllOverlays(paint_tabs);
    }

    @Override
    public void onTabClosed(int tab) {
        // TODO Auto-generated method stub
        //toggleDrawer();
    }

    @Override
    public void onColorChosen(PaintColor paintColor, int color)
    {
        setPaintColor(paintColor, color, false);
    }

    @Override
    public void onAction(int action)
    {

    }

    @Override
    public void onLibraryChosen() {
        PaintUtility.launchLibraryIntent(this);
        toggleDrawer(false);
    }

    @Override
    public void onCameraColorChosen() {
        Intent intent = new Intent(this, HarmonyActivity.class);
        intent.putExtra(HarmonyActivity.USE_LIBRARY_EXTRA, false);
        startActivity(intent);
        toggleDrawer(false);
    }

    @Override
    public void onLibraryColorChosen() {
        Intent intent = new Intent(this, HarmonyActivity.class);
        intent.putExtra(HarmonyActivity.USE_LIBRARY_EXTRA, true);
        startActivity(intent);
        toggleDrawer(false);
    }

    @Override
    public void onExampleChosen() {
        reloadImage();
        toggleDrawer(false);
    }

    @Override
    public void onColorCategoryChosen(ColorCategory colorCategory) {
        paint_tabs.loadColorCategory(colorCategory);
        PaintUtility.broadcastColorCategory(colorCategory, getBaseContext(), this);
        toggleDrawer(false);
        paint_tabs.openTab(paint_tabs.MENU_TAB_SWATCHES, true);
    }

    @Override
    public void onPaintColorChosen(PaintColor color)
    {
        setPaintColor(color, color.getColor(), true);
        toggleDrawer(false);
    }

    @Override
    public void onMoreChosen() {
        if (PaintUtility.isLite(getBaseContext())) {
            //get app
            final Context context = getApplicationContext();

            Uri uri = Uri.parse((PaintUtility.isAmazon
                    ? "http://www.amazon.com/gp/mas/dl/android?p="
                    : "market://details?id=")
                    + PaintUtility.PRO_BUNDLE_ID);

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(browserIntent);
        } else {
            //email
            final Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("plain/text");
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{ EMAIL_FEEDBACK_ADDRESS});
            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, PaintUtility.getProductName(getBaseContext()) + EMAIL_FEEDBACK_SUBJECT);
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        }
    }

    public void onCameraButtonClicked()
    {
        //take photo
        toggleBrushSize(false);
        PaintUtility.launchCameraIntent(PaintActivity.this);
        painter.openedCamera();
    }

    public void onUndoButtonClicked()
    {
        toggleBrushSize(false);
        finishedCommitting();
        painter.stepBackward();
        _bottomButtons._paint_undo_button.setEnabled(painter.canStepBackward());
    }

    public void onToolButtonClicked()
    {
        toggleBrushSize(false);
        if (_paint_menu_buttons.getVisibility() == View.VISIBLE) {
            closeOptionsMenu();
        } else {
            openOptionsMenu();
        }
    }

    public void onBrushSizeButtonClicked()
    {
        toggleBrushSize(_paint_brush_size.getVisibility() != View.VISIBLE);
    }

    public void onLayersButtonClicked()
    {
        showLayersMenu(layers_menu.getVisibility() != View.VISIBLE);
    }

    public void onSaveButtonClicked()
    {
        saveImage();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        onToolButtonClicked();
        return false;
    }

    private void closeAllOverlays(Object except) {
        if (except != layers_menu) showLayersMenu(false);
        if (except != layers_edit_menu) showLayersEditMenu(false);
        if (except != _paint_brush_size) toggleBrushSize(false);
        if (except != _paint_menu_buttons) closeOptionsMenu();
        //if (except != paint_tabs) paint_tabs.hid
    }

    @Override
    public void openOptionsMenu()
    {
        _paint_menu_buttons.bringToFront();
        _paint_menu_buttons.setVisibility(View.VISIBLE);
    }

    @Override
    public void closeOptionsMenu()
    {
        _paint_menu_buttons.setVisibility(View.INVISIBLE);
    }

    private void setDefaultToolMode() {
        setToolMode(CBImagePainter.PAINT_TOOL_PAINT);
    }

    private void setToolMode(int toolMode) {
        painter.setToolMode(toolMode);
        toolModeChanged();
        closeOptionsMenu();
    }

    public void onBucketButtonClicked()
    {
        setToolMode(CBImagePainter.PAINT_TOOL_FILL);
    }

    public void onRectangleButtonClicked()
    {
        setToolMode(CBImagePainter.PAINT_TOOL_RECTANGLE);
    }

    public void onPaintbrushButtonClicked()
    {
        setToolMode(CBImagePainter.PAINT_TOOL_PAINT);
    }

    public void onEraserButtonClicked()
    {
        setToolMode(CBImagePainter.PAINT_TOOL_ERASER);
    }

    private void toolModeChanged() {
        _bottomButtons._paint_brush_size_button.setEnabled(false);
        switch (painter.getToolMode()) {
            case (CBImagePainter.PAINT_TOOL_FILL):
                PaintUtility.setViewBackground(_bottomButtons._paint_tool_button, getResources().getDrawable(R.drawable.bucket_button));
                break;
            case (CBImagePainter.PAINT_TOOL_RECTANGLE):
                PaintUtility.setViewBackground(_bottomButtons._paint_tool_button, getResources().getDrawable(R.drawable.rect_button));
                break;
            case (CBImagePainter.PAINT_TOOL_PAINT):
                _bottomButtons._paint_brush_size_button.setEnabled(true);
                if (this.painter.autoBrushSizeEnabled()) {
                    _bottomButtons._paint_brush_size_button.setText("auto");
                } else {
                    _bottomButtons._paint_brush_size_button.setText("" + this.painter.getBrushSize());
                }
                PaintUtility.setViewBackground(_bottomButtons._paint_tool_button, getResources().getDrawable(R.drawable.paint_button));
                break;
            case (CBImagePainter.PAINT_TOOL_ERASER):
                PaintUtility.setViewBackground(_bottomButtons._paint_tool_button, getResources().getDrawable(R.drawable.eraser_button));
                break;
        }
    }
}
