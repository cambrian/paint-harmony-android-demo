package com.cambriantech.paintharmony.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cambriantech.CBImagePainter;
import com.cambriantech.paintharmony.PaintUtility;

public class PaintColorDataSource extends CBDataSource {

	private static final String TABLE = "ZCOLOR";
    private static final String COLUMN_NAME = "ZNAME";
    private static final String COLUMN_CODE = "ZCODE";
    private static final String COLUMN_RED = "ZRED";
    private static final String COLUMN_GREEN = "ZGREEN";
    private static final String COLUMN_BLUE = "ZBLUE";
    private static final String COLUMN_FAVORITE = "ZFAVORITE";
    private static final String COLUMN_CATEGORY = "ZCATEGORY";
	
	// Database fields
	private String[] allColumns = { 
			CoreSQL.COLUMN_ID,
            COLUMN_CATEGORY,
			COLUMN_NAME,
			COLUMN_CODE,
			COLUMN_RED,
			COLUMN_GREEN,
			COLUMN_BLUE,
			COLUMN_FAVORITE,
			};

	public PaintColorDataSource() {
		
	}

    public PaintColorDataSource(CBDataSource dataSource) {
        super(dataSource);
    }
	
	public List<PaintColor> getColorsForCategory(long categoryId) {
		return getColorsForCategory(categoryId, null);
	}

	List<PaintColor> getColorsForCategory(long categoryId, ColorCategory category) {
		List<PaintColor> colors = new ArrayList<PaintColor>();

		Cursor cursor = getDatabase().query(TABLE,
				allColumns, COLUMN_CATEGORY + "=" + categoryId, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			PaintColor color = cursorToColor(cursor);
			if (category != null) {
				color.setColorCategory(category);
			}
			colors.add(color);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return colors;
	}

    public List<PaintColor> getFavoriteColors() {
        List<PaintColor> colors = new ArrayList<PaintColor>();

        Cursor cursor = getDatabase().query(TABLE,
                allColumns, COLUMN_FAVORITE + "=1", null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PaintColor color = cursorToColor(cursor);
            colors.add(color);
            cursor.moveToNext();
        }
        // Make sure to close the cursor
        cursor.close();
        return colors;
    }

    public PaintColor getPaintColor(long colorId) {
        PaintColor paintColor = null;

        Cursor cursor = getDatabase().query(TABLE,
                allColumns, CoreSQL.COLUMN_ID + "=" + colorId, null, null, null, null);

        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            paintColor = cursorToColor(cursor);
        }
        // Make sure to close the cursor
        cursor.close();

        return paintColor;
    }

    public PaintColor getClosestPaintColor(int color, Brand brand, ColorCategory category) {
        int components[] = PaintUtility.getColorComponents(color);

        int delta = 15;
        PaintColor paintColor = null;

        ArrayList<Long> categoryIds = new ArrayList<Long>();

        if (category != null) {
            categoryIds.add(category.getId());
        } else if (brand != null) {
            List<ColorCategory> categories = brand.getCategories(this);
            for (ColorCategory colorCategory : categories) {
                categoryIds.add(colorCategory.getId());
            }
        }

        while (delta < 256 && paintColor == null) {
            paintColor = getClosestPaintColor(color, getPaintColors(components, delta, categoryIds));
            if (paintColor == null) delta *= 2;
        }
        return paintColor;
    }

    public boolean setIsFavorite(PaintColor color) {
        ContentValues args = new ContentValues();
        args.put(COLUMN_FAVORITE, color.getIsFavorite() ? 1 : 0);

        int result = getDatabase().update(TABLE,
                args,
                CoreSQL.COLUMN_ID + "=" + color.getId(),
                null);

        return result > 0;
    }

    public PaintColor getClosestPaintColor(int color, List<PaintColor> colors) {

        PaintColor paintColor = null;

        double bestDistance = Double.MAX_VALUE;
        for (int i=0; i<colors.size(); i++) {
            PaintColor testColor = colors.get(i);
            double distance = PaintUtility.getColorDistance(color, testColor.getColor());
            if (distance < bestDistance) {
                paintColor = testColor;
            }
        }

        return paintColor;
    }

    private List<PaintColor> getPaintColors(int[] rgb, int delta, ArrayList<Long> colorCategories) {

        int[] rgbMin = new int[3];
        int[] rgbMax = new int[3];

        for (int i=0; i<3; i++) {
            if (rgb[i] - delta < 0) rgb[i] = delta;
            else if (rgb[i] + delta > 255) rgb[i] = 255 - delta;
            rgbMin[i] = rgb[i] - delta;
            rgbMax[i] = rgb[i] + delta;
        }

        String categoriesQuery = "1=1";

        if (colorCategories.size() == 1) {
            categoriesQuery = COLUMN_CATEGORY + "=" + colorCategories.get(0);
        } else if (colorCategories.size() > 1) {
            categoriesQuery = COLUMN_CATEGORY + " IN (";
            categoriesQuery += PaintUtility.join(colorCategories, ", ");
            categoriesQuery += ")";
        }

        String where = COLUMN_RED + ">=" + rgbMin[0] + " AND " + COLUMN_RED + "<=" + rgbMax[0]
                + " AND " + COLUMN_GREEN + ">=" + rgbMin[1] + " AND " + COLUMN_GREEN + "<=" + rgbMax[1]
                + " AND " + COLUMN_BLUE + ">=" + rgbMin[2] + " AND " + COLUMN_BLUE + "<=" + rgbMax[2]
                + " AND " + categoriesQuery;

        Cursor cursor = getDatabase().query(TABLE, allColumns, where,
                null, null, null, null);

        List<PaintColor> colors = new ArrayList<PaintColor>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PaintColor color = cursorToColor(cursor);
            colors.add(color);
            cursor.moveToNext();
        }
        // Make sure to close the cursor
        cursor.close();

        return colors;
    }

    public PaintColor getRandomColor() {
        //SELECT * FROM table ORDER BY RANDOM() LIMIT 1;
        PaintColor color = null;

        //java.lang.String table, java.lang.String[] columns, java.lang.String selection, java.lang.String[] selectionArgs,
        // java.lang.String groupBy,
        // java.lang.String having, java.lang.String orderBy, java.lang.String limit
        Cursor cursor = getDatabase().query(TABLE,
                allColumns, //columns
                null, //selection
                null, //selectionargs
                null, //groupby
                null, //having
                "RANDOM()", //orderby
                "1"); //limit

        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            color = cursorToColor(cursor);
        }
        // Make sure to close the cursor
        cursor.close();

        return color;
    }

    public List<PaintColor>searchForColors(String query) {

        boolean isNumber = false;
        if(query.matches(".*\\d.*")){
            // contains a number
            isNumber = true;
            query = query.replaceAll("\\D", "");
        }

        String where = isNumber ? COLUMN_CODE + " LIKE ?" : COLUMN_NAME + " LIKE ?";

        Cursor cursor = getDatabase().query(TABLE, allColumns, where,
                new String[] {"%" + query + "%"}, null, null, null);

        List<PaintColor> colors = new ArrayList<PaintColor>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PaintColor color = cursorToColor(cursor);
            colors.add(color);
            cursor.moveToNext();
        }
        // Make sure to close the cursor
        cursor.close();

        return colors;
    }

    /*
    - (void) searchQuery:(NSString *)text
    {
        __unsafe_unretained SideBarViewController * weakSelf = self;

        self.searchScreen.hidden = NO;
        [self.loadingIndicator startAnimating];
        self.loadingIndicatorLabel.hidden = NO;

        NSScanner *scanner = [NSScanner scannerWithString:text];
        NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];

        // Throw away characters before the first number.
        [scanner scanUpToCharactersFromSet:numbers intoString:NULL];

        // Collect numbers.
        NSString *numbersString;
        [scanner scanCharactersFromSet:numbers intoString:&numbersString];

        [CBThreading performBlock:^{
        //execute lookup:
        NSMutableString *query = [numbersString length] ? [NSMutableString stringWithString:@"(code CONTAINS[cd] %@)"] : [NSMutableString stringWithString:@"(name CONTAINS[cd] %@)"];
        #if LITE
                [query appendString:@" AND (category.pro_only==NO)"];
        #endif
        NSPredicate *predicate = [NSPredicate predicateWithFormat:query, text];
        _searchResults = [NSMutableArray  arrayWithArray:[[CBCoreData sharedInstance] getRecordsForClass:[Color class] predicate:predicate sortedBy:nil context:nil]];

        dispatch_async(dispatch_get_main_queue(), ^{
                //show loading
                [weakSelf.tableView reloadData];

        weakSelf.searchScreen.hidden = YES;
        weakSelf.loadingIndicatorLabel.hidden = YES;
        [weakSelf.loadingIndicator stopAnimating];
        });
    } onQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
        withIdentifier:@"searchQuery"];
    }*/


    private PaintColor cursorToColor(Cursor cursor) {
		PaintColor color = new PaintColor();
		color.setId(cursor.getLong(0));
		color.setColorCategoryId(cursor.getLong(1));
		color.setName(cursor.getString(2));
		color.setCode(cursor.getString(3));
		color.setRed(cursor.getInt(4));
		color.setGreen(cursor.getInt(5));
		color.setBlue(cursor.getInt(6));
		color.setIsFavorite(1 == cursor.getInt(7));
		return color;
	}

    public static long getPaintColorIDFromMetaData(CBImagePainter painter, int index) {
        Map<String,String> userData = painter.getLayerUserData(index);
        if (userData.containsKey("paintColor")) {
            return Long.parseLong(userData.get("paintColor"));
        }
        return 0;
    }

    public static void setPaintColorIDMetaData(CBImagePainter painter, int index, long colorID) {
        Map<String,String> userData = painter.getLayerUserData(index);
        if (userData != null) {
            userData.put("paintColor", Long.toString(colorID));
            painter.setLayerUserData(userData, index);
        }
    }

    public static PaintColor getPaintColor(CBImagePainter painter, int layerIndex) {
        long colorID = PaintColorDataSource.getPaintColorIDFromMetaData(painter, layerIndex);
        PaintColorDataSource dataSource = new PaintColorDataSource();
        dataSource.open();
        PaintColor color = dataSource.getPaintColor(colorID);
        dataSource.close();
        return color;
    }
}
