package com.cambriantech.paintharmony.data;

import java.io.Serializable;
import java.util.List;

public class ColorCategory implements Serializable {
	private long id;
	private String name;
	private long brandId;
	private Brand brand;
	private List<PaintColor> colors;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	long getBrandId() {
		return brandId;
	}

	void setBrandId(long id) {
		this.brandId = id;
	}
	
	public Brand getBrand() {
		if (this.brand == null) {
			BrandDataSource dataSource = new BrandDataSource();
			dataSource.open();
			brand = dataSource.getBrand(getBrandId());
			dataSource.close();
		}
		return brand;
	}

	void setBrand(Brand brand) {
		this.brand = brand;
	}

	public String getName() {
		return name;
	}

	public void setName(String value) {
		this.name = value;
	}

    public List<PaintColor> getPaintColors() {
        synchronized (this) {
            if (colors == null) {

                PaintColorDataSource datasource = new PaintColorDataSource();
                datasource.open();
                colors = datasource.getColorsForCategory(getId(), this);
                datasource.close();
            }
        }
        return colors;
    }
	
	// Will be used by the ArrayAdapter in the ListView
	@Override
	public String toString() {
		return getName();
	}
}
