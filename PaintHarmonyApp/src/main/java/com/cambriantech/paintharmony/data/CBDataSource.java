package com.cambriantech.paintharmony.data;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by joelteply on 7/15/13.
 */
abstract class CBDataSource {

    private SQLiteDatabase database;
    private int threadPriority;
    private boolean forWriting;
    private static boolean inUse;
    private boolean isDBOwner;

    CBDataSource() {
        this(null);
    }

    CBDataSource(CBDataSource dataSource) {
        if (null != dataSource && null != dataSource.database) {
            database = dataSource.database;
            isDBOwner = false;
        } else {
            isDBOwner = true;
        }
    }

    public void open() throws SQLException {
        open(false);
    }

    public void open(boolean allowWriting) throws SQLException {
        if (!isDBOwner) {
            return;
        }
        while (inUse)  {
            try {
                Thread.currentThread().sleep(100L);
            } catch (InterruptedException e) {

            }
        }
        inUse = true;
        forWriting = allowWriting;
        //gotta go fast: http://knowyourmeme.com/photos/148751
        threadPriority = Thread.currentThread().getPriority();
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        if (allowWriting) {
            database = CoreSQL.sharedContext().getWritableDatabase();
        } else {
            database = CoreSQL.sharedContext().getReadableDatabase();
        }
    }

    public void close() {
        if (!isDBOwner) {
            return;
        }
        database.close();
        inUse = false;
        Thread.currentThread().setPriority(threadPriority);
    }

    protected SQLiteDatabase getDatabase() {
        if (database.isDbLockedByCurrentThread()) {
            Log.d("CBDataSource", "Database IS LOCKED!");
        }
        return database;
    }
}
