package com.cambriantech.paintharmony.data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import android.graphics.Color;
import android.util.Log;

import com.cambriantech.CBImagePainter;
import com.cambriantech.paintharmony.PaintUtility;

public class PaintColor implements Serializable {
	private long id;
	private long colorCategoryId;
	private ColorCategory colorCategory;
	private String name;
	private String code;
	private int red;
	private int green;
	private int blue;
	private boolean isFavorite;
    private boolean setIsFavorite;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	long getColorCategoryId() {
		return colorCategoryId;
	}

	void setColorCategoryId(long id) {
		this.colorCategoryId = id;
	}
	
	public ColorCategory getColorCategory() {
		if (this.colorCategory == null) {
			ColorCategoryDataSource datasource = new ColorCategoryDataSource();
			datasource.open();
			colorCategory = datasource.getColorCategory(getColorCategoryId());
			datasource.close();
		}
		return colorCategory;
	}

	void setColorCategory(ColorCategory category) {
		this.colorCategory = category;
	}

	public String getName() {
		return name;
	}

	public void setName(String value) {
		this.name = value;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String value) {
		this.code = value;
	}
	
	public int getRed() {
		return red;
	}

	public void setRed(int red) {
		this.red = red;
	}
	
	public int getGreen() {
		return green;
	}

	public void setGreen(int red) {
		this.green = red;
	}
	
	public int getBlue() {
		return blue;
	}

	public void setBlue(int blue) {
		this.blue = blue;
	}

	public int getColor() 
	{
		return Color.rgb(getRed(), getGreen(), getBlue());
	}

    public void setColor(int hexColor) {
        int[] components = PaintUtility.getColorComponents(hexColor);
        setRed(components[0]);
        setGreen(components[1]);
        setBlue(components[2]);
    }
	
	public boolean getIsFavorite() {
		return isFavorite;
	}

	public void setIsFavorite(boolean isFavorite) {
		this.isFavorite = isFavorite;
        if (this.setIsFavorite) {
            //update database
            PaintColorDataSource dataSource = new PaintColorDataSource();
            dataSource.open(true);
            dataSource.setIsFavorite(this);
            dataSource.close();
        }
        this.setIsFavorite = true;
	}

	// Will be used by the ArrayAdapter in the ListView
	@Override
	public String toString() {
		return getName();
	}

    public boolean equals(PaintColor color) {
        if (color == null) return false;

        if (color.getId() == this.getId()) {
            return true;
        }
        return false;
    }

    public PaintColor clone() {
        PaintColor color = new PaintColor();
        color.setId(this.getId());
        color.setColorCategoryId(this.getColorCategoryId());

        color.setName(this.getName());
        color.setCode(this.getCode());
        color.setRed(this.getRed());
        color.setGreen(this.getGreen());
        color.setBlue(this.getBlue());
        color.setIsFavorite(this.getIsFavorite());

        return color;
    }
}
