package com.cambriantech.paintharmony.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Brand implements Serializable {
	private long id;
	private String name;
	private String iconFileName;
	private List<ColorCategory>categories;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String value) {
		this.name = value;
	}

    public List<ColorCategory> getCategories()
    {
        return getCategories(null);
    }

    List<ColorCategory> getCategories(CBDataSource dataSource) {
        synchronized (this) {
            if (categories == null) {
                ColorCategoryDataSource categoryDataSource = new ColorCategoryDataSource(dataSource);
                categoryDataSource.open();
                categories = categoryDataSource.getCategoriesForBrand(getId(), this);
                categoryDataSource.close();
            }
        }
        return categories;
    }

	public String getIconFileName() 
	{
		return iconFileName;
	}

	public void setIconFileName(String value) {
		this.iconFileName = value;
	}

	// Will be used by the ArrayAdapter in the ListView
	@Override
	public String toString() {
		return getName();
	}
} 
