package com.cambriantech.paintharmony.data;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class ColorCategoryDataSource extends CBDataSource {

	private static final String TABLE = "ZCOLORCATEGORY";
	private static final String COLUMN_NAME = "ZNAME";
    private static final String COLUMN_BRAND = "ZBRAND";
	
	// Database fields
	private String[] allColumns = { CoreSQL.COLUMN_ID,
            COLUMN_BRAND,
			COLUMN_NAME };

	public ColorCategoryDataSource() {

	}

    public ColorCategoryDataSource(CBDataSource dataSource) {
        super(dataSource);
    }
	
	public List<ColorCategory> getCategoriesForBrand(long brandId) {
		return getCategoriesForBrand(brandId, null);
	}

	List<ColorCategory> getCategoriesForBrand(long brandId, Brand brand) {
		List<ColorCategory> categories = new ArrayList<ColorCategory>();

		Cursor cursor = getDatabase().query(TABLE,
				allColumns, COLUMN_BRAND + "=" + brandId, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			ColorCategory category = cursorToCategory(cursor);
			if (brand != null) {
				category.setBrand(brand);
			}
			categories.add(category);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return categories;
	}
	
	public ColorCategory getColorCategory(long categoryId) {
		ColorCategory category = null;

		Cursor cursor = getDatabase().query(TABLE,
				allColumns, CoreSQL.COLUMN_ID + "=" + categoryId, null, null, null, null);

		cursor.moveToFirst();
		if (!cursor.isAfterLast()) {
			category = cursorToCategory(cursor);
		}
		// Make sure to close the cursor
		cursor.close();
		
		return category;
	}

	private ColorCategory cursorToCategory(Cursor cursor) {
		ColorCategory category = new ColorCategory();
		category.setId(cursor.getLong(0));
		category.setBrandId(cursor.getLong(1));
		category.setName(cursor.getString(2));
		return category;
	}
}
