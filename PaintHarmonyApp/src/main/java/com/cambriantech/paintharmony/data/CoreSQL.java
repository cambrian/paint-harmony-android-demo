package com.cambriantech.paintharmony.data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Path;
import android.util.Log;

public class CoreSQL extends SQLiteOpenHelper {

	public static final String COLUMN_ID = "Z_PK";

	private static String DB_PATH = "/data/data/com.cambriantech.paintharmony/databases/";
	private static final String DATABASE_NAME = "ColorModel.sqlite";
	private static final int DATABASE_VERSION = 1;
	private SQLiteDatabase _database; 
	private Context _context; 
	private static CoreSQL _sharedContext;
	
	public static CoreSQL sharedContext()
	{
		return _sharedContext;
	}
	
	public static void initialize(Context context) 
	{
		if (null == _sharedContext) {
			_sharedContext = new CoreSQL(context);
		}
	}

	private CoreSQL(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
        String destPath = context.getFilesDir().getPath();
        DB_PATH = destPath.substring(0, destPath.lastIndexOf("/")) + "/databases/";
        _sharedContext = this;
		_context = context;
		boolean dbexist = checkDatabase();
        if (dbexist) {
        } else {
            System.out.println("Database doesn't exist");
            try {
                createDatabase();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		//String destinationPath = Path.Combine (System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal),dBName);
		
		database = _database;
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
	}

	public void createDatabase() throws IOException {
        boolean dbexist = checkDatabase();
        if (dbexist) {
        } else {
            this.getReadableDatabase();
            try {
                copyDatabase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    private boolean checkDatabase() {
        boolean checkdb = false;
        try {
            String myPath = DB_PATH + DATABASE_NAME;
            File dbfile = new File(myPath);
            checkdb = dbfile.exists();
        } catch (SQLiteException e) {
            System.out.println("Database doesn't exist");
        }

        return checkdb;
    }

    private void copyDatabase() throws IOException {

    	//Open your local db as the input stream
    	InputStream myInput = _context.getAssets().open(DATABASE_NAME);
 
    	// Path to the just created empty db
    	String outFileName = DB_PATH + DATABASE_NAME;
 
    	//Open the empty db as the output stream
    	OutputStream myOutput = new FileOutputStream(outFileName);
 
    	//transfer bytes from the inputfile to the outputfile
    	byte[] buffer = new byte[1024];
    	int length;
    	while ((length = myInput.read(buffer))>0){
    		myOutput.write(buffer, 0, length);
    	}
 
    	//Close the streams
    	myOutput.flush();
    	myOutput.close();
    	myInput.close();

    }

    public void openDatabase() {
        // Open the database
        String mypath = DB_PATH + DATABASE_NAME;
        _database = SQLiteDatabase.openDatabase(mypath, null,
                SQLiteDatabase.OPEN_READWRITE);

    }

    public synchronized void closeDatabase() {
    	_database.close();
        super.close();
    }
} 