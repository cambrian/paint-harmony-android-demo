package com.cambriantech.paintharmony.data;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class BrandDataSource extends CBDataSource {

	private static final String TABLE = "ZBRAND";
    private static final String COLUMN_NAME = "ZNAME";
    private static final String COLUMN_ICON = "ZICON";
	
	// Database fields
	private String[] allColumns = { CoreSQL.COLUMN_ID,
			COLUMN_NAME, COLUMN_ICON };

	public BrandDataSource() {
		
	}

    public BrandDataSource(CBDataSource dataSource) {
        super(dataSource);
    }
	
	public Brand getBrand(long brandId) {
		Brand brand = null;

		Cursor cursor = getDatabase().query(TABLE,
				allColumns, CoreSQL.COLUMN_ID + "=" + brandId, null, null, null, null);

		cursor.moveToFirst();
		if (!cursor.isAfterLast()) {
			brand = cursorToBrand(cursor);
		}
		// Make sure to close the cursor
		cursor.close();
		
		return brand;
	}

	public List<Brand> getAllBrands() {
		List<Brand> brands = new ArrayList<Brand>();

		Cursor cursor = getDatabase().query(TABLE,
				allColumns, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Brand brand = cursorToBrand(cursor);
			brands.add(brand);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return brands;
	}

	private Brand cursorToBrand(Cursor cursor) {
		Brand brand = new Brand();
		brand.setId(cursor.getLong(0));
		brand.setName(cursor.getString(1));
		brand.setIconFileName(cursor.getString(2));
		return brand;
	}
} 