package com.cambriantech.paintharmony;

import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public class PanelSlideAnimation extends Animation {

    public final static int LEFT_MARGIN = 0;
    public final static int RIGHT_MARGIN = 1;
    public final static int TOP_MARGIN = 2;
    public final static int BOTTOM_MARGIN = 3;

    int margin;
    float originalWidth;
    float originalHeight;
    public float startOffset;
    public float endOffset;
    LinearLayout view;


    public PanelSlideAnimation(LinearLayout view, int margin) {
        this.view = view;
        this.margin = margin;
        this.setInterpolator(new AccelerateDecelerateInterpolator());
    }

    public void prepareForAnimation(long duration, float startOffset, float endOffset)
    {
        this.setDuration(duration);

        this.originalWidth = view.getMeasuredWidth();
        this.originalHeight = view.getMeasuredHeight();
        this.startOffset = startOffset;
        this.endOffset = endOffset;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {

        int currentOffset = (int) (this.endOffset * interpolatedTime
                + this.startOffset * (1-interpolatedTime));

        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();

        switch (this.margin) {
            case LEFT_MARGIN:
                params.leftMargin = currentOffset;
                break;
            case RIGHT_MARGIN:
                params.rightMargin = currentOffset;
                break;
            case TOP_MARGIN:
                params.topMargin = currentOffset;
                break;
            case BOTTOM_MARGIN:
                params.bottomMargin = currentOffset;
                break;
        }

        params.width = (int) this.originalWidth;
        params.height = (int) this.originalHeight;
        view.setLayoutParams(params);

        view.requestLayout();
    }

    @Override
    public void initialize(int width, int height, int parentWidth, int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }
}
