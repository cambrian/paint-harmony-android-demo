package com.cambriantech.paintharmony;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.cambriantech.paintharmony.data.Brand;
import com.cambriantech.paintharmony.data.ColorCategory;
import com.cambriantech.paintharmony.data.PaintColor;
import com.cambriantech.paintharmony.data.PaintColorDataSource;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class MenuTabs extends LinearLayout implements AsyncPaintColorFinder.Handler {

    private MenuTabsListener _delegate;

    public static final int MENU_TAB_COLOR = 0;
    public static final int MENU_TAB_SWATCHES = 1;
    public static final int MENU_TAB_FAVORITES = 2;

    private static final int ACTION_INTERNAL = -1;
    public static final int ACTION_DOWN = 0;
    public static final int ACTION_UP = 1;
    public static final int ACTION_MOVE = 2;

    private Button tabs_drawer_button;

    private SeekBar tabs_color_tab_seekBar;
    private TextView tabs_color_tab_text;

    private SeekBar tabs_saturation_tab_seekBar;
    private TextView tabs_saturation_tab_text;

    private SeekBar tabs_value_tab_seekBar;
    private TextView tabs_value_tab_text;

    private SwatchesScroller paint_swatches;

    private LinearLayout paint_favorites_placeholder;
    private ColorScroller paint_favorites;

    private PaintCheckBox tabs_match_checkbox;
    private PaintCheckBox tabs_match_brand_checkbox;
    private PaintCheckBox tabs_match_category_checkbox;

    private ColorCategory _currentCategory;

    private class TabData
    {
        public int number;
        public LinearLayout tab;
        public Button button;
        public int leftMargin;
        public int minWidth;
        public int openLocation;
        public int closedLocation;
        public boolean isOpen;
        private PanelSlideAnimation animation;
    }

    ArrayList<TabData> tabs;
    private TabData colorTab;
    private TabData swatchesTab;
    private TabData favoritesTab;

    private boolean setupTabs;

    /**
     * Interface definition for a callback to be invoked when a view is clicked.
     */
    public interface MenuTabsListener {
        void onDrawerClicked();
        void onTabOpened(int tab);
        void onTabClosed(int tab);
        void onAction(int action);
        void onColorChosen(PaintColor paintColor, int color);
    }

    public MenuTabs(Context context) {
        super(context);

        inflate(context);
    }

    public MenuTabs(Context context, AttributeSet attrs) {
        super(context, attrs);

        inflate(context);
    }

    @SuppressLint("NewApi")
    public MenuTabs(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        inflate(context);
    }

    private void inflate(Context context)
    {
        tabs = new ArrayList<TabData>();

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(inflater != null){
            inflater.inflate(R.layout.menu_tabs, this);
        }
        Button tabs_drawer_button  =  (Button) findViewById(R.id.tabs_drawer_button);
        tabs_drawer_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != _delegate) {
                    _delegate.onDrawerClicked();
                }
            }
        });

        tabs_color_tab_seekBar = (SeekBar) findViewById(R.id.tabs_color_tab_seekBar);
        tabs_color_tab_seekBar.setMax(360);
        tabs_color_tab_seekBar.setOnSeekBarChangeListener(_seekbarChanged);
        tabs_color_tab_text = (TextView) findViewById(R.id.tabs_color_tab_text);

        tabs_saturation_tab_seekBar = (SeekBar) findViewById(R.id.tabs_saturation_tab_seekBar);
        tabs_saturation_tab_seekBar.setMax(255);
        tabs_saturation_tab_seekBar.setOnSeekBarChangeListener(_seekbarChanged);
        tabs_saturation_tab_text = (TextView) findViewById(R.id.tabs_saturation_tab_text);

        tabs_value_tab_seekBar = (SeekBar) findViewById(R.id.tabs_value_tab_seekBar);
        tabs_value_tab_seekBar.setMax(255);
        tabs_value_tab_seekBar.setOnSeekBarChangeListener(_seekbarChanged);
        tabs_value_tab_text = (TextView) findViewById(R.id.tabs_value_tab_text);

        paint_swatches = (SwatchesScroller) findViewById(R.id.paint_swatches);

        paint_favorites_placeholder = (LinearLayout) findViewById(R.id.paint_favorites_placeholder);
        paint_favorites = (ColorScroller) findViewById(R.id.paint_favorites);

        float density = PaintUtility.getScreenDensity(getContext());

        colorTab = new TabData();
        colorTab.number = MENU_TAB_COLOR;
        colorTab.tab = (LinearLayout) findViewById(R.id.tabs_color_tab);
        colorTab.button = (Button) findViewById(R.id.tabs_color_button);
        colorTab.minWidth = (int) (60.0f * density);
        tabs.add(colorTab);

        swatchesTab = new TabData();
        swatchesTab.number = MENU_TAB_SWATCHES;
        swatchesTab.tab = (LinearLayout) findViewById(R.id.tabs_swatches_tab);
        swatchesTab.button = (Button) findViewById(R.id.tabs_swatches_button);
        swatchesTab.minWidth = (int) (120.0f * density);
        tabs.add(swatchesTab);

        favoritesTab = new TabData();
        favoritesTab.number = MENU_TAB_FAVORITES;
        favoritesTab.tab = (LinearLayout) findViewById(R.id.tabs_favorites_tab);
        favoritesTab.button = (Button) findViewById(R.id.tabs_favorites_button);
        favoritesTab.minWidth = (int) (100.0f * density);
        tabs.add(favoritesTab);

        //create tabs
        int widthAvailable = PaintUtility.getScreenSize(getContext()).x;
        float deviceWidth = ((float) widthAvailable) / density;

        boolean isLargeDisplay = false;

        int leftMargin = (int) (50.0f * density);

        widthAvailable = widthAvailable - leftMargin;
        if (PaintUtility.isLandscape(getContext())) {
            widthAvailable -= 300.0f * density;
        }

        int i = 0;
        for (TabData tab : tabs) {
            tab.leftMargin = leftMargin;
            int amount = 0;
            if (tab == colorTab) {
                amount = widthAvailable / 4;
            } else if (tab == swatchesTab) {
                amount = isLargeDisplay ? widthAvailable / 2 : widthAvailable * 2 / 3;
            }
            leftMargin += tab.minWidth > amount ? tab.minWidth : amount;
            widthAvailable -= leftMargin;
            i++;
        }

        loadFavorites();
        registerReceivers(true);

        // set a global layout listener which will be called when the layout pass is completed and the view is drawn
        this.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                if (!setupTabs) {
                    for (TabData tab : tabs) {
                        setupTab(tab);
                    }
                }
                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            }
        });

        tabs_match_category_checkbox = (PaintCheckBox) findViewById(R.id.tabs_match_category_checkbox);
        if (PaintUtility.isLite(getContext())) {
            tabs_match_category_checkbox.setVisibility(View.GONE);
        }

        tabs_match_checkbox = (PaintCheckBox) findViewById(R.id.tabs_match_checkbox);
        tabs_match_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                tabs_match_brand_checkbox.setEnabled(checked);
                tabs_match_category_checkbox.setEnabled(checked);
            }
        });
        tabs_match_brand_checkbox = (PaintCheckBox) findViewById(R.id.tabs_match_brand_checkbox);
        tabs_match_brand_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                tabs_match_category_checkbox.setEnabled(checked);
            }
        });

    }

    private void registerReceivers(boolean register) {

        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getContext());
        if (register) {
            broadcastManager.registerReceiver(_paintColorReceiver,
                    new IntentFilter(PaintUtility.COLOR_CHANGED_NOTIFICATION));

            broadcastManager.registerReceiver(_colorCategoryReceiver,
                    new IntentFilter(PaintUtility.COLOR_CATEGORY_CHANGED_NOTIFICATION));

            broadcastManager.registerReceiver(_favoritesReceiver,
                    new IntentFilter(PaintUtility.FAVORITES_CHANGED_NOTIFICATION));
        } else {
            broadcastManager.unregisterReceiver(_paintColorReceiver);
            broadcastManager.unregisterReceiver(_colorCategoryReceiver);
            broadcastManager.unregisterReceiver(_favoritesReceiver);
        }
    }

    @Override
    public void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        //super.onSearchRequested()
        registerReceivers(false);
    }

    private BroadcastReceiver _colorCategoryReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (PaintUtility.isSender(intent, this)) return;

            ColorCategory colorCategory = (ColorCategory) intent.getSerializableExtra(PaintUtility.COLOR_CATEGORY_EXTRA_KEY);

            loadColorCategory(colorCategory);
        }
    };

    private BroadcastReceiver _paintColorReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (PaintUtility.isSender(intent, this)) return;

            PaintColor paintColor = (PaintColor) intent.getSerializableExtra(PaintUtility.PAINT_COLOR_EXTRA_KEY);
            int color = intent.getIntExtra(PaintUtility.COLOR_EXTRA_KEY, 0);

            setColor(color);
        }
    };

    private BroadcastReceiver _favoritesReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (PaintUtility.isSender(intent, this)) return;
            loadFavorites();
        }
    };

    private SeekBar.OnSeekBarChangeListener _seekbarChanged = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            _delegate.onAction(ACTION_DOWN);
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            seekbarsChanged(ACTION_MOVE);
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            seekbarsChanged(ACTION_UP);
        }
    };

    private void setColor(int color) {
        float[] hsvValues = new float[3];
        Color.colorToHSV(color, hsvValues);

        int[] hsv = new int[3];

        hsv[0] = (int) (hsvValues[0] * ((float) tabs_color_tab_seekBar.getMax()) / 360.0f);
        hsv[1] = (int) (hsvValues[1] * (float) tabs_saturation_tab_seekBar.getMax());
        hsv[2] = (int) (int) (hsvValues[2] * (float) tabs_value_tab_seekBar.getMax());

        isInternalModification = true;
        tabs_color_tab_seekBar.setProgress(hsv[0]);
        tabs_saturation_tab_seekBar.setProgress(hsv[1]);
        tabs_value_tab_seekBar.setProgress(hsv[2]);
        isInternalModification = false;
    }

    private boolean isInternalModification;
    private void seekbarsChanged(int action)
    {
        if (null == _currentCategory) return;
        int[] hsv = new int[3];
        hsv[0] = tabs_color_tab_seekBar.getProgress();
        hsv[1] = tabs_saturation_tab_seekBar.getProgress();
        hsv[2] = tabs_value_tab_seekBar.getProgress();

        tabs_color_tab_text.setText(Integer.toString(hsv[0]));
        tabs_saturation_tab_text.setText(Integer.toString(hsv[1]));
        tabs_value_tab_text.setText(Integer.toString(hsv[2]));

        if (!isInternalModification) {

            _delegate.onAction(action);

            float[] hsvValues = new float[3];
            hsvValues[0] = (float) hsv[0];
            hsvValues[1] = ((float) hsv[1]) / 255.0f;
            hsvValues[2] = ((float) hsv[2]) / 255.0f;

            int color = Color.HSVToColor(hsvValues);

            if (tabs_match_checkbox.isChecked()) {
                PaintColor paintColor = null;
                Brand brand = null;
                ColorCategory category = null;

                if (tabs_match_brand_checkbox.isChecked()) {
                    brand = _currentCategory.getBrand();
                    if (tabs_match_category_checkbox.isChecked()) {
                        category = _currentCategory;
                    }
                }

                PaintUtility.findColorAsync(this, color, brand, category, action == ACTION_DOWN);
            }
            else {
                _delegate.onColorChosen(null, color);
            }
        }
    }

    @Override
    public void AsyncPaintColorLocated(PaintColor paintColor) {
        _delegate.onColorChosen(paintColor, paintColor.getColor());
    }

    private void setupTab(final TabData tabData)
    {
        ViewGroup.MarginLayoutParams lpButton = (ViewGroup.MarginLayoutParams)tabData.button.getLayoutParams();
        if (lpButton.leftMargin != tabData.leftMargin) {
            lpButton.setMargins(tabData.leftMargin,lpButton.topMargin,lpButton.rightMargin,lpButton.bottomMargin);
            tabData.button.setLayoutParams(lpButton);
        }

        tabData.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tabClicked(tabData);
            }
        });

        tabs_drawer_button = (Button) findViewById(R.id.tabs_drawer_button);
        final int[] tabButtonLocation = new int[2];
        tabs_drawer_button.getLocationInWindow(tabButtonLocation);

        int[] buttonLocation = {0,0};
        tabData.button.getLocationInWindow(buttonLocation);
        tabData.isOpen = true;
        tabData.openLocation = 0;
        tabData.closedLocation = tabButtonLocation[1] - buttonLocation[1];

        tabData.animation = new PanelSlideAnimation(tabData.tab, PanelSlideAnimation.TOP_MARGIN);
        toggleTab(tabData, false, false);
/*
        tabData.tab.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    public void onGlobalLayout() {

                        tabs_drawer_button = (Button) findViewById(R.id.tabs_drawer_button);
                        final int[] tabButtonLocation = new int[2];
                        tabs_drawer_button.getLocationInWindow(tabButtonLocation);

                        int[] buttonLocation = {0,0};
                        tabData.button.getLocationInWindow(buttonLocation);
                        tabData.isOpen = true;
                        tabData.openLocation = 0;
                        tabData.closedLocation = tabButtonLocation[1] - buttonLocation[1];

                        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            tabData.tab.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        } else {
                            tabData.tab.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }

                        tabData.animation = new PanelSlideAnimation(tabData.tab, PanelSlideAnimation.TOP_MARGIN);
                        toggleTab(tabData, false, false);
                    }
                }
        );
        */
    }

    public void setMenuTabsListener(MenuTabsListener delegate) {
        _delegate = delegate;
    }

    public boolean isTabOpen(int tabNumber) {
        TabData tabData = tabs.get(tabNumber);
        return tabData.isOpen;
    }

    public void openTab(int tabNumber, boolean isAnimated) {
        boolean isOpen = isTabOpen(tabNumber);
        if (!isOpen) toggleTab(tabNumber, isAnimated);
    }

    private void toggleTab(int tabNumber, boolean isAnimated) {
        TabData tabData = tabs.get(tabNumber);
        toggleTab(tabData, isAnimated, true);
    }

    private float swatchButtonFontSize;
    public void loadColorCategory(ColorCategory category) {
        if (category == null) return;
        _currentCategory = category;
        String categoryName = category.getName();
        swatchesTab.button.setText(categoryName);

        //TODO: make generic
        Rect bounds = new Rect();

        if (swatchButtonFontSize == 0) {
            swatchButtonFontSize = swatchesTab.button.getTextSize();
        }

        float fontSize = swatchButtonFontSize;
        float leftPadding = swatchesTab.button.getPaddingLeft();
        float rightPadding = leftPadding / 2;
        float availableWidth = PaintUtility.getScreenSize(getContext()).x - swatchesTab.leftMargin - leftPadding - rightPadding;

        if (availableWidth > 0) {
            do {
                swatchesTab.button.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);
                swatchesTab.button.getPaint().getTextBounds(categoryName, 0, categoryName.length(), bounds);
                fontSize --;
            } while (fontSize > 0.7 * swatchButtonFontSize && bounds.width() > availableWidth);
        }
        paint_swatches.setColorCategory(category);
    }

    private void tabClicked(TabData tabData) {
        toggleTab(tabData, true, true);
    }

    private void toggleTab(final TabData tabData, boolean isAnimated, final boolean sendEvents) {

        final boolean willOpen = !tabData.isOpen;

        if (willOpen) {
            tabData.tab.bringToFront();
            tabs_drawer_button.setEnabled(false);
            for (TabData tab : tabs) {
                if (tab != tabData) tab.button.setEnabled(false);
            }
        } else if (isAnimated) {

        }

        Log.d("TAB", (willOpen ? "Opening" : "Closing") + " Tab " + tabData.number);

        if (isAnimated) {
            if (willOpen) {
                tabData.animation.prepareForAnimation(200, tabData.closedLocation, tabData.openLocation);
            } else {
                tabData.animation.prepareForAnimation(200, tabData.openLocation, tabData.closedLocation);
            }
            tabData.animation.setAnimationListener(new Animation.AnimationListener(){
                @Override
                public void onAnimationStart(Animation arg0) {}
                @Override
                public void onAnimationRepeat(Animation arg0) {}
                @Override
                public void onAnimationEnd(Animation arg0) {
                    if (sendEvents && null != _delegate) {
                        if (willOpen) {
                            _delegate.onTabOpened(tabData.number);
                        } else {
                            _delegate.onTabClosed(tabData.number);
                        }
                    }
                    if (!willOpen) {
                        for (TabData tab : tabs) {
                            tab.tab.bringToFront();
                            tab.button.setEnabled(true);
                        }
                        tabs_drawer_button.setEnabled(true);
                    }
                }
            });
            tabData.tab.startAnimation(tabData.animation);
            tabData.isOpen = willOpen;
        } else {
            RelativeLayout.LayoutParams lpTab = new RelativeLayout.LayoutParams(tabData.tab.getLayoutParams());
            lpTab.topMargin = willOpen ? tabData.openLocation : tabData.closedLocation;
            tabData.tab.setLayoutParams(lpTab);
            //send event
            if (sendEvents && null != _delegate) {
                if (willOpen) {
                    _delegate.onTabOpened(tabData.number);
                } else {
                    _delegate.onTabClosed(tabData.number);
                }
            }
            tabData.isOpen = willOpen;
        }

        setClickable(true);
    }

    void loadFavorites()
    {
        PaintColorDataSource datasource = new PaintColorDataSource();
        datasource.open();
        List<PaintColor> colors = datasource.getFavoriteColors();
        datasource.close();

        paint_favorites.setColors(colors);

        if (colors.size() > 0) {
            paint_favorites.setVisibility(VISIBLE);
            paint_favorites_placeholder.setVisibility(GONE);
        } else {
            paint_favorites.setVisibility(GONE);
            paint_favorites_placeholder.setVisibility(VISIBLE);
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l,t,r,b);
    }

    @Override
    public void setClickable(boolean clickable) {
        super.setClickable(clickable);
        for (TabData tab : tabs) {
            tab.button.setClickable(clickable);
        }
    }
}
