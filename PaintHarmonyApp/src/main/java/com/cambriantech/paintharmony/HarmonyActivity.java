package com.cambriantech.paintharmony;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cambriantech.CBHarmony;
import com.cambriantech.paintharmony.data.PaintColor;

import java.util.ArrayList;

/**
 * Created by joelteply on 7/7/13.
 */
public class HarmonyActivity extends SubViewActivity implements CBHarmony.CBHarmonyListener, AsyncPaintColorFinder.Handler {

    public final static String USE_LIBRARY_EXTRA = "USE_LIBRARY";
    private CBHarmony _harmony_image_view;
    private ColorScroller _harmony_color_scroller;
    private ImageView _harmony_color_dropper;
    private ViewGroup _harmony_layout;
    private PaintCircleDrawable _harmony_color_dropper_bg;

    private ViewGroup _harmony_color_container;
    private TextView _harmony_color_name;
    private View _harmony_color_view;
    private boolean _launchedCamera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.harmony_activity);

        _harmony_layout = (ViewGroup) findViewById(R.id.harmony_layout);
        _harmony_image_view = (CBHarmony) findViewById(R.id.harmony_image_view);
        _harmony_image_view.setCBHarmonyListener(this);

        _harmony_color_container = (ViewGroup) findViewById(R.id.harmony_color_container);
        _harmony_color_name = (TextView) findViewById(R.id.harmony_color_name);
        _harmony_color_view = findViewById(R.id.harmony_color_view);

        _harmony_color_scroller = (ColorScroller) findViewById(R.id.harmony_color_scroller);
        _harmony_color_scroller.setMatchBrand(true);

        _harmony_color_dropper = (ImageView) findViewById(R.id.harmony_color_dropper);
        _harmony_color_dropper.setVisibility(View.INVISIBLE);

        _harmony_color_dropper_bg = new PaintCircleDrawable(new Point(5, -9), -12);

        PaintUtility.setViewBackground(_harmony_color_dropper, _harmony_color_dropper_bg);

        setPaintColor(PaintUtility.getCurrentPaintColor());

        LocalBroadcastManager.getInstance(this.getBaseContext()).registerReceiver(_paintColorReceiver,
                new IntentFilter(PaintUtility.COLOR_CHANGED_NOTIFICATION));
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.d("JOEL", "onResume " + _launchedCamera);
        if (!_launchedCamera) {
            _launchedCamera = true;
            boolean useLibrary = this.getIntent().getBooleanExtra(USE_LIBRARY_EXTRA, false);

            if (useLibrary) {
                PaintUtility.launchLibraryIntent(this);
            }
            else {
                PaintUtility.launchCameraIntent(this);
                _harmony_image_view.openedCamera();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (outState != null) {
            outState.putBoolean("launchedCamera", _launchedCamera);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle outState) {
        if (outState != null) {
            _launchedCamera = outState.getBoolean("launchedCamera");
        }
        super.onRestoreInstanceState(outState);
    }

    private BroadcastReceiver _paintColorReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (PaintUtility.isSender(intent, this)) return;

            PaintColor paintColor = (PaintColor) intent.getSerializableExtra(PaintUtility.PAINT_COLOR_EXTRA_KEY);
            int color = intent.getIntExtra(PaintUtility.COLOR_EXTRA_KEY, 0);

            setPaintColor(paintColor);
        }
    };

    private void setPaintColor(PaintColor paintColor) {
        if (paintColor != null) {
            _harmony_color_container.setVisibility(View.VISIBLE);
            _harmony_color_name.setText(paintColor.getName());
            _harmony_color_view.setBackgroundColor(paintColor.getColor());
            _harmony_color_dropper_bg.setColor(paintColor.getColor());
        } else {
            _harmony_color_container.setVisibility(View.GONE);
        }
        _harmony_layout.refreshDrawableState();
    }

    //Camera
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        int orientation = ExifInterface.ORIENTATION_UNDEFINED;
        if (data != null) {
            orientation = data.getIntExtra(MediaStore.Images.ImageColumns.ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        }
        Uri imageUri = PaintUtility.getPhotoUri(this, requestCode, resultCode, data);
        _harmony_image_view.closedCamera();

        if (imageUri != null) {
            //_harmony_image_view.setImageURI(imageUri);
            _harmony_image_view.loadCompressedImage(imageUri, requestCode == PaintUtility.CAMERA_PIC_REQUEST, orientation);
        }
        //file:///storage/emulated/0/HarmonyActivity.png
        //Log.d("JOEL", "loaded image uri: " + imageUri);
    }

    public void gotCommonColors(int detectedColors[])
    {
        ArrayList<PaintColor> colors = new ArrayList<PaintColor>();
        for (int i=0; i<detectedColors.length; i++) {
            PaintColor color = new PaintColor();
            color.setColor(detectedColors[i]);
            colors.add(color);
        }
        _harmony_color_scroller.setColors(colors);
    }

    public void touchStarted(Point point, int color)
    {
        _harmony_color_dropper.setVisibility(View.VISIBLE);
        _harmony_color_dropper_bg.setColor(color);

        PaintUtility.findColorAsync(this, color, false);

        setImageViewLocation(point);
    }

    public void touchMoved(Point point, int color)
    {
        //_harmony_color_dropper_bg.setColor(color);
        PaintUtility.findColorAsync(this, color, false);
        setImageViewLocation(point);
    }

    public void touchEnded(Point point, int color)
    {
        _harmony_color_dropper.setVisibility(View.INVISIBLE);
    }

    @Override
    public void AsyncPaintColorLocated(PaintColor paintColor) {
        setPaintColor(paintColor);
        PaintUtility.broadcastPaintColor(paintColor, paintColor.getColor(), getBaseContext(), this);
    }

    private void setImageViewLocation(Point point) {

        FrameLayout.LayoutParams dropperParams = (FrameLayout.LayoutParams)_harmony_color_dropper.getLayoutParams();
        dropperParams.leftMargin = point.x + _harmony_color_dropper.getMeasuredWidth() / 3;
        dropperParams.topMargin = point.y - _harmony_color_dropper.getMeasuredHeight();
        _harmony_color_dropper.setLayoutParams(dropperParams);
        _harmony_layout.refreshDrawableState();
    }
}
