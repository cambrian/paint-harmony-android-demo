package com.cambriantech.paintharmony;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.CheckBox;

/**
 * Created by joelteply on 7/20/13.
 */
class PaintCheckBox extends CheckBox {

    private int _originalTextColor = -1;

    public PaintCheckBox(Context context) {
        super(context);
    }

    public PaintCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PaintCheckBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            setAlpha(enabled ? 1.0f : 0.3f);
        } else {
            if (_originalTextColor == -1) {
                _originalTextColor = getCurrentTextColor();
            }
            int[] color = PaintUtility.getColorComponents(_originalTextColor);
            setTextColor(enabled ? _originalTextColor : Color.argb(76, color[0], color[1], color[2]));
        }
    }
}

