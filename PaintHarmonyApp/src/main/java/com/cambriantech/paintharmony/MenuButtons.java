package com.cambriantech.paintharmony;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by joelteply on 7/29/13.
 */
public class MenuButtons extends LinearLayout {

    PaintButton _tool_bucket_button;
    PaintButton _tool_rect_button;
    PaintButton _tool_brush_button;
    PaintButton _tool_eraser_button;

    public interface MenuButtonsListener {
        void onBucketButtonClicked();
        void onRectangleButtonClicked();
        void onPaintbrushButtonClicked();
        void onEraserButtonClicked();
    }

    MenuButtonsListener _listener;
    public void setListener(MenuButtonsListener listener) {
        _listener = listener;
    }

    private void inflate(Context context)
    {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(inflater != null){
            inflater.inflate(R.layout.menu_buttons, this);
        }

        //context menu, tool menu
        _tool_bucket_button = (PaintButton) findViewById(R.id.tool_fill_button);
        _tool_bucket_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _listener.onBucketButtonClicked();
            }
        });

        //Rect tool
        _tool_rect_button = (PaintButton) findViewById(R.id.tool_rect_button);
        _tool_rect_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _listener.onRectangleButtonClicked();
            }
        });

        //context menu, tool menu
        _tool_brush_button = (PaintButton) findViewById(R.id.tool_brush_button);
        _tool_brush_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _listener.onPaintbrushButtonClicked();
            }
        });

        //context menu, tool menu
        _tool_eraser_button = (PaintButton) findViewById(R.id.tool_eraser_button);
        _tool_eraser_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _listener.onEraserButtonClicked();
            }
        });
    }

    public MenuButtons(Context context) {
        super(context);

        inflate(context);
    }

    public MenuButtons(Context context, AttributeSet attrs) {
        super(context, attrs);

        inflate(context);
    }

    @SuppressLint("NewApi")
    public MenuButtons(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        inflate(context);
    }
}
