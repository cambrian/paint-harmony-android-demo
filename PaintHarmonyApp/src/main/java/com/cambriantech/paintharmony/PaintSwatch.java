package com.cambriantech.paintharmony;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.cambriantech.paintharmony.data.Brand;
import com.cambriantech.paintharmony.data.ColorCategory;
import com.cambriantech.paintharmony.data.ColorCategoryDataSource;
import com.cambriantech.paintharmony.data.PaintColor;
import com.cambriantech.paintharmony.data.PaintColorDataSource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.logging.Handler;

/**
 * Created by joelteply on 7/6/13.
 */
public class PaintSwatch extends View {

    public static PaintSwatch currentSwatch;

    private boolean _selected;
    private Paint _paintPreview;
    private int _color;
    private PaintColor _paintColor;

    private Brand _brand;
    private ColorCategory _category;
    private boolean _hasDrawn;

    public PaintSwatch(Context context) {
        super(context);

        init();
    }

    public PaintSwatch(Context context, Brand brand, ColorCategory category) {
        super(context);
        init();
        this._brand = brand;
        this._category = category;
    }

    public PaintSwatch(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    @SuppressLint("NewApi")
    public PaintSwatch(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init();
    }

    private void init()
    {
        _paintPreview = new Paint();
        _paintPreview.setStrokeWidth(5.0f * getResources().getDisplayMetrics().density);

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setIsHighlighted(true);
                PaintUtility.broadcastPaintColor(paintColor(), getColor(), getContext(), this);
            }
        });
    }

    int getColor() {
        return _color;
    }

    void setColor(int color) {
        _color = color;
        this.setBackgroundColor(color);
    }

    PaintColor paintColor() {
        if (_paintColor == null || _paintColor.getId() == 0) {
            return null;
        }
        return _paintColor;
    }

    void setPaintColor(PaintColor color) {
        _paintColor = color;
        setColor(color.getColor());

        if (color.equals(PaintUtility.getCurrentPaintColor())) {
            _selected = true;
            setAsCurrentSwatch();
        }

        if (color.getId() == 0) {
            AsyncColorLoader loader = new AsyncColorLoader(this, this._brand, this._category);
            loader.execute();
        }
        safeInvalidate();
    }

    boolean isHighlighted() {
        return _selected;
    }

    void setIsHighlighted(boolean selected) {
        _selected = selected;
        if (selected) {
            setAsCurrentSwatch();
        }
        safeInvalidate();
    }

    void setAsCurrentSwatch() {
        if (currentSwatch != null) {
            currentSwatch.setIsHighlighted(false);
        }
        currentSwatch = PaintSwatch.this;
    }

    private void safeInvalidate() {
        if (!_hasDrawn) return;
        new InvalidateTask().execute();
    }

    private class InvalidateTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }
        protected void onPostExecute(Void result) {
            invalidate();
        }
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (isHighlighted()) {
            double distanceFromHighlight = PaintUtility.getColorDistance(PaintUtility.HIGHLIGHT_COLOR, this.getColor());
            _paintPreview.setColor(distanceFromHighlight < 120 ? PaintUtility.ALT_HIGHLIGHT_COLOR : PaintUtility.HIGHLIGHT_COLOR);
            canvas.drawLine(0, 0, getWidth(), 0, _paintPreview);
            canvas.drawLine(getWidth(), 0, getWidth(), getHeight(), _paintPreview);
            canvas.drawLine(getWidth(), getHeight(), 0, getHeight(), _paintPreview);
            canvas.drawLine(0, getHeight(), 0, 0, _paintPreview);
        }
        _hasDrawn = true;
    }

    private class AsyncColorLoader extends AsyncTask<Void, Void, PaintColor> {

        PaintSwatch swatch;
        private Brand brand;
        private ColorCategory category;

        public AsyncColorLoader(PaintSwatch swatch, Brand brand, ColorCategory category) {
            this.swatch = swatch;
            this.brand = brand;
            this.category = category;
        }


        @Override
        protected PaintColor doInBackground(Void... params) {

            PaintColorDataSource datasource = new PaintColorDataSource();
            datasource.open();
            PaintColor closestMatch = datasource.getClosestPaintColor(this.swatch.getColor(), this.brand, this.category);
            datasource.close();

            return closestMatch;
        }

        @Override
        protected void onPostExecute(PaintColor paintColor) {
            this.swatch.setPaintColor(paintColor);
        }
    }
}
