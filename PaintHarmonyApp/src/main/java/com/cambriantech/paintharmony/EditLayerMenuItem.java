package com.cambriantech.paintharmony;

/**
 * Created by joelteply on 9/11/14.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class EditLayerMenuItem extends RelativeLayout {

    public interface EditLayerMenuItemListener {
        void EditLayerMenuItem_Deleted(int index);
    }

    //context menu, tool menu
    EditLayerMenuItemListener _listener;
    private View _item_color;
    private Button _item_button;
    private TextView _item_text;

    //private VisualiserFragment _visualiser;
    private int _index;

    private void inflate(Context context)
    {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(inflater != null){
            inflater.inflate(R.layout.edit_layer_menu_item, this);
        }

        _item_color = findViewById(R.id.paint_layers_edit_menu_item_color);
        _item_text = (TextView) findViewById(R.id.paint_layers_edit_menu_item_text);

        _item_button = (Button) findViewById(R.id.paint_layers_edit_menu_item_button);
        _item_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _listener.EditLayerMenuItem_Deleted(_index);
            }
        });

        setClickable(true);
    }

    public EditLayerMenuItem(Context context) {
        super(context);

        inflate(context);
    }

    public EditLayerMenuItem(Context context, AttributeSet attrs) {
        super(context, attrs);

        inflate(context);
    }

    @SuppressLint("NewApi")
    public EditLayerMenuItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        inflate(context);
    }

    public void setInfo(EditLayerMenuItemListener visualiser, int index, int color, String text) {
        _listener = visualiser;
        _index = index;
        _item_color.setBackgroundColor(color);
        _item_text.setText(text);
    }
}
