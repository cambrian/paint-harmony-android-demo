package com.cambriantech.paintharmony;

import android.content.Context;
import android.os.AsyncTask;

import com.cambriantech.CBRepeatingTask;
import com.cambriantech.paintharmony.data.Brand;
import com.cambriantech.paintharmony.data.ColorCategory;
import com.cambriantech.paintharmony.data.PaintColor;
import com.cambriantech.paintharmony.data.PaintColorDataSource;

/**
 * Created by joelteply on 7/22/13.
 */
public class AsyncPaintColorFinder extends CBRepeatingTask<PaintColor> {

    private Handler _handler;
    private int _color;
    private Brand _brand;
    private ColorCategory _colorCategory;

    public interface Handler
    {
        public void AsyncPaintColorLocated(PaintColor color);
    }

    public AsyncPaintColorFinder(final Handler handler, int color, Brand brand, ColorCategory colorCategory) {
        this._handler = handler;
        this._color = color;
        this._brand = brand;
        this._colorCategory = colorCategory;
    }

    protected PaintColor ExecuteTask() {
        PaintColor paintColor = null;

        PaintColorDataSource datasource = new PaintColorDataSource();
        datasource.open();
        paintColor = datasource.getClosestPaintColor(this._color, _brand, _colorCategory);
        datasource.close();

        return paintColor;
    }

    protected void HandleTaskResult(PaintColor paintColor) {
        _handler.AsyncPaintColorLocated(paintColor);
    }
}
